import { noteStatus } from "../../common/note-status.enum.js";
import { BoardItem } from "./board-item.model.js";

export class Note extends BoardItem{
    #description;
    #importance;
    #status;
    static DESCRIPTION_MIN_LENGTH=6;
    static DESCRIPTION_MAX_LENGTH=60;

    /**
     * Creates an instance of the Note class.
     * @param {string} name - The name of the note.
     * @param {string} description - The description of the note.
     * @param {noteImportance} importance - The importance of the note.
     */
    constructor(name, description, importance) {
        super(name);
        this.description = description;
        this.#importance=importance;
        this.#status = noteStatus.CREATED;
    }

    /**
     * Gets the description of the note.
     * @returns {string} The description of the note.
     */
    get description() { return this.#description; }

    /**
     * Sets the description of the note.
     * @param {string} value - The new description of the note.
     * @throws {Error} If the description is invalid.
     */
    set description(value) {
        if (typeof value !== 'string' || value.length < Note.DESCRIPTION_MIN_LENGTH || value.length > Note.DESCRIPTION_MAX_LENGTH) {
            throw new Error('Invalid description');
        }
        this.#description = value;
    }

    /**
     * Gets the importance of the note.
     * @returns {noteImportance} The importance of the note.
     */
    get importance() { return this.#importance; }

    /**
     * Gets the status of the note.
     * @returns {noteStatus} The status of the note.
     */
    get status() {
        return this.#status;
    }

    /**
     * Resets the status of the note to CREATED.
     */
    reset() {
        this.#status = noteStatus.CREATED;
    }

    /**
     * Advances the status of the note to PENDING.
     */
    advance() {
        this.#status = noteStatus.PENDING;
    }

    /**
     * Completes the note by setting its status to APPROVED.
     */
    complete() {
        this.#status = noteStatus.APPROVED;
    }

    /**
     * Returns a string representation of the note.
     * @returns {string} The string representation of the note.
     */
    toString() {
        return `* Note *
        Name: ${this.name}
        Status: ${this.status}
        Description: ${this.description}`;
    } 
}