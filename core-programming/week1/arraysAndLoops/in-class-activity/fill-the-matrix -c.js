let input = [
    '3'
];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
let n = +gets();

let matrix = []

for (let i = 0; i < n; i++) {
    matrix[i] = [];
}

const arr = new Array(n).fill(null).map(() => new Array(n).fill(0));

arr[n - 1][0] = 1;

let counter = 1;

for (let row = n - 2; row >= 0; row--) {
    arr[row][0] = arr[row + 1][0] + counter;
    let newRow = row;
    for (let diagonal = 1; diagonal <= counter; diagonal++) {
        arr[newRow + 1][diagonal] = arr[newRow][diagonal - 1] + 1;
        newRow++;
    }
    counter++;
}

arr[0][n - 1] = n * n;
let diagonalLength = 2;
let posX = 1;
let posY = n - 1;
let prevX = 0;
let prevY = n - 1;

for (let i = 1; i < counter - 1; i++) {
    for (let j = 1; j <= diagonalLength; j++) {
        arr[posX][posY] = arr[prevX][prevY] - 1;
        prevX = posX;
        prevY = posY;
        posX--;
        posY--;
    }
    diagonalLength++;
    posX = i + 1;
    posY = n - 1;
}

for (let i = 0; i < arr.length; i++) {
    let line = "";
    for (let j = 0; j < arr[i].length; j++) {
        line += arr[i][j].toString().padStart(4, " ");
    }
    console.log(line);
}