import './About.css'
import Footer from "../../components/Footer/Footer"


export default function About() {

    return (
        <div className="about-wrapper">
            <div className="about">About the website:</div>
            <p> Welcome to our Weather App! A place where you can find all the current weather information about any city on the planet!</p>
            <Footer></Footer>
        </div>

    )
}