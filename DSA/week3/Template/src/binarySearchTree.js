import { BinarySearchTreeNode } from './binarySearchTreeNode.js';

export class BinarySearchTree {
  #root;

  constructor() {
    this.#root = null;
  }

  get root() {
    return this.#root;
  }
  get height() {
    if (this.getHeight(this.#root) === -1) {
      return 0;
    }
    return this.getHeight(this.#root);
  } 
  get dfsInorder() {
    return this.dfsInorderMethod(this.#root);
  }
  get dfsPreorder() {
    return this.dfsPreorderMethod(this.#root);
  }
  get bfs() {
    return this.bfsMethod();
  }
  get dfsPostorder() {
    return this.dfsPostorderMethod(this.#root);
  }

  remove(value) {
    this.#root = this.removeNode(this.#root, value);
  }

  removeNode(node, value) {
    if (!node) {
      return null;
    }

    if (value < node.value) {
      node.left = this.removeNode(node.left, value);
    } else if (value > node.value) {
      node.right = this.removeNode(node.right, value);
    } else {
      if (!node.left && !node.right) {
        node = null;
      }
      else if (!node.left) {
        node = node.right;
      } else if (!node.right) {
        node = node.left;
      }
      else {
        const minValue = this.findMinValue(node.right);
        node.value = minValue;
        node.right = this.removeNode(node.right, minValue);
      }
    }

    return node;
  }

  findMinValue(node) {
    if (!node.left) {
      return node.value;
    }
    return this.findMinValue(node.left);
  }

  insert(value) {
    const newNode = new BinarySearchTreeNode(value);
    if (this.isEmpty()) {
      this.#root = newNode;
    } else {
      this.insertNode(this.#root, newNode);
    }
  }
  insertNode(root, newNode) {
    if (newNode.value < root.value) {
      if (root.left === null) {
        root.left = newNode;
      } else {
        this.insertNode(root.left, newNode);
      }
    } else if (newNode.value > root.value) {
      if (root.right === null) {
        root.right = newNode;
      } else {
        this.insertNode(root.right, newNode);
      }
    }
  }
  search(value) {
    let current = this.root;
    while (current) {
      if (value === current.value) {
        return true;
      } else if (value < current.value) {
        current = current.left;
      } else {
        current = current.right;
      }
    }
    return false;
  }
  dfsInorderMethod(node) {
    const values = [];
    function traverse(node) {
      if (node) {
        traverse(node.left);
        values.push(node.value);
        traverse(node.right);
      }
    }
    traverse(node);
    return values;
  }
  getHeight(node) {
    if (node === null) {
      return -1;
    }
    let leftHeight = this.getHeight(node.left);
    let rightHeight = this.getHeight(node.right);
    return Math.max(leftHeight, rightHeight) + 1;
  }
  dfsPreorderMethod(node) {
    const values = [];
    function traverse(node) {
      if (node) {
        values.push(node.value);
        traverse(node.left);
        traverse(node.right);
      }
    }
    traverse(node);
    return values;
  }
  bfsMethod() {
    const values = [];
    const queue = [];

    if (this.#root) {
      queue.push(this.#root);
    }

    while (queue.length > 0) {
      const node = queue.shift();
      values.push(node.value);

      if (node.left) {
        queue.push(node.left);
      }
      if (node.right) {
        queue.push(node.right);
      }
    }

    return values;
  }
  dfsPostorderMethod(root = this.#root) {
    const values = [];
    function traverse(node) {
      if (node) {
        traverse(node.left);
        traverse(node.right);
        values.push(node.value);
      }
    }
    traverse(root);
    return values;
  }
  isEmpty() {
    return this.#root === null;
  }
  // this function is used to help with testing
  __testSetup(root) {
    this.#root = root;
  }
}


