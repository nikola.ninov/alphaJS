import React from 'react';
import { Route, Navigate } from 'react-router-dom';
import { useAuth } from '../contexts/AuthContext'; 

export default function PrivateRoute({ element: Element, ...rest }) {
    const { currentUser, loading } = useAuth();

    if (loading) {
        // You may show a loading spinner here while checking the authentication state.
        // For example: return <div>Loading...</div>;
        return null;
    }

    return (
        <Route
            {...rest}
            element={
                currentUser ? <Element /> : <Navigate to="/login" replace={true} />
            }
        />
    );
};