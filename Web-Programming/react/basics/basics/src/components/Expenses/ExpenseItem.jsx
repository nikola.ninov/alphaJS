/* eslint-disable react/prop-types */
import './ExpenseItem.css';
import ExpenseDate from './ExpenseDate';
import { useState } from 'react';

function ExpenseItem(props) {
    const [title, setTitle] = useState(props.data.title);

    const clickHandler = () => {
        setTitle('Updated!');
        console.log(title);
    }
    return (
        <li>
            <div className='expense-item'>
                <ExpenseDate date={props.data.date} />
                <div className='expense-item__description'>
                    <h2>{title}</h2>
                    <div className='expense-item__price'>$ {props.data.amount}</div>
                </div>
                <button onClick={clickHandler}>Change Title</button>
            </div>
        </li>
    )
}
export default ExpenseItem;
