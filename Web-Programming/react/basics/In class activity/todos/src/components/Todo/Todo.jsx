import PropTypes from 'prop-types';
import './Todo.css';
const Todo = ({ todo, toggle }) => {    
    const handleToggle = () => toggle(todo.id);

    return (
        <div className="todo-item">
            <div className='todo-item__description'>
                <h2>{todo.name}</h2>
                <div className='todo-item__price'>{todo.due}</div>
            </div>
            <input type='checkbox' id={`todo-item-${todo.id}`} checked={todo.isDone} onChange={handleToggle}/><label htmlFor={`todo-item-${todo.id}`}>{`${todo.isDone}`}</label>
        </div>

    )
}

Todo.propTypes = {
    todo: PropTypes.object.isRequired,
    toggle: PropTypes.func.isRequired,
};
export default Todo;