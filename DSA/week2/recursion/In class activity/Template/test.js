const permutations = (string) => {
    if (string.length === 0) {
      return ''
    }
    if (string.length === 1) {
      return string;
    }
    let result = [];
    for (let i = 0; i < string.length; i++) {
      const element = string[i];
      const remainingChars = string.slice(0, i) + string.slice(i + 1);
      const remainingCharsPermuted = permutations(remainingChars);
      for (let j = 0; j < remainingCharsPermuted.length; j++) {
        const permutation=element + permutations(remainingChars)[j]
        result.push(permutation);
      }
    }
    return result;
  };

  console.log(permutations('dsa'));