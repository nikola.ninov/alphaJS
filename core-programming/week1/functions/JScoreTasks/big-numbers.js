// let input = [
//     "3 4",
//     "8 3 3",
//     "6 2 4 2",
// ]
let input = [
    "5 5",
    "0 3 9 3 2",
    "5 9 5 1 7",
]
let print = this.print || console.log;

let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
const [size1, size2] = gets().split(' ').map(Number);

const a = gets().split(' ').map(Number);
const b = gets().split(' ').map(Number);

const sum = addArrays(a, b);

function addArrays(a, b) {
    const result = [];
    let carry = 0;
    let i = 0;
    while (i < a.length || i < b.length || carry > 0) {
        const digitA = i < a.length ? a[i] : 0;
        const digitB = i < b.length ? b[i] : 0;
        const sum = digitA + digitB + carry;
        result.push(sum % 10);
        carry = Math.floor(sum / 10);
        i++;
    }
    return result.reverse();
}
print(sum.reverse().join(' '));