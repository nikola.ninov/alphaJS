// let input = [`2 3`, `0 0 0`, `0 0 0`];
let input = [`4 3`, `3 2 4`, `2 0 3`, `1 1 5`, `2 2 5`];

let print = this.print || console.log;
let gets =
    this.gets ||
    (
        (arr, index) => () =>
            arr[index++]
    )(input, 0);

const [N, M] = gets().split(` `).map(Number);
let startRow = 0;
let startCol = 0;
const map = [];

for (let i = 0; i < N; i++) {
    const element = gets().split(` `).map(Number);
    //   const indexZero = element.findIndex((e, index) => e == 0);
    const indexZero = element.indexOf(0) !== -1;
    map.push(element);
    if (indexZero) {
        const index = element.indexOf(0);
        startRow = i;
        startCol = index;
    }
}

const traesureHunt = (map, row = startRow, col = startCol, coins = 0) => {
    if (map[row][col] !== 0) {
        map[row][col]--;
        coins++;
    }
    const left = map[row][col - 1] ?? 0;
    const right = map[row][col + 1] ?? 0;
    const top = map[row - 1] === undefined ? 0 : map[row - 1][col];
    const down = map[row + 1] === undefined ? 0 : map[row + 1][col];
    const maxDirection = Math.max(left, right, top, down);

    if (maxDirection === left) {
        col -= 1;
      } else if (maxDirection === right) {
        col += 1;
      } else if (maxDirection === top) {
        row -= 1;
      } else if (maxDirection === down) {
        row += 1;
      }

    if (maxDirection <= 0) {
        return coins;
    } else {
        return traesureHunt(map, row, col, coins);
    }
};
console.log(traesureHunt(map));