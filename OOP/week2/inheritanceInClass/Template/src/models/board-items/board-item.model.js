export class BoardItem {
    #name;
    static NAME_MAX_LENGTH=20; 
    static NAME_MIN_LENGTH=6;
    /**
     * Creates an instance of the BoardItem class.
     * @param {string} name - The name of the board item.
     */
    constructor(name) {
        this.name = name;
    }

    /**
     * Gets the name of the board item.
     * @returns {string} The name of the board item.
     */
    get name() {
        return this.#name;
    }

    /**
     * Sets the name of the board item.
     * @param {string} value - The new name of the board item.
     * @throws {Error} If the name is invalid.
     */
    set name(value) {
        if (typeof value !== 'string' || value.length === 0 || value.length < BoardItem.NAME_MIN_LENGTH || value.length > BoardItem.NAME_MAX_LENGTH) {
            throw new Error('Invalid name');
        }
        this.#name = value;
    }

    /**
     * Returns a string representation of the board item.
     * @returns {string} The string representation of the board item.
     */
    toString() {
        return `Name: ${this.name}`;
    }
}
