import { status } from '../common/status.enum.js';

/** The Task class holds all relevant data and behavior a task might have. */
export class Task {
  // One way to indicate that a field should not be touched is to put an underscore before its name.
  // This won't prevent your colleges from directly interacting with it.

  #name;
  #dueDate;
  #status;
  static #NAME_MIN_LENGTH = 6;
  static #NAME_MAX_LENGTH = 20;

  /**
    * Creates an instance of the Task class.
    * @param {string} name - The name of the task.
    * @param {Date} dueDate - The due date of the task.
    */
  constructor(name, dueDate) {
    this.name = name;
    this.dueDate = dueDate;
    this.status = status.TODO;
  }

  /**
     * Gets the name of the task.
     * @returns {string} The name of the task.
     */
  get name() {
    return this.#name;
  }

  /**
     * Sets the name of the task.
     * @param {string} value - The new name for the task.
     * @throws {Error} If the name is empty or its length is outside the specified range.
     */
  set name(value) {
    if (!value) {
      throw Error('Name cannot be empty!');
    }

    if (value.length < Task.#NAME_MIN_LENGTH || value.length > Task.#NAME_MAX_LENGTH) {
      // eslint-disable-next-line quotes
      throw Error(`Name should be between ${Task.#NAME_MIN_LENGTH} and ${Task.#NAME_MAX_LENGTH} symbols`);
    }
    this.#name = value;
  }

  /**
  * Gets the due date of the task.
  * @returns {Date} The due date of the task.
  */
  get dueDate() {
    return this.#dueDate;
  }

  /**
     * Sets the due date of the task.
     * @param {Date} value - The new due date for the task.
     * @throws {Error} If the date is empty or earlier than the current date.
     */
  set dueDate(value) {
    if (!value) {
      throw Error('Date cannot be empty!');
    }
    if (value.valueOf() < Date.now().valueOf()) {
      throw Error('Due date cannot be earlier than today');
    }
    this.#dueDate = value;
  }

  /**
    * Gets the status of the task.
    * @returns {string} The status of the task.
    */
  get status() {
    return this.#status;
  }
  
  /**
   * Sets the status of the task.
   * @param {string} value - The new status for the task.
   * @throws {Error} If the status is not a valid value.
   */
  set status(value) {
    if (!Object.values(status).includes(value)) {
      throw new Error('Invalid status.');
    }
    this.#status = value
  }

  /** Sets the task status to TODO. */
  reset() {
    this.status = status.TODO;
  }

  /** Sets the task status to IN_PROGRESS. */
  advance() {
    this.status = status.IN_PROGRESS;
  }

  /** Sets the task status to DONE. */
  complete() {
    this.status = status.DONE;
  }

  /** Transforms the task data into a formatted string */
  toString() {
    return `Name: ${this.#name}\n` +
      `Status: ${this.#status}\n` +
      `Due: ${this.#dueDate.toLocaleDateString()} ${this.#dueDate.toLocaleTimeString()}`;
  }
}
