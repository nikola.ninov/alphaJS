# Algorithm Complexity Tasks

Define the complexity of each program/algorithm.

## Task 1

```javascript
function product(a, b) {
    let sum = 0;
    for (let i = 0; i < b; i++) {
        sum += a;
    }
    return sum;
}
```
O(b)
## Task 2

```javascript
function power(a, b) {
    if (b < 0) {
        return 0;
    }
    if (b == 0) {
        return 1;
    }
    let power = a;
    while (b > 1) {
        power *= a;
        b--;
    }
    return power;
}
```
O(b)
## Task 3

```javascript
function mod(a, b) {
    if (b < 0) {
        return -1;
    }
    let div = Math.floor(a / b);
    return a - div * b;
}
```
O(1)
## Task 4

```javascript
function sum3(n) {
    let sum = 0;
    for (let a = 0; a < n; a++) {
        for (let b = 0; b < n; b++) {
            for (let c = 0; c < n; c++) {
                sum += (a * b * c);
            }
        }
    }
    return sum;
}
```
O(n^3)
## Task 5

```javascript
function sumNM(n, m) {
    let sum = 0;
    for (let a = 0; a < n; a++) {
        for (let b = 0; b < m; b++) {
            sum += (a * b);
        }
    }
    return sum;
}
```
O(n*m)
## Task 6

```javascript
function sumNM(n, m) {
    let sum = 0;
    for (let a = 0; a < n; a++) {
        for (let b = 0; b < m; b++) {
            if (a == b) {
                for (let c = 0; c < n; c++) {
                    sum += (a * b * c);
                }
            }
        }
    }
    return sum;
}
```
O(n^3) worst case, O(n*m + m)
## Task 7

```javascript
function factorial(n) {
    let factorial = 1;
    while (n > 1) {
        factorial *= n;
        n--;
    }
    return factorial;
}
```
O(n)