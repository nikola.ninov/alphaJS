let input = ['Valentin Liya Lyubomir Yana Radina'];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let names = gets().split(' ')
let sortNames=[];


names.forEach(el => {
    sortNames.push(sortString(el)); 
});
let result = sortNames.sort();
print(result.join(' '));

function sortString(str) {
    let splitString = str.split("");
    let reverseArray = splitString.sort();
    let joinArray = reverseArray.join("");
    return joinArray;
}