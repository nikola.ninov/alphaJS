import { employees } from './employees.js';
const minNumb = Number.MIN_SAFE_INTEGER;
const maxNumb = Number.MAX_SAFE_INTEGER;

function findEmployeesInPriceRange(emp, { min = minNumb, max = maxNumb }) {
    return emp.filter(s => s.salary > min && s.salary < max).map(e => e.name);
}

// console.log(findEmployeesInPriceRange(employees, { max: 20000 }));
// console.log(findEmployeesInPriceRange(employees, { min: 25000 }));
// console.log(findEmployeesInPriceRange(employees, { min: 15000, max: 30000 }));


function findEmployeesByCity(emp, cityName) {
    let empCopy = JSON.parse(JSON.stringify(emp));
    let result = [];
    result = empCopy.filter(e => e.address).filter(e => e.address.city === cityName);
    return result.map(e => `${e.name}, ${e.address.street}`)
}

//`"{name}, {street}"`

// console.log(findEmployeesByCity(employees, 'Everett'));
// console.log(findEmployeesByCity(employees, 'Kenmore'));
// console.log(findEmployeesByCity(employees, 'Sofia'));

function findByProjects(emp, ...projects) {
    let proj = projects;
    return emp.find(e =>
        proj.every(p => e.projects.includes(p))
    );
}
const emp1 = findByProjects(employees, 'Hydration Pack', 'Fender Set - Mountain', 'Water Bottle');
// emp1.name should be JoLynn Dobney

const emp2 = findByProjects(employees, 'Hydration Pack', 'Fender Set - Mountain');
// emp2.name should be Thierry D'Hers

const emp3 = findByProjects(employees, 'Hydration Pack', 'ML Fork');
// emp3 should be null (no such employee)

const emp4 = findByProjects(employees);
// emp4 should be null (no such employee)
// console.log(emp1);
// console.log(emp2);
// console.log(emp3);
// console.log(emp4);

function getDepartments(employees) {
    return employees.reduce((acc, employee) => {
        if (!(Object.values(acc).includes(employee.department))) {
            acc.push(employee.department)
        }
        return acc
    }, [])
}
// console.log(getDepartments(employees));

function getProjects(employees) {
    return employees.reduce((acc, employee) => {
        employee.projects.forEach(el => {
            if (!(Object.values(acc).includes(el))) {
                acc.push(el);
            }
        });
        return acc;
    }, [])
}
// console.log(getProjects(employees));

function getDepartmentsWithEmployeesCount(employees) {
    return employees.reduce((acc, employee) => {
        if (!acc[employee.department]) {
            acc[employee.department] = 0;
        }
        if (Object.keys(acc).includes(employee.department)) {
            acc[employee.department] = acc[employee.department] + 1
        }
        return acc
    }, {})
}
// console.log(getDepartmentsWithEmployeesCount(employees))

function getEmployeeWithMaxSalary(employees) {
    let maxSalary = Number.MIN_SAFE_INTEGER;
    return employees.reduce((acc, employee) => {
        if (employee.salary > maxSalary) {
            maxSalary = employee.salary;
            acc = employee;
        }
        return acc
    }, {})
}
// console.log(getEmployeeWithMaxSalary(employees))

function getCitiesByPopulation(employees, n) {
    const cities = employees.reduce((acc, employee) => {
        const city = employee.address.city;
        if (!acc[city]) {
            acc[city] = 0;
        }
        if (Object.keys(acc).includes(city)) {
            acc[city] = acc[city] + 1
        }
        return acc
    }, {});
    const sortedCities = Object.keys(cities).sort((a, b) => cities[b] - cities[a]).slice(0, n);
    return sortedCities.map(c => ({ name: c, population: cities[c] }));
}

console.log(getCitiesByPopulation(employees, 3));