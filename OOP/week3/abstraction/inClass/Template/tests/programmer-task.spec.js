/* eslint-disable no-undef */
import { programmerLevel } from '../src/common/programmer-level.enum';
import { Task } from '../src/models/board-items/task.model.js';
import { ProgrammerTask } from '../src/models/board-items/tasks/programmer-task.model.js';
import { Employee } from '../src/models/employee.model.js';

describe('ProgrammerTask', () => {
    const validName = 'Task 1';
    const validDueDate = new Date('2024/09/03');
    const validProgrammerLevel = programmerLevel.REGULAR;
    const validAssignee = new Employee('John Doe');

    describe('Class', () => {
        it('should extend Task', () => {
            expect(ProgrammerTask.prototype).toBeInstanceOf(Task);
        });
    });

    describe('Constructor', () => {
        it('constructor should throw if passed programmer level is invalid', () => {
            const invalidProgrammerLevel = 'invalidProgrammerLevel';
            expect(() => new ProgrammerTask(validName, validDueDate, invalidProgrammerLevel, validAssignee));
        });
        it('constructor should throw if passed assignee is not a Employee object', () => {
            const invalidAssignee = 'invalidAsignee';
            expect(() => new ProgrammerTask(validName, validDueDate, validProgrammerLevel, invalidAssignee));
        });
        it('constructor should assign correctly the values of projectType and assignee', () => {
            const programmerTask = new ProgrammerTask(validName, new Date('2024/09/03'), validProgrammerLevel, validAssignee);
            expect(programmerTask.programmerLevel).toBe(validProgrammerLevel);
            expect(programmerTask.assignee).toBe(validAssignee);
        });
    });

    describe('programmerLevel', () => {
        it('should not be able to change the value of programmerLevel', () => {
            const programmerTask = new ProgrammerTask(validName, new Date('2024/09/03'), validProgrammerLevel, validAssignee);
            expect(() => { programmerTask.programmerLevel = programmerLevel.SENIOR;}).toThrow();
        });
     });
    describe('assignee', () => {
        it('should not be able to change the value of assignee', () => {
            const programmerTask = new ProgrammerTask(validName, new Date('2024/09/03'), validProgrammerLevel, validAssignee);
            const newEmployee = new Employee('Star Doe');
            expect(() => { programmerTask.assignee = newEmployee;}).toThrow();
        });
     });
});