const getRejected = (reason) => new Promise((_, reject) => reject(new Error(reason)));

// produces the same result as above
const getRejectedAsync = async(reason) => {
  throw new Error(reason);
};

const main = async () => {

  try {
    console.log('Before promise rejection');
    await getRejectedAsync('Code executed too fast!');
    console.log('This will log if the promise above was not rejected');
  } catch (e) {
    console.log(`Promise rejected inside try/catch, reason: ${e.message}`);
  }

  await getRejected('Second attempt to break the app')
};

main()
  .catch(e => console.log(`Promise rejected outside try/catch, inside Promise.catch, reason: ${e.message}`));
