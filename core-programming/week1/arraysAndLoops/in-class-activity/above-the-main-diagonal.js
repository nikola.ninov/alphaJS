let input = [
    "5",

];
let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let n = +gets();
let matrix = [];
let counter = 1;
let result = BigInt(0);


let element = 0;

for (let col = 0; col < n; col++) {
    let resultRow = [];

    for (let row = 0; row < n; row++) {
        element = 2 ** (row + col);
        resultRow.push(element)
    }
    matrix.push(resultRow);
}
console.log(matrix);
for (let row = 0; row < matrix.length; row++) {
    for (let col = 0; col < matrix.length; col++) {
        if (col > row) {
            result += BigInt(matrix[row][col])
        }
    }
}

print(result);

for (const row of matrix) {
    print(row.join(" "));
}