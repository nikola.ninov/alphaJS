// Students graduation

/**
 * 
 * @param {{ course: string, passingGrade: number, students: Array<{id: number, name: string}>, grades: Array<{ studentId: number, grade: number}>, exams: Array<{ studentId: number, grade: number }>} data 
 */
export default function (data) {
  // your code starts here
  const dataCopy = JSON.parse(JSON.stringify(data));
  const { course: name, passingGrade, students, grades, exams } = dataCopy;

  const graduates = [];
  const nonGraduates = [];

  students.forEach((student) => {
      const studentGrades = grades.filter(
          (grade) => grade.studentId === student.id || grade.studentName === student.name
      );
      const studentExams = exams.filter(
          (exam) => exam.studentId === student.id || exam.studentName === student.name
      );



      const totalGrade =
          (studentGrades.reduce((sum, grade) => sum + grade.grade, 0) / Math.max(studentGrades.length, 1)) * 0.4 +
          (studentExams.reduce((sum, exam) => sum + exam.grade, 0) / Math.max(studentExams.length, 1)) * 0.6;



      if (studentExams.length >= 2) {
          if (totalGrade >= passingGrade) {
              graduates.push({ id: student.id, name: student.name, grade: Number(totalGrade) });
          } else if (totalGrade < passingGrade) {
              nonGraduates.push({ id: student.id, name: student.name, reason: 'score' });
          }
      } else if (studentExams.length < 2) {
          if (totalGrade >= passingGrade) {
              nonGraduates.push({ id: student.id, name: student.name, reason: 'exams' });
          } else {
              nonGraduates.push({ id: student.id, name: student.name, reason: 'score and exams' });
          }
      }
  });

  return {
      name,
      graduates,
      nonGraduates,
  };
  // your code ends here
}
