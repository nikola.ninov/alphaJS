/* eslint-disable react/no-unescaped-entities */
import { useState } from "react";
import "./App.css";
import ExpenseItem from "./components/Expenses/ExpenseItem";
import ExpensesFilter from "./components/Expenses/ExpensesFilter";
import NewExpense from "./components/NewExpense/NewExpense";
import ExpensesList from "./components/Expenses/ExpensesList";
const DummyData = [
  {
    id: 'e1',
    title: 'Toilet Paper',
    amount: 94.12,
    date: new Date(2020, 7, 14),
  },
  { id: 'e2', title: 'New TV', amount: 799.49, date: new Date(2021, 2, 12) },
  {
    id: 'e3',
    title: 'Car Insurance',
    amount: 294.67,
    date: new Date(2021, 2, 28),
  },
  {
    id: 'e4',
    title: 'New Desk (Wooden)',
    amount: 450,
    date: new Date(2021, 5, 12),
  },

];
const App = () => {

  const [filteredYear, setFilteredYear] = useState('2020');
  const [expenses, setExpenses] = useState(DummyData);
  const [showAddExpense, setShowAddExpense] = useState(false);

  const filteredItems = expenses.filter(el => { return el.date.getFullYear() === parseInt(filteredYear, 10) });

  const addExpenseHandler = (enteredExpenseData) => {
    setExpenses([enteredExpenseData, ...expenses])
  }

  const filterChangeHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  }

  const handleShowAddExpense = () => {
    setShowAddExpense(true);
  };
  const handleHideAddExpense = (props) => {
    setShowAddExpense(props);
  };
  return (
    <div>
      {!showAddExpense && (
        <button className="button button-primary" onClick={handleShowAddExpense}>Add New Expense</button>
      )}
      {showAddExpense && (
        <NewExpense onSaveExpenseData={addExpenseHandler} conditionHideAddExpense={handleHideAddExpense} />
      )}

      <div className="expenses">
        <ExpensesFilter selected={filteredYear} onChangeFilter={filterChangeHandler} />
        <ExpensesList items={filteredItems} />
      </div>
    </div>
  );
}

export default App;
