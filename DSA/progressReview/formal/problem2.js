let input = [
    '2 4'
];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
const [K, N] = gets().split(' ').map(Number);
let arr = [];
arr.push(K);
let element;

for (let i = 0, step = 0, nextElement = 0; i < N; i++, step++) {
    if (step === 0) {
        element = arr[nextElement];
    }
    if (step === 1) {
        element = arr[nextElement] + 1
    }
    if (step === 2) {
        element = 2 * arr[nextElement] + 1;
    }
    if (step === 3) {
        element = arr[nextElement] + 2;
        step = 0;
        nextElement++;
    }
    arr.push(element);
    if (step === 0 && nextElement === 0) {
        arr.pop();
    }
}
console.log(arr[N - 1]);