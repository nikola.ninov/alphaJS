const arr = [1, 2, 3];

for (let i = 0; i < arr.length; i++) {
  (i % 2 == 0) ? console.log(`${i} is even`) : console.log(`${i} is odd.`)
}
