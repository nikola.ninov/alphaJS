function printLogo(company, ...initiative) {
    console.log(company);
    console.log(initiative);
}
// function printMessage(initiative = 'Telerik Academy', greeting) {
//     console.log(`${greeting} from ${initiative}`);
// }
// printLogo();
// printMessage('Telerik Academy', 'Hello');
// printMessage(undefined, 'Hello');

printLogo('Telerik Academy');
printLogo('Telerik Academy', '.NET ALPHA', 'JS Alpha', 'Java Alpha');
printLogo();

const numbers = [1, 2, 3]
console.log(...numbers);
const moreNumbers = [...numbers, 4, 5, 6];
console.log(moreNumbers);

function print() {
    console.log(arguments);
    console.log(arguments[0]);
}
print('telerik Academy');

const printMessage = (greeting, initiative) => {
    console.log(`${greeting} from ${initiative}`);
}

printMessage(undefined, 'Hello');


const sendMessageWithDate = (msg, date = new Date(), ...recipient) => {
    recipient.forEach(el => {
        console.log(`To: ${el} - ${msg}: ${date.toLocaleDateString()}`);
    });
    // console.log(`To: ${recipient} - ${msg}: ${date.toLocaleDateString()}`);
}

sendMessageWithDate('Hello, World!', undefined, "John", 'Maria', 'Ivan');