
import './App.css'
import todosData, { generateID } from './data/todos';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Todo from './components/Todo/Todo';
import CreateTodo from './components/Todo/CreateTodo';
import { useState } from 'react';

function App() {
  const [todos, setTodos] = useState(todosData);
  const [showCreateTodo, setShowCreateTodo] = useState(false);

  const toggle = (id) => {
    setTodos((prevTodos) =>
      prevTodos.map((todo) =>
        todo.id === id ? { ...todo, isDone: !todo.isDone } : todo
      )
  );
      console.log(todos);
      updateTodosData(id);
  };

  const createTodo = (todo) => {
    todo.id=generateID();
    setTodos((prevTodos) => [...prevTodos, todo]);
  };

  const handleCancelCreateTodo = () => {
    alert('No todo added.');
  };
  const updateTodosData = (id) => {
    todosData.forEach((todo) => {
      if (todo.id === id) {
        todo.isDone = !todo.isDone;
      }
    });
  };
  const handleShowCreateTodo = () => {
    setShowCreateTodo(true); 
  };
  const handleHideCreateTodo = () => {
    setShowCreateTodo(false); 
  };

  return (
    <>
      <Header />
      {!showCreateTodo && (
        <button className="button button-primary" onClick={handleShowCreateTodo}>Create Todo</button>
      )}
      {showCreateTodo && (
        <button className="button button-primary" onClick={handleHideCreateTodo}>Hide Create Todo</button>
      )}
      {showCreateTodo && (
        <CreateTodo createTodo={createTodo} onCancel={handleCancelCreateTodo} />
      )}
      
      <div className="todo-back">
      {todos.length ? (
          // Render Todo components with unique keys
          todos.map((todo) => (
            <Todo key={todo.id} todo={todo} toggle={toggle} />
          ))
        ) : (
          // Render "No Todos" message when there are no todos
          <p>No Todos</p>
        )}
      </div>

      <Footer />
    </>
  )
}

export default App
