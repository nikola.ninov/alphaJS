﻿import { InvalidUserInputException } from '../Exceptions/InvalidUserInputException.js';
import { BaseCommand } from './BaseCommand.js';

export class ShowUsersCommand extends BaseCommand {
	constructor(commandParameters, repository) {
		super(commandParameters, repository);
	  }
	
	  get requireLogin() {
		return false;
	  }
	
	  executeCommand() {
		const username = this.commandParameters[0];	
		return this.showUsers(username);
	  }
	
	  showUsers() {
		return this.repository.users;
	  }
	
}