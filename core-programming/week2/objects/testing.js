let obj = {
  a: null,
  b: undefined,
  c: false,
  d: -0,
  e: [],
}
let obj1 = {

  a: null,
  b: undefined,
  c: false,
  d: -0,
  e: [],

}
let obj3 = {
  keys: ['toString', 'h', 'c-3po'],
  first: {
    h: 'c-3po',
  },
  second: {
    h: 'r2-d2',
    ['c-3po']: 'null',
  },
}
let obj4 = {
  keys: ['a', 'b', 'c', 'd'],
  first: {
    name: 'John',
    age: 20,
  },
  second: {
    name: 'Lisa',
    age: 23,
  },
}
let obj5 = {
  keys: [0, 1, 2, 3, 'length'],
  first: [1, 2, 3],
  second: [3, 2, 1, 0],
}
let obj11 = {
  keys: ['toString', 'h', 'c-3po'],
  first: {
    h: 'c-3po',
  },
  second: {
    h: 'r2-d2',
    ['c-3po']: 'null',
  },
}
let obj6 = {
  course: 'Geography',
  minPassingGrade: 3.1,
  students: [
    {
      name: 'Layla',
      grades: [3],
    },
    {
      name: 'Jane',
      grades: [4, 3, 5],
    },
  ],
}
let obj7 = {
  course: 'Alien Studies',
  minPassingGrade: 9000,
  students: [],
}
let obj8 = {
  course: 'Maths',
  minPassingGrade: 3.5,
  students: [
    {
      name: 'Mark',
      grades: [4, 3, 4, 5, 5],
    },
    {
      name: 'John',
      grades: [4, 3, 3],
    },
  ],
}
let obj9 = {
  course: 'English',
  minPassingGrade: 4.3,
  students: [
    {
      name: 'Prashant',
      grades: [4, 4],
    },
    {
      name: 'Niklaus',
      grades: [5, 5, 5, 6],
    },
    {
      name: 'Derick',
      grades: [6],
    },
    {
      name: 'Ivan',
      grades: [],
    },
  ],
}
let obj10 = {
  course: 'Developing Eye-Crossing Applications With Delphi',
  minPassingGrade: 5.3,
  students: [
    {
      name: 'Igor',
      grades: [6, 6],
    },
    {
      name: 'Helga',
      grades: [5, 5, 5, 6],
    },
    {
      name: 'Hvorta',
      grades: [6, 3, 6, 6, 6, 6, 6, 6, 6],
    },
    {
      name: 'Penka',
      grades: [6, 6, 6, 5, 6, 6],
    },
  ],
}

// console.log(Task1(obj1));
// console.log(Task2(obj11));
console.log(Task3(obj9));
// console.log(Task3(obj11));

function Task1(data) {
  const resultObject = {};
  for (let key in data) {
    if (Array.isArray(data[`${key}`])) {
      resultObject[`${key}`] = [...data[`${key}`]];
    } else
      if (typeof data[`${key}`] === 'object' && data[`${key}`] !== null) {
        resultObject[`${key}`] = Object.assign({}, data[`${key}`]);
      } else {
        resultObject[`${key}`] = data[`${key}`];
      }
  }
  return resultObject;

}
function Task2(data) {
  const { keys, first, second } = data;
  let result = {};

  keys.forEach(el => {
    if (el in first && el in second) {
      result[el] = [first[el], second[el]]
    } else
      if (el in first && !(el in second)) {
        result[el] = first[el]
      } else
        if (!(el in first) && el in second) {
          result[el] = second[el]
        } else
          if (!(el in first && el in second) && !(first.el && second.el)) {
            result[el] = null
          }
  });
  return result;
}

function Task3(data) {
  let { course, minPassingGrade, students } = data;

  let graduates = [];
  let courseAverage = 0;
  students.forEach(el => {
    let gradesAvg = 0;
    el.grades.forEach(grade => {
      gradesAvg += grade;
    });
    gradesAvg = Number((gradesAvg / el.grades.length)).toFixed(1);
    if (gradesAvg.toFixed(1) >= minPassingGrade) {
      graduates.push({ name: el.name, score: Number(gradesAvg) });
      courseAverage += gradesAvg;
    }
  });
  if (courseAverage !== 0 && students.length !== 0) {
    courseAverage = Number((courseAverage / graduates.length).toFixed(1))
  }
  let result = { course, minPassingGrade, courseAverage, graduates };
  return result;
}


function TaskTest(data) {
  let { course, minPassingGrade, students } = data;

  let passedStudents = [];
  let avrage = 0;

  students.forEach(el => {

    let gradesAvrage = 0;
    el.grades.forEach(element => {
      gradesAvrage += element;
    });

    gradesAvrage = Number((gradesAvrage / el.grades.length).toFixed(1));

    if (gradesAvrage.toFixed(1) >= minPassingGrade) {
      passedStudents.push({ name: el.name, score: gradesAvrage });
      avrage += gradesAvrage;
    }

    gradesAvrage = 0;
  });

  if (avrage !== 0 && students.length !== 0) {
    avrage /= passedStudents.length;
  }

  let obj = { course, minPassingGrade, courseAverage: avrage, passedStudents: graduates }

  return obj;

}