try {
    console.log(calculator(5, 2, 'add', 'multiply', 'subtract'));
} catch (error) {
    console.log(error.message);
}

function extendedAdd(...numbers) {
    let result = 0;
    numbers.forEach(el => {
        if (!(typeof el === 'number')) {
            throw new Error(`${el} is not a number`);
        }
    });
    numbers.forEach(el => {
        result += el
    });
    return result
}

function add(num1, num2) {
    if (!(typeof num1 === 'number')) {
        throw new Error(`${num1} is not a number`);
    }
    if (!(typeof num2 === 'number')) {
        throw new Error(`${num2} is not a number`);
    }
    return num1 + num2
}

function subtract(num1, num2) {
    if (!(typeof num1 === 'number')) {
        throw new Error(`${num1} is not a number`);
    }
    if (!(typeof num2 === 'number')) {
        throw new Error(`${num2} is not a number`);
    }
    return num1 - num2
}

function multiply(num1, num2) {
    if (!(typeof num1 === 'number')) {
        throw new Error(`${num1} is not a number`);
    }
    if (!(typeof num2 === 'number')) {
        throw new Error(`${num2} is not a number`);
    }
    return num1 * num2
}

function divide(num1, num2) {
    if (!(typeof num1 === 'number')) {
        throw new Error(`${num1} is not a number`);
    }
    if (!(typeof num2 === 'number')) {
        throw new Error(`${num2} is not a number`);
    }
    return num1 / num2
}

function calculator(num1, num2, ...args) {
    let result = [];
    args.forEach(el => {
        switch (el) {
            case 'subtract':
                result.push(subtract(num1, num2));

                break;
            case 'divide':
                result.push(divide(num1, num2));

                break;
            case 'multiply':
                result.push(multiply(num1, num2));

                break;
            case 'add':
                result.push(add(num1, num2));
                break;
            default:
                break;
        }
    });
    return result
}



const firstOne = () => (console.log('something'),console.log('different'));
firstOne();