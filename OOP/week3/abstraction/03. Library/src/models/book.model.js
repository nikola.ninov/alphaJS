export class Book {
  #name;
  #authorName;

  constructor(name, authorName) {
    this.name = name;
    this.authorName = authorName;
  }

  get name() {
    return this.#name;
  }

  set name(value) {
    if(!value) {      
      throw new Error('Book name cannot be empty!');
    }

    this.#name = value;
  }

  get authorName() {
    return this.#authorName;
  }

  set authorName(value) {
    if(!value) {      
      throw new Error('Book author name cannot be empty!');
    }

    this.#authorName = value;
  }
}