import { Task } from '../models/task.js';
import { wait } from '../utils/utils.js';
import { Command } from './command.js';

export class AddTaskCommand extends Command {

  async execute(name, status) {
    await wait(500);

    try {
      const command = new Task(name, status);
      await this.taskRegistry.addTask(command);
      console.log('The task was added!');
    } catch (error) {
      console.log(error.message);
    }
  }
}
