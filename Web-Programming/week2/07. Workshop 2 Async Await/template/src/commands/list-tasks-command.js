import { Command } from './command.js';
import { wait } from '../utils/utils.js';

export class ListTasksCommand extends Command {
    async execute(params = '') {
        await wait(500);

        try {
            const arr = this.taskRegistry.tasks;
            if (arr.length === 0) {
                console.log('The list is empty')
            } else if (params.length !== 0) {
                let list = await arr.filter(el => el.name.includes(params))
                    .map(el => `${el.name} ${el.status}`)
                    .join('\n');

                if (list.length === 0) {
                    console.log(`There is no element with the ${params} symbol`);
                } else {
                    console.log(`This is the list:`);
                    console.log(list);
                }
            } else {
                console.log(`This is the list:`);
                await arr.map(el => {
                    let answer = `${el.name} ${el.status}`;
                    console.log(answer);
                });
            }
        } catch (error) {
            console.log(`This is an error with message ${error.message}`);
        }
    }
}
