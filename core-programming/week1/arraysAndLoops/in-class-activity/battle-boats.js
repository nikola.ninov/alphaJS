// let input = [
//     "2 2",
//     "0 1",
//     "1 1",
//     "1 0",
//     "1 1",
//     "Shoot 1 1",
//     "Shoot 0 1",
//     "Shoot 0 0",
//     "Shoot 0 0",
//     "Shoot 1 1",
//     "Shoot 1 0",
//     "END",
// ];
// let input = [
//     "3 4",
//     "0 1 1 1",
//     "1 1 0 0",
//     "1 1 0 1",
//     "1 0 1 1",
//     "1 0 0 0",
//     "1 1 1 1",
//     "Shoot 2 3",
//     "Shoot 1 1",
//     "Shoot 2 1",
//     "Shoot 0 0",
//     "Shoot 1 1",
//     "Shoot 1 1",
//     "Shoot 2 1",
//     "Shoot 2 3",
//     "END",
// ]
let input = [
    "2 2",
    "0 0",
    "1 0",
    "0 1",
    "0 1",
    "Shoot 1 1",
    "END",
]


let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let n = gets();
let R = Number(n.split(' ')[0]);
let C = Number(n.split(' ')[1]);

let table1 = new Array(R).fill(null).map(() => new Array(C).fill(0));
let table2 = new Array(R).fill(null).map(() => new Array(C).fill(0));

for (let i = 0; i < R; i++) {
    let row = gets().split(' ');
    for (let j = 0; j < C; j++) {
        table1[i][j] = Number(row[j]);
    }
}
for (let i = 0; i < R; i++) {
    let row = gets().split(' ');
    for (let j = 0; j < C; j++) {
        table2[i][j] = Number(row[j]);

    }
    table2[i].reverse();
}

table2.reverse();
let commands = [];

while (true) {
    let command = gets();
    if (command === 'END') {
        break;
    }
    commands.push(command.split(' '))
}

for (let i = 0; i < commands.length; i++) {
    const x = commands[i][1];
    const y = commands[i][2];

    if (i % 2 == 0 && table2[x][y] === 1) {
        table2[x][y] = 'hit';
        print('Booom');
    } else if (i % 2 == 0 && table2[x][y] === 0) {
        table2[x][y] = 'hit';
        print('Missed');
    } else if (i % 2 == 0 && table2[x][y] === 'hit') {
        table2[x][y] = 'hit';
        print('You already shot there!');
    } else if (i % 2 == 1 && table1[x][y] === 1) {
        table1[x][y] = 'hit';
        print('Booom');
    } else if (i % 2 == 1 && table1[x][y] === 0) {
        table1[x][y] = 'hit';
        print('Missed');
    } else if (i % 2 == 1 && table1[x][y] === 'hit') {
        table1[x][y] = 'hit';
        print('You already shot there!');
    }
}
let oneScore = 0;
let twoScore = 0;


let playerOneCopy = table1.join(",").split(",");
let playerTwoCopy = table2.join(",").split(",");


playerOneCopy.forEach(el => {
    if (el === '1') {
        oneScore++;
    }
});
playerTwoCopy.forEach(el => {
    if (el === '1') {
        twoScore++;
    }
});
print(`${oneScore}:${twoScore}`);
