import { readFile, writeFile } from 'fs/promises';
import { join, dirname } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));
const read = async (filename = '') => {
    filename = join(__dirname, filename);
    try {
        const data = await readFile(filename, 'utf-8');
        let tasks;
        tasks = JSON.parse(data);
        console.log('The file was read successfully');
        return tasks;
    } catch (error) {
        throw error;
    }
};

const write = async (filename = '', data = '') => {
    filename = join(__dirname, filename);
    try {
        await writeFile(filename, JSON.stringify(data, null, 2), 'utf-8');
        console.log('Successfully written');
    } catch (error) {
        throw error;
    }
};

const combineData = async () => {
    try {
        const promises = await Promise.all([
            read('data/ids-to-cities.json'),
            read('data/ids-to-grades.json'),
            read('data/ids-to-names.json')
        ]);
        const [file1Data, file2Data, file3Data] = promises;

        const combinedData = file1Data.map((item) => {
            const { id } = item;
            const { name } = file3Data.find((x) => x.id === id);
            const { city } = file1Data.find((x) => x.id === id);
            const { grades } = file2Data.find((x) => x.id === id);
            return { id, name, city, grades };
        });
        await write('data/student-data.json', combinedData);
    } catch (error) {
        console.error('Error combining data:', error);
    }
};

combineData();
