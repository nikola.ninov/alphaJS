import './App.css';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import Home from './components/Home/Home';
import About from './components/About/About';
import Navigation from './components/Navigation/Navigtion';
import Contact from './components/Contact/Contact';
import Details from './components/Details/Details';

const elements = [
  { id: 1, name: 'Element 1', description: 'This is Element 1 description.' },
  { id: 2, name: 'Element 2', description: 'This is Element 2 description.' },
  { id: 3, name: 'Element 3', description: 'This is Element 3 description.' },
  { id: 4, name: 'Element 4', description: 'This is Element 4 description.' },
  { id: 5, name: 'Element 5', description: 'This is Element 5 description.' },
];

function App() {

  return (
    <Router>
      <div>
        <h1>App is running</h1>
        <Navigation/>
        <Routes>
          <Route path="/" exact element={<Home elements={elements}/>} />
          <Route path="/about" element={<About/>} />
          <Route path="/contacts" element={<Contact/>} />
          <Route path="/details/:id" element={<Details elements={elements}/>} />
        </Routes>
      </div>
    </Router>
  )
}

export default App
