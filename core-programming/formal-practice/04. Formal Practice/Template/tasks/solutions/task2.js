// Special mapping function

/**
 * 
 * @param {(param: any) => 1 | 2 | 3} data 
 */
export default function (data /* a function - data: (param: any) => 1 | 2 | 3 */) {
  // your code starts here
  return (param) => {
    return param.map((el) => {

      const result = data(el);

      if (result === 1) {
        return el + 1
      }
      
      if (result === 2) {
        return { name: el }
      }
      
      if (result === 3) {
        return el.map(e => e * e);
      }
    })

  }
  // your code ends here
}
