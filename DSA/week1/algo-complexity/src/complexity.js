//O(1)

/**
 * 
 * @param {Array} arr 
 * @returns 
 */
 const fourForYou = (arr) => 4;

 /**
  * 
  * @param {Number} a 
  * @param {Number} b 
  * @returns 
  */
 const addTwoNumbers = (a, b) => a + b;
 
 //O(n)
 
 /**
  * @typedef {Object} Result
  * @property {number} index - The index of the element, -1 if not found
  * @property {number} steps - The count of operations executed
  */
 
 /**
  * 
  * @param {*} elem 
  * @param {Array} arr 
  * @return Result
  */
 
 const findElem = (elem, arr) => {
   let steps = 0;
   let index = -1;
   for (let i = 0; i < arr.length; i++) {
     steps++;
     if (elem == arr[i]) {
       index = i;
       break;
     }
   }
 
   return {
     index,
     steps
   };
 }
 
 console.log(findElem(2, [1, 2]));
 console.log(findElem(2, [1, 5, 3, 4, 5, 6, 7, 8, 9, 9, 2]));
 const arr = Array.from({ length: 100000000 }).map(_ => 0);
 arr[arr.length - 1] = 2;
 console.log(findElem(2, arr));
 
 //O(m)
 const logNumbersToM = (m) => {
   for (let i = 0; i < m; i++) {
     console.log(m);
   }
 }
 
 //O(m^2)
 const howManyAreBigger = (m) => {
   const result = [];
   for (let i = 0; i < m; i++) {
     let br = 0;
     for (let j = 0; j < m; j++) {
       if (j > i) br++;
     }
     result.push({ i, br });
   }
 
   return result;
 }
 
 console.log(howManyAreBigger(7));
 
 //Let’s estimate the Big O of some examples together​
 
 /**
  * 
  * @param {Array} arr 
  * @returns 
  */
 const findMaxElement = (arr) => {
   let max = arr[0];
   for (let i = 0; i < arr.length; i++) {
     if (arr[i] > max) {
       max = arr[i];
     }
   }
 
   return max;
 }
 
 // Answer: O(m) where m is the size of the array​
 // The number of elementary steps is ~m​​
 
 
 // Inversion Count for an array indicates – how far (or close) the array is from being sorted. 
 // If array is already sorted, then inversion count is 0. 
 // If array is sorted in reverse order that inversion count is the maximum. ​
 // Formally speaking, two elements a[i] and a[j] form an inversion if a[i] > a[j] and i < j​
 // Example:​
 // The sequence 2, 4, 1, 3, 5 has three inversions (2, 1), (4, 1), (4, 3).​
 
 /**
  * 
  * @param {Array} arr 
  * @returns 
  */
 const findInversions = (arr) => {
   let inversions = 0;
   for (let i = 0; i < arr.length; i++) {
     for (let j = i + 1; j < arr.length; j++) {
       if (arr[i] > arr[j]) {
         inversions++;
       }
     }
   }
   return inversions;
 }
 
 //O(m2) where m is the size of the array​
 // The number of elementary steps is ~m*(m-1)/2​
 
 /**
  * 
  * @param {Number} n 
  * @returns 
  */
 const sum3 = (n) => {
   let sum = 0;
   for (let a = 0; a < n; a++)
     for (let b = 0; b < n; b++)
       for (let c = 0; c < n; c++)
         sum = sum + (a * b * c);
   return sum;
 }
 
 // Runs in cubic time O(n3)​
 // The number of elementary steps is ~n3
 
 
 /**
  * 
  * @param {Number} n 
  * @param {Number} m 
  * @returns Number
  */
 const sumMN = (n, m) => {
   let sum = 0;
   for (let x = 0; x < n; x++)
     for (let y = 0; y < m; y++)
       sum = sum + (x * y);
   return sum;
 }
 
 // Runs in quadratic time O(n * m)​
 // The number of elementary steps is ~ (n* m)​
 
 /**
  * 
  * @param {Number} n 
  * @returns 
  */
 const calculation = (n) => {
   let result = 0;
   for (let i = 0; i < 2 * n; i++)
     result++;
   return result;
 }
 
 // Runs in exponential time O(2n) ​
 // The number of elementary steps is ~2n​
 
 /**
  * 
  * @param {Number} n 
  * @returns 
  */
 const factorial = (n) => {
   if (n === 0) return 1;
   return n * factorial(n - 1);
 }
 
 // Runs in linear time O(n) ​
 // The number of elementary steps is ~n​