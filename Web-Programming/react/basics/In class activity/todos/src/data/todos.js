// let id = 0;
// export const nextId = () => id++;

export const generateID = (() => { let counter = 0; return () => counter++; })();
export const todos = [
    {
        id: generateID(),
        name: 'Buy Milk',
        due: '2023-07-20',
        isDone: true
    },
    {
        id: generateID(),
        name: 'Learn JavaScript',
        due: '2023-09-01',
        isDone: false
    },
    {
        id: generateID(),
        name: 'Learn React',
        due: '2023-8-20',
        isDone: false
    },
    {
        id: generateID(),
        name: 'Go to the Gym',
        due: '2023-07-25',
        isDone: false
    },
    {
        id: generateID(),
        name: 'Read a Book',
        due: '2023-08-10',
        isDone: false
    },
    {
        id: generateID(),
        name: 'Prepare Dinner',
        due: '2023-07-27',
        isDone: false
    },
    {
        id: generateID(),
        name: 'Write a Blog Post',
        due: '2023-08-05',
        isDone: false
    },
    {
        id: generateID(),
        name: 'Do Laundry',
        due: '2023-07-30',
        isDone: false
    },
    {
        id: generateID(),
        name: 'Practice Guitar',
        due: '2023-08-15',
        isDone: false
    },
    {
        id: generateID(),
        name: 'Call a Friend',
        due: '2023-07-22',
        isDone: false
    },
];

export default todos;
