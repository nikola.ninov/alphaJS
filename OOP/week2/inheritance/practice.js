import { Person } from "./person.js";
import { Teacher } from "./teacher.js";
const person = new Person('Ivan');
const teacher = new Teacher('Pesho');
console.log(teacher.name);
teacher.subject = 'Math';
console.log(teacher.subject);

console.log(person.sayName());
console.log(teacher.sayName());