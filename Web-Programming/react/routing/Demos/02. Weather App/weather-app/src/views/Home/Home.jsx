import Body from "./../../components/Body/Body.jsx"
import Footer from "./../../components/Footer/Footer.jsx"


export default function Home() {

    return (
        <div>
            <Body></Body>
            <Footer></Footer>
        </div>

    )
}