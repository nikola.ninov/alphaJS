import DoublyLinkedList from './src/doubly-linked-list.js';

// Create a new DoublyLinkedList
const list = new DoublyLinkedList();

// Test addFirst and addLast
list.addFirst(1);
list.addFirst(2);
list.addLast(3);
console.log(list.values()); // Output: [2, 1, 3]

// Test removeFirst and removeLast
list.removeFirst();
list.removeLast();
console.log(list.values()); // Output: [1]

// Test insertBefore and insertAfter
const node = list.find(1);
list.insertBefore(node, 4);
list.insertAfter(node, 5);
console.log(list.values()); // Output: [4, 1, 5]

// Test count property
console.log(list.count); // Output: 3

// Test find method
console.log(list.find(4)); // Output: LinkedListNode  
console.log(list.find(6)); // Output: null
