/**
 * This class represent a Person
 */
export class Person {
    //State
    name;

    constructor(name) {   //конструктурите са специални малки методи които задават начални стойности
        this.name = name
    }

    //Behavior
    /**
     * Prints on the console a hello message with the person's name
     */
    sayName() {
        console.log(`Hello! I am ${this.name}`);
    }
}