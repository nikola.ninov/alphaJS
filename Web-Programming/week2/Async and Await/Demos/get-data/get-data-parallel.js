const data = {
  names: ['Jon', 'Sansa', 'Arya', 'Brandon'],
  users: [
    { name: 'Jon', state: 'reborn' },
    { name: 'Sansa', state: 'alive' },
    { name: 'Arya', state: 'nameless' },
    { name: 'Brandon', state: 'three-eyed crow' },
  ],
  families: {
    Jon: 'Targaryen',
    Sansa: 'Stark',
    Arya: 'Stark',
    Brandon: 'Stark',
  },
};

const requestAsPromise = (key) => {
  console.log(`${key} processing...`);

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(data[key]);
    }, 2000);
  });
};

(async () => {
  const [names, users, families] = await Promise.all([
    requestAsPromise('names'),
    requestAsPromise('users'),
    requestAsPromise('families'),
  ]);

  names.forEach(name => console.log(name));
  users.forEach(user => console.log(user));
  console.log(families);
})().catch(() => { console.log('rejected'); });
