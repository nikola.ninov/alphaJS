const reverse = (array) => {
    if (array.length === 0 || array.length === 1) {
        return [];
    }
    const first = array[0];
    const subArr = array.slice(1);
    const reversedArr = reverse(subArr);
    const result = reversedArr.concat(first);
    return result

}

console.log(reverse([1, 2, 3, 4, 5]));

const deepCopy = (object) => {

    if (typeof object !== "object" || object === null) {
        return object
    }
    const result = {};

    for (const key in object) {
        result[key] = deepCopy(object[key]);
    }
    return result
};


console.log(deepCopy({
    test: {
        name: 'Pesho',
        age: 16,
        profession: null,
    },
    expected: {
        name: 'Pesho',
        age: 16,
        profession: null,
    },
}));

const expression = (expression) => {

}