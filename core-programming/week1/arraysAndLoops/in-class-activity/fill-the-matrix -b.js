let input = [
    '5'
];
let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
const n = +gets();
let matrix = []
for (let i = 0; i < n; i++) {
    matrix[i] = [];
}
let count = 1;
for (let row = 1; row <= n; row++) {
    if (row % 2 === 0) {
        count = count - 1;
        count += n;
        for (let col = 0; col < n; col++) {
            matrix[col][row] = count;
            count--;
        }
    }
    else if (row !== 1 && row % 2 === 1) {
        count++;
        count += n;
        for (let col = 0; col < n; col++) {
            matrix[col][row] = count;
            count++;
        }
    }
    else {
        for (let col = 0; col < n; col++) {
            matrix[col][row] = count;
            count++;
        }
    }

}
for (const row of matrix) {
    print(row.join(" "));
}