// Function spy
export default function (data /* always a function */) {
  // your code starts here
  let operations = {};
  
  return (param) => {
    if (operations[param]) {
      operations[param].calls = operations[param].calls + 1
    }
    if (!operations[param]) {
      const value = data(param);
      operations[param] = { value: value, calls: 1 }
    }
    return operations[param];
  }
  // your code ends here
}