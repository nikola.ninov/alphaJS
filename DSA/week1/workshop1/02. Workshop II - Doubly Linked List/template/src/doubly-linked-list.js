import LinkedListNode from './linked-list-node.js';

export default class DoublyLinkedList {
    #count;
    constructor() {
        this.head = null;
        this.tail = null;
        this.#count = 0;
    }

    get count() {
        return this.#count;
    }

    addFirst(value) {
        const node = new LinkedListNode(value);

        if (!this.head) {
            this.head = node;
            this.tail = node;
        } else {
            node.next = this.head;
            this.head.prev = node;
            this.head = node;
        }
        this.#count++;
    }
    addLast(value) {
        const node = new LinkedListNode(value);
        if (!this.head) {
            this.head = node;
            this.tail = node;
        } else {
            node.prev = this.tail;
            this.tail.next = node;
            this.tail = node;
        }

        this.#count++;
    }

    removeFirst() {
        if (!this.head) {
            throw new Error('List is empty');
        }
        const value = this.head.value;
        if (this.head === this.tail) {
            this.head = null;
            this.tail = null;
        } else {
            this.head = this.head.next;
            this.head.prev = null;
        }
        this.#count--;
        return value;
    }
    removeLast() {
        if (!this.head) {
            throw new Error('List is empty');
        }
        const value = this.tail.value;
        if (this.head === this.tail) {
            this.head = null;
            this.tail = null;
        } else {
            this.tail = this.tail.prev;
            this.tail.next = null;
        }
        this.#count--;
        return value;
    }

    insertBefore(node, value) {
        if (!node) {
            throw new Error('Invalid Node');
        }
        const newNode = new LinkedListNode(value);
        if (node === this.head) {
            newNode.next = this.head;
            this.head.prev = newNode;
            this.head = newNode;
        } else {
            newNode.prev = node.prev;
            newNode.next = node;
            node.prev.next = newNode;
            node.prev = newNode;
        }
        this.#count++;
    }
    insertAfter(node, value) {
        if (!node) {
            throw new Error('Invalid Node');
        }
        const newNode = new LinkedListNode(value);

        if (node === this.tail) {
            newNode.prev = this.tail;
            this.tail.next = newNode;
            this.tail = newNode;
        } else {
            newNode.prev = node;
            newNode.next = node.next;
            node.next.prev = newNode;
            node.next = newNode;
        }
        this.#count++;
        return newNode;
    }

    find(value) {
        let currrent = this.head;
        while (currrent) {
            if (currrent.value === value) {
                return currrent;
            }
            currrent = currrent.next;
        }
        return null;
    }
    values() {
        const result = [];
        let currrent = this.head;
        while (currrent) {
            result.push(currrent.value);
            currrent = currrent.next;
        }
        return result;
    }

}