import './Landing.css'
import Footer from "../../components/Footer/Footer"


export default function Landing() {

    return (
        <div className="landing-wrapper">
            <div className="landing">Welcome to the Weather App!</div>
            <Footer></Footer>
        </div>

    )
}