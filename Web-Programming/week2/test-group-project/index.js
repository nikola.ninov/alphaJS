import { searchGif } from './src/events/search-events.js';



document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('btnSearch').addEventListener('click', (event) => {
      event.preventDefault();
  
      if (event.target.id === 'btnSearch') {
        const inputSearch = document.getElementById('search');
        const searchValue = inputSearch.value;
        searchGif(searchValue);
        inputSearch.value='';
      }
    });
  
    document.addEventListener('click', (event) => {
      if (event.target.classList.contains('gif-link')) {
        const gifId = event.target.dataset.id;
        console.log(gifId);
        // logic to navigate to the details page using the gifId
        //implement event handler then request from api the back to event handler and visualize data with view
      }
    });
  });
