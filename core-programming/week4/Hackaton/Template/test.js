// const keys = (arr) => {
//   return arr.map((p, index) => index);
// };
// // Test case 1: Array of strings
// const arr1 = ["apple", "banana", "cherry", "durian"];
// const keys1 = keys(arr1);
// console.log(keys1); // Output: [0, 1, 2, 3]

// // Test case 2: Array of numbers
// const arr2 = [1, 2, 3, 4, 5];
// const keys2 = keys(arr2);
// console.log(keys2); // Output: [0, 1, 2, 3, 4]

// // Test case 3: Empty array
// const arr3 = [];
// const keys3 = keys(arr3);
// console.log(keys3); // Output: []

// // Test case 4: Array with null values
// const arr4 = [null, null, null];
// const keys4 = keys(arr4);
// console.log(keys4); // Output: [0, 1, 2]

// // Test case 5: Array with undefined values
// const arr5 = [undefined, undefined, undefined];
// const keys5 = keys(arr5);
// console.log(keys5); // Output: [0, 1, 2]

//DOC
// keys(arr)
// The keys function returns an array of indices for the elements in the input array. It is a pure function, meaning it does not modify the original array.

// Arguments
// arr (array): The array for which to generate keys.
// Returns
// An array of keys for the input array. Each key is a non-negative integer representing the index of the corresponding element in the input array. The length of the output array is equal to the length of the input array.

//-------------------------------------------------------------------------------------------------------------------------------------------------


const find = (predicate) => {
  return (arr) => {
    const findElement = arr.filter(predicate);
    return findElement.length > 0 ? findElement[0] : null;
  };
};
// const arr = [1, 2, 3, 4, 5, 6];
// // Test case 1: Find the first even number in the array
// const isEven = (el) => el % 2 === 0;
// const findEven = find(isEven)(arr);
// console.log(findEven); // Output: 2

// // Test case 2: Find the first number greater than 5 in the array
// const greaterThanFive = (el) => el > 5;
// const findGreaterThanFive = find(greaterThanFive)(arr);
// console.log(findGreaterThanFive); // Output: 6

// // Test case 3: Find the first element that is a string (returns null)
// const isString = (el) => typeof el === 'string';
// const findString = find(isString)(arr);
// console.log(findString); // Output: null

// Function Name: find

// Description: This function accepts a predicate function and returns a closure that takes an array as an argument. The closure filters the array using the provided predicate function and returns the first element that matches the condition. If no element matches the condition, it returns null.

// Arguments:

// predicate: (el: any, index: number) => boolean
// A function that takes an element and an index as arguments and returns a boolean value based on some condition.
// Returns:

// (arr: any[]) => any | null
// A closure that takes an array as an argument and filters it based on the provided predicate function. It returns the first element that matches the condition, or null if no element matches the condition.


//----------------------------------------------------------------------------------------------------------------------------------------


// const reduceRight = (fn, initialValue) => {
//   return (arr) => {
//     const reverceArray = arr.reverse();
//     let acumulator = initialValue;
//     reverceArray.map((el) => {
//       acumulator = fn(acumulator, el);
//     });
//     return acumulator;
//   };
// };

// const arr = [1, 2, 3, 4, 5];
// const sum = reduceRight(function(acc, el) {
//     return acc + el;
// }, 0);
// console.log(sum(arr)); // Output: 15
// const product = reduceRight(function (acc, el) {
//     return acc * el;
// }, 1);
// console.log(product(arr));

// const concat = reduceRight(function (acc, el) {
//     return acc + el.toString();
// }, '');
// console.log(concat(arr)); // Output: "54321";

// // Test case 5: Empty array with an initial value of 10
// const emptyArray = [];
// const emptyReducer = reduceRight(function (acc, el) {
//     return acc - el;
// }, 10);
// console.log(emptyReducer(emptyArray)); // Output: 10
// const sum = (acc, val) => acc + val;
// const arr = [1, 2, 3, 4];

// const reduceRightSum = reduceRight(sum, 0);

// console.log(reduceRightSum(arr)); // expected output: 10

// const getMax = (acc, val) => (val > acc ? val : acc);

// const reduceRightMax = reduceRight(getMax, 0);

// console.log(reduceRightMax(arr)); // expected output: 4

// const concat = (acc, val) => acc.concat(val);

// const arr2 = [[0, 1], [2, 3], [4, 5]];

// const reduceRightConcat = reduceRight(concat, []);

// console.log(reduceRightConcat(arr2)); // expected output: [4, 5, 2, 3, 0, 1]

// Function Name: reduceRight

// Description: The reduceRight function iterates over elements of an array in reverse order and reduces all of them into a single value. It accepts a reducer function that will be used to reduce the elements and an initial value for the accumulator.

// Arguments:

// fn: (acc: any, el: any) => any: A reducer function that will accept an accumulator and the next element, and return the updated accumulator.
// initialValue: any: The initial value for the accumulator.
// Returns:

// (arr: any[]) => any: A closure function that will iterate over the passed array in reverse order and call the received reducer function with the accumulator variable and the current element. The function returns the accumulator at the end.

//------------------------------------------------------------------------------------------------------------------------------------------------


const arrayFrom = ({ length }) => {
  if (length < 0) {
    return [];
  }
  return Array(length).fill(undefined);
};

//   Creates an array from object by using its length property.
//   args:
//   `obj: { length: number }`: An object that has the length property.
//   returns:
//   `undefined[]`: Returns the created array with length equal to the passed object's length property, filled with undefined values.

// Test case 1
// const obj = { length: 5 };
// const result = arrayFrom(obj);
// console.log(result); // Output: 5

// Test case 2
let obj1 = { length: 5 };
let arr1 = arrayFrom(obj1);
console.log(arr1); // Output: [undefined, undefined, undefined, undefined, undefined]

// Test case 3
let obj2 = { length: 0 };
let arr2 = arrayFrom(obj2);
console.log(arr2); // Output: []

// Test case 4
let obj3 = { length: 1 };
let arr3 = arrayFrom(obj3);
console.log(arr3); // Output: [undefined]

// Test case 5
let obj4 = { length: 7 };
let arr4 = arrayFrom(obj4);
console.log(arr4); // Output: [undefined, undefined, undefined, undefined, undefined, undefined, undefined]

// Test case 6
let obj5 = { length: -3 };
let arr5 = arrayFrom(obj5);
console.log(arr5); // Output: []

// Function arrayFrom creates an array from an object that has a length property, filling it with undefined values.

// Arguments:
// obj: { length: number }: An object that has a length property.
// Returns:
// undefined[]: If the length property of the object is negative, an empty array will be returned. Otherwise, returns an array with length equal to the passed object's length property, filled with undefined values.