import './Footer.css'; 

function Footer() {
  return (
    <footer className="footer">
      <h1>&copy; {new Date().getFullYear()} Todo App Footer</h1>
    </footer>
  );
}

export default Footer;