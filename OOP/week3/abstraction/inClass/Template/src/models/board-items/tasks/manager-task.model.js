import { Task } from '../task.model.js';
import { projectType } from '../../../common/project-type.enum.js';
import { Employee } from '../../employee.model.js';

export class ManagerTask extends Task {
    #projectType;
    #assignee;

    constructor(name, dueDate, projectType, assignee) {
        super(name, dueDate);
        this.validateProjectType(projectType);
        this.validateAssignee(assignee);
        this.#assignee = assignee;
        this.#projectType = projectType;
    }
    get assignee() {
        return this.#assignee;
    }
    get projectType() {
        return this.#projectType;
    }

    validateProjectType(value) {
        if (!Object.values(projectType).includes(value)) {
            throw new Error('Invalid project type for ManagerTask.');
        }
    }
    validateAssignee(value) {
        if (!(value instanceof Employee) || !value) {
            throw new Error('Invalid assignee for ManagerTask.');
        }
    }
}
