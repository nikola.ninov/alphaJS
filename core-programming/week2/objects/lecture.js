// const student = {
//     name: 'Pesho',
//     grade: 'A'
// }

// let grade = "grade";

// student.name = "Stoyan";

// student[grade] = 'C'

// console.log(student);

// // Destructuring

// let C
// let d

// [c = 5, d = 7] = [1]
// console.log(c);
// console.log(d);

// //destructuring object
// const user = {
//     id: 42,
//     isVerified: true
// }

// const { isVerified, id: ids } = user


// console.log(ids);
// console.log(isVerified);

// //destructuring with default values

// const { firstName, lastName = 'Due' } = {
//     firstName: undefined,
//     lastName: undefined
// }



// console.log(firstName);
// console.log(lastName);


// const letters = ['a', 'e', 'c', 'b', 'd'];
//     const lettersCopy = letters;
//     lettersCopy.sort();
//     console.log(letters);

// const person = { name: 'Pesho' };
//     const modify = (p) => {
//       p.name = 'Gosho';
//     };
//     modify(person);
//     console.log(person.name);



    // const person = { name: 'Pesho' };
    // const modify = (p) => {
    //   p = { name: 'Gosho' }
    // };
    // modify(person);
    // console.log(person.name);

    const user1 = { name: 'Pesho' };
    const user2 = { name: 'Gosho' };
    const user3 = { ...user1, ...user2 };

    console.log(user3);
    console.log(user1 === user2);
    console.log(user1 === user3);
    console.log(user2 === user3);