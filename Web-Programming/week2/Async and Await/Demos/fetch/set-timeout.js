import fetch from 'node-fetch';

const logMovies = async () => {
  const result = await fetch('https://jsonmock.hackerrank.com/api/movies/search/?Title=harry%20potter');
  const jsonResult = await result.json();
  console.log(jsonResult.data);
};

logMovies();

// Blocking operation
let n = 0;
while(n < 10000){
  console.log(n++);
}

console.log('End');
