const input = [
    '4 3',
    '3 2 4',
    '2 0 3',
    '1 1 5',
    '2 2 5',
];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

const [n, m] = gets().split(' ').map(Number);
const matrix = [];
let coins = 0;
let zeroIndex = null;

for (let i = 0; i < n; i++) {
    const row = gets().split(' ').map(Number);
    matrix.push(row);
}

matrix.forEach((row, rowIndex) => {
    const colIndex = row.indexOf(0);
    if (colIndex !== -1) {
        zeroIndex = [rowIndex, colIndex];
    }
});

if (zeroIndex) {
    dfs(zeroIndex[0], zeroIndex[1], matrix[zeroIndex[0]][zeroIndex[1]]);
}

function dfs(row, col, value) {
    if (
        row < 0 ||
        row >= n ||
        col < 0 ||
        col >= m 
    ) {
        return 0;
    }

    if (value > 0) {
        matrix[row][col] -= 1
        coins += 1;
    }
    const directions = [
        [0, -1], // Left
        [0, 1], // Right
        [-1, 0], // Up
        [1, 0] // Down
    ];
    let maxCoins = -1;
    let nextRow = -1;
    let nextCol = -1;

    for (const [dx, dy] of directions) {
        const newRow = row + dx;
        const newCol = col + dy;
        if (
            newRow >= 0 &&
            newRow < n &&
            newCol >= 0 &&
            newCol < m &&
            matrix[newRow][newCol] > maxCoins
        ) {
            maxCoins = matrix[newRow][newCol];
            nextRow = newRow;
            nextCol = newCol;
        }
    }

    if (nextRow !== -1 && nextCol !== -1 && maxCoins !== 0) {
        dfs(nextRow, nextCol, matrix[nextRow][nextCol]);
    } else {
        return 0;
    }

}


print(coins);
// const input = [
//     '4 3',
//     '3 2 4',
//     '2 0 3',
//     '1 1 5',
//     '2 2 5',
// ];

// let print = this.print || console.log;
// let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

// const [n, m] = gets().split(' ').map(Number);
// const matrix = [];
// let coins = 0;
// let zeroIndex = null;

// for (let i = 0; i < n; i++) {
//     const row = gets().split(' ').map(Number);
//     matrix.push(row);
// }

// matrix.forEach((row, rowIndex) => {
//     const colIndex = row.indexOf(0);
//     if (colIndex !== -1) {
//         zeroIndex = [rowIndex, colIndex];
//     }
// });

// if (zeroIndex) {
//     const stack = [];
//     stack.push([zeroIndex[0], zeroIndex[1], matrix[zeroIndex[0]][zeroIndex[1]]]);

//     while (stack.length > 0) {
//         const [row, col, value] = stack.pop();

//         if (
//             row < 0 ||
//             row >= n ||
//             col < 0 ||
//             col >= m 
//         ) {
//             continue;
//         }

//         if (value > 0) {
//             matrix[row][col] -= 1
//             coins += 1;
//         }
//         const directions = [
//             [0, -1], // Left
//             [0, 1], // Right
//             [-1, 0], // Up
//             [1, 0] // Down
//         ];
//         let maxCoins = -1;
//         let nextRow = -1;
//         let nextCol = -1;

//         for (const [dx, dy] of directions) {
//             const newRow = row + dx;
//             const newCol = col + dy;
//             if (
//                 newRow >= 0 &&
//                 newRow < n &&
//                 newCol >= 0 &&
//                 newCol < m &&
//                 matrix[newRow][newCol] > maxCoins
//             ) {
//                 maxCoins = matrix[newRow][newCol];
//                 nextRow = newRow;
//                 nextCol = newCol;
//             }
//         }

//         if (nextRow !== -1 && nextCol !== -1 && maxCoins !== 0) {
//             stack.push([nextRow, nextCol, matrix[nextRow][nextCol]]);
//         }
//     }
// }

// print(coins);