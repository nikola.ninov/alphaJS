const getRejected = (reason) => new Promise((_, reject) => reject(new Error(reason)));

// produces the same result as above
const getRejectedAsync = async(reason) => {
  throw new Error(reason);
};

const main = async () => {
  console.log('Before promise rejection');
  await getRejected('Code executed too fast!');
  console.log('This will log if the promise above was not rejected');
};

main()
  .catch(e => console.log(`Promise got rejected, reason: ${e.message}`));
