// Game of Key-owns

/**
 * 
 * @param {{ keys: string[], first: object, second: object }} data 
 */
export default function (data) {
  const { keys, first, second } = data;
  let result = {};

  keys.forEach(el => {
    if (el in first && el in second) {
      result[el] = [first[el], second[el]]
    } else if (el in first && !(el in second)) {
      result[el] = first[el]
    } else if (!(el in first) && el in second) {
      result[el] = second[el]
    } else if (!(el in first && el in second) && !(first.el && second.el)) {
      result[el] = null
    }
  });
  return result;
  // your code ends here

}