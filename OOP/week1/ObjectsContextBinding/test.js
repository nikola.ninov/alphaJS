const person = {
    name: 'Ivan',
    age: 20,

    sayHello: function () {
        console.log(`Hello, my name is ${this.name}`);
    },
    getName: function () {
        return this.name;
    },
    setAge: function (newAge) {
        if (newAge < 0 || newAge > 120) {
            throw 'Invalid age!';
        }
        this.age = newAge;
    },
    getAge: function () {
        return this.age
    }
}

console.log(person.name);
console.log(person.getName());
person.setAge(22)
person.sayHello();
console.log(person.getAge());