// let input = [
//     '4',
//     '9 1'
// ];

// let print = this.print || console.log;
// let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
// const n = +gets();
// const symbols = gets().split(' ');
// const result = [];

// function generateVariations(length, symbols, current = '') {
//     if (current.length === length) {
//         result.push(current)
//         return;
//     }
//     for (const el of symbols) {
//         generateVariations(length, symbols, current + el)
//     }
// }

// generateVariations(n, symbols);
// result.sort();
// result.forEach(element => {
//     console.log(element);
// });
//--------------------- Variations----------------------------

// let input = [
//     '1122',
//     'A1B12C11D2'
// ];

// let print = this.print || console.log;
// let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

// const secretCode = gets().trim();
// const cipher = gets().trim();

// // Find all possible original messages
// const originalMessages = findOriginalMessages(secretCode, cipher);

// // Print the number of original messages and the messages themselves
// print(originalMessages.length);
// originalMessages.forEach(message => print(message));
// function findOriginalMessages(secretCode, cipher) {
//     const originalMessages = [];

//     function generateCombinations(codeIndex, message) {
//         if (codeIndex === secretCode.length) {
//             originalMessages.push(message);
//             return;
//         }

//         const letter = secretCode[codeIndex];

//         for (let i = 0; i < cipher.length; i += 4) {
//             const cipherLetter = cipher[i];
//             if (cipherLetter === letter) {
//                 const codeLength = parseInt(cipher[i + 1]);
//                 const code = cipher.substring(i + 2, i + 2 + codeLength);
//                 generateCombinations(codeIndex + 1, message + code);
//             }
//         }
//     }

//     generateCombinations(0, '');
//     originalMessages.sort();
//     return originalMessages;
// }



//----------------------Cipher-------------------------------


// const input = [
//     '4 3',
//     '3 2 4',
//     '2 0 3',
//     '1 1 5',
//     '2 2 5',
// ]

// let print = this.print || console.log;
// let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

// function findMaximumNeighbour(row, col, field) {
//     let maxNeighbour = -1;
//     let maxNeighbourCoords = null;

//     // Check left neighbour
//     if (col > 0 && field[row][col - 1] > maxNeighbour) {
//       maxNeighbour = field[row][col - 1];
//       maxNeighbourCoords = [row, col - 1];
//     }

//     // Check right neighbour
//     if (col < field[0].length - 1 && field[row][col + 1] > maxNeighbour) {
//       maxNeighbour = field[row][col + 1];
//       maxNeighbourCoords = [row, col + 1];
//     }

//     // Check upper neighbour
//     if (row > 0 && field[row - 1][col] > maxNeighbour) {
//       maxNeighbour = field[row - 1][col];
//       maxNeighbourCoords = [row - 1, col];
//     }

//     // Check lower neighbour
//     if (row < field.length - 1 && field[row + 1][col] > maxNeighbour) {
//       maxNeighbour = field[row + 1][col];
//       maxNeighbourCoords = [row + 1, col];
//     }

//     return maxNeighbourCoords;
//   }

//   function collectCoins(row, col, field) {
//     if (field[row][col] === 0) {
//       return 0;
//     }

//     let totalCoins = field[row][col];
//     field[row][col] = 0;

//     const nextCoords = findMaximumNeighbour(row, col, field);
//     if (nextCoords !== null) {
//       const [nextRow, nextCol] = nextCoords;
//       totalCoins += collectCoins(nextRow, nextCol, field);
//     }

//     return totalCoins;
//   }

//   // Read input
//   const [n, m] = gets().split(' ').map(Number);
//   const field = [];
//   for (let i = 0; i < n; i++) {
//     const row = gets().split(' ').map(Number);
//     field.push(row);
//   }

//   // Find the starting position of Scrooge McDuck
//   let startRow, startCol;
//   for (let row = 0; row < n; row++) {
//     for (let col = 0; col < m; col++) {
//       if (field[row][col] === 0) {
//         startRow = row;
//         startCol = col;
//         break;
//       }
//     }
//     if (startRow !== undefined) {
//       break;
//     }
//   }

//   // Collect coins and print the result
//   const result = collectCoins(startRow, startCol, field);
//   print(result);

const input = [
    '5 6',
    '1 3 2 2 2 4',
    '3 3 3 2 4 4',
    '4 3 1 2 3 3',
    '4 3 1 3 3 1',
    '4 3 3 3 1 1'
]

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

const [n, m] = gets().split(' ').map(Number);
const matrix = [];
for (let i = 0; i < n; i++) {
    const row = gets().split(' ').map(Number);
    matrix.push(row);
}
const largestArea = findLargestArea(matrix);

function findLargestArea(matrix) {
    const n = matrix.length;
    const m = matrix[0].length;
    const visited = new Array(n).fill(false).map(() => new Array(m).fill(false));
    let maxArea = 0;

    function dfs(row, col, value) {
        if (
            row < 0 ||
            row >= n ||
            col < 0 ||
            col >= m ||
            visited[row][col] ||
            matrix[row][col] !== value
        ) {
            return 0;
        }

        visited[row][col] = true;
        let area = 1;
        area += dfs(row - 1, col, value); // Up
        area += dfs(row + 1, col, value); // Down
        area += dfs(row, col - 1, value); // Left
        area += dfs(row, col + 1, value); // Right
        return area;
    }

    for (let row = 0; row < n; row++) {
        for (let col = 0; col < m; col++) {
            if (!visited[row][col]) {
                const value = matrix[row][col];
                const area = dfs(row, col, value);
                maxArea = Math.max(maxArea, area);
            }
        }
    }
    return maxArea;
}

print(largestArea);