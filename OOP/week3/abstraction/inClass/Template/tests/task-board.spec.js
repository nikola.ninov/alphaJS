/* eslint-disable no-undef */
import { projectType } from '../src/common/project-type.enum';
import { Task } from '../src/models/board-items/task.model.js';
import { ManagerTask } from '../src/models/board-items/tasks/manager-task.model.js';
import { Board } from '../src/models/board.model.js';
import { TaskBoard } from '../src/models/task-board.model.js';
import { Employee } from '../src/models/employee.model.js';
import { programmerLevel } from '../src/common/programmer-level.enum';
import { ProgrammerTask } from '../src/models/board-items/tasks/programmer-task.model.js';

describe('ManagerTask', () => {

    describe('TaskBoard', () => {
        it('should extend Board model', () => {
            const taskBoard=new TaskBoard();
            expect(taskBoard instanceof TaskBoard).toBe(true);
            expect(taskBoard instanceof Board).toBe(true);
        });
    });
    describe('add(item)',()=>{
        it('add(item) should throw if item is not an instance of Task, ManagerTask, or ProgrammerTask',()=>{
            const taskBoard=new TaskBoard();
            const invalidItem='Invalid Item';
            expect(()=>taskBoard.add(invalidItem)).toThrow();
        });

        it('add(item) should not throw if item is an instance of Task, ManagerTask, or ProgrammerTask',()=>{
            const validAssignee = new Employee('John Doe');
            const validAssignee1 = new Employee('Max Doe');
            const taskBoard=new TaskBoard();
            const validTask=new Task('Task 1',new Date('2024/09/03'));          

            const validManagerTask=new ManagerTask('Task manager 1',new Date('2024/09/03'),projectType.INTERNAL_PRODUCT,validAssignee);            
            const validProgrammerTask = new ProgrammerTask('Programmer Task', new Date('2024/09/03'), programmerLevel.JUNIOR, validAssignee1);
            
            taskBoard.add(validTask);
            taskBoard.add(validManagerTask);
            taskBoard.add(validProgrammerTask);
            expect(taskBoard.items).toHaveLength(3);
            expect(taskBoard.items).toContain(validTask);
            expect(taskBoard.items).toContain(validManagerTask);
            expect(taskBoard.items).toContain(validProgrammerTask);
            
        });
    });
});