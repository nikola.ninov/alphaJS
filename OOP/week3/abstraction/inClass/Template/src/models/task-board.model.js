import { Board } from './board.model.js';
import { Task } from './board-items/task.model.js';

export class TaskBoard extends Board {
    constructor() {
        super();
    }

    add(item) {
        if (item instanceof Task) {
          super.add(item);
        } else {
          throw new Error('Only instances of Task can be added to the TaskBoard.');
        }
    }
}
