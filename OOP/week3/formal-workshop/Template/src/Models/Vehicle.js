import { Comment } from './Comment.js';
import { VehicleType } from './VehicleType.js';

export class Vehicle {

	/**
	 * The list of comments for the vehicle.
	 *
	 * @type {Comment[]}
	 */
	#comments = [];
	static MAKE_MIN_LENGTH = 2;
	static MAKE_MAX_LENGTH = 15;
	static MODEL_MIN_LENGTH = 1;
	static MODEL_MAX_LENGTH = 15;
	static PRICE_MIN_VALUE = 0.0;
	static PRICE_MAX_VALUE = 1000000.0;
	#make;
	#model;
	#price;
	#wheels;
	#type;
	/**
	 * @abstract
	 *
	 * @param {string} make - The make of the vehicle.
	 * @param {string} model - The model of the vehicle.
	 * @param {number} price - The price of the vehicle.
	*/
	constructor(type, make, model, price, wheels) {
	  this.make = make;
	  this.model = model;
	  this.price = price;
	  this.#wheels = wheels;
	  this.#type = type;
	}

	get make() {
	  return this.#make;
	}
	set make(value) {
	  if (value.length < Vehicle.MAKE_MIN_LENGTH || value.length > Vehicle.MAKE_MAX_LENGTH) {
	    throw new Error('Make length should be between 2 and 15 symbols');
	  }
	  this.#make = value;
	}

	get model() {
	  return this.#model;
	}
	set model(value) {
	  if (value.length < Vehicle.MODEL_MIN_LENGTH || value.length > Vehicle.MODEL_MAX_LENGTH) {
	    throw new Error('Make length should be between 2 and 15 symbols');
	  }
	  this.#model = value;
	}

	get price() {
	  return this.#price;
	}
	set price(value) {
	  if (value < Vehicle.PRICE_MIN_VALUE || value > Vehicle.PRICE_MAX_VALUE) {
	    throw new Error('Price range should be between 0.0 and 1000000.0');
	  }
	  this.#price = value;
	}
	get wheels() {
	  return this.#wheels;
	}
	get type() {
	  return this.#type;
	}

	addComment(value) {
	  this.#comments.push(value);
	}
	removeComment(commentIndex) {
	  this.#comments.splice(commentIndex, 1);
	}
	get comments() {
	  return this.#comments;
	}

	get printComments() {
	  if (this.#comments.length === 0) {
	    return '--NO COMMENTS--';
	  }
	  const builder = [];
	  builder.push('--COMMENTS--');
	  for (const comment of this.#comments) {
	    builder.push(comment.print());
	  };
	  return builder.join('\n');
	}

	print() {
	  return `${this.type}:\nMake: ${this.make}\nModel: ${this.model}\nWheels: ${this.wheels}\nPrice: $${this.price.toFixed(1)}\n${this.vehicleSpecifics}\n${this.printComments}`;
	}
}
