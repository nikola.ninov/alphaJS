const input = [
    '10',
    'AddWish Electric Scooter 2000Z;1536.50;Stefan',
    'AddWish Fortnite Skin;3000;Stefan',
    'AddWish AMD Radeon;4099.99;Pesho',
    'AddWish Apple AirPods;200000;Kiril',
    'AddWish Socks;10000;Tosho',
    'AddWish Swater;999;Stefan',
    'FindWishesByChild Stefan',
    'DeleteWishes Stefan',
    'FindWishesByChild Stefan',
    'FindWishesByPriceRange 100000;200000',
];
// const input1 = [
//     '8',
//     'AddWish Electric Scooter 2000Z;3500.05;Rayko Petrov',
//     'AddWish Fortnite Skin;3000;Rayko Petrov',
//     'AddWish AMD Radeon;16400;Hristo',
//     'AddWish Apple AirPods;21111.50;Nadya',
//     'FindWishesByChild Toshko',
//     'DeleteWishes Rayko Petrov',
//     'FindWishesByChild Rayko Petrov',
//     'FindWishesByPriceRange 5000;30000',
// ]

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
let n = +gets();
const wishesByChild = new Map();

for (let i = 0; i < n; i++) {
    const command = gets();
    const commandName = command.substring(0, command.indexOf(' '));
    const arguments = command.substring(command.indexOf(' ') + 1).split(';');


    if (commandName === 'AddWish') {
        let [item, price, childName] = arguments
        addWish(item, Number(price).toFixed(2), childName);
    } else if (commandName === 'FindWishesByChild') {
        let childName = arguments;
        findWishesByChild(childName[0]);
    } else if (commandName === 'DeleteWishes') {
        let childName = arguments;
        deleteWishes(childName[0]);
    } else if (commandName === 'FindWishesByPriceRange') {
        let [fromPrice, toPrice] = arguments;
        findWishesByPriceRange(Number(fromPrice), Number(toPrice));
    }
}
function addWish(item, price, child) {
    const wishKeyChild = child;
    if (wishesByChild.has(wishKeyChild)) {
        wishesByChild.get(wishKeyChild).push({ item, price, child });
        console.log('Wish added');
        return;
    }
    wishesByChild.set(wishKeyChild, [{ item, price, child }]);
    console.log('Wish added');
}

function deleteWishes(childName) {
    if (wishesByChild.has(childName)) {
        let count = wishesByChild.get(childName).length;
        wishesByChild.delete(childName);
        console.log(`${count} Wishes deleted`);
    } else {
        console.log('No Wishes found');
    }
}

function findWishesByChild(childName) {
    if (wishesByChild.has(childName)) {
        const elements = wishesByChild.get(childName).sort((a, b) => {
            const itemA = a.item.toUpperCase();
            const itemB = b.item.toUpperCase();
            if (itemA < itemB) {
                return -1;
            } else if (itemA > itemB) {
                return 1;
            } else {
                return 0;
            }
        });
        console.log(elements.map(wish => `{${wish.item};${wish.child};${wish.price}}`).join('\n'));        
    } else {
        console.log('No Wishes found');
    }
}

function findWishesByPriceRange(fromPrice, toPrice) {
    const matchingWishes = [];
    for (const wishArray of wishesByChild.values()) {
        for (const wish of wishArray) {
            if (wish.price >= fromPrice && wish.price <= toPrice) {
                matchingWishes.push(wish);
            }
        }
    }
    if (matchingWishes.length === 0) {
        console.log('No Wishes found');
        return;
    }
    matchingWishes.sort((a, b) => {
        const itemA = a.item;
        const itemB = b.item;
        if (itemA < itemB) {
            return -1;
        } else if (itemA > itemB) {
            return 1;
        } else {
            return 0;
        }
    });
    const formattedWishes = matchingWishes.map(wish => `{${wish.item};${wish.child};${wish.price}}`);
    console.log(formattedWishes.join('\n'));
}
// const n = input[0];
// let test = input.slice(1);
// const input1 = [
//     '8',
//     'AddWish Electric Scooter 2000Z;3500.05;Rayko Petrov',
//     'AddWish Fortnite Skin;3000;Rayko Petrov',
//     'AddWish AMD Radeon;16400;Hristo',
//     'AddWish Apple AirPods;21111.50;Nadya',
//     'FindWishesByChild Toshko',
//     'DeleteWishes Rayko Petrov',
//     'FindWishesByChild Rayko Petrov',
//     'FindWishesByPriceRange 5000;30000',
// ]
// const input = [
//     '10',
//     'AddWish Electric Scooter 2000Z;1536.50;Stefan',
//     'AddWish Fortnite Skin;3000;Stefan',
//     'AddWish AMD Radeon;4099.99;Pesho',
//     'AddWish Apple AirPods;200000;Kiril',
//     'AddWish Socks;10000;Tosho',
//     'AddWish Swater;999;Stefan',
//     'FindWishesByChild Stefan',
//     'DeleteWishes Stefan',
//     'FindWishesByChild Stefan',
//     'FindWishesByPriceRange 100000;200000',
// ];


// let print = this.print || console.log;
// let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
// const n = +gets();

// const wishList = new Map (); 
// let params = ''; 
// for (let i = 0; i < n; i++) {
    
//     let line = gets();
//     if (line.includes('AddWish')) {
//         params = line.slice(8).split(';');
//         addWish(params);
//     }
    
//     if (line.includes('DeleteWishes')) {
//         params = line.slice(13);
//         deleteWish(params);
//     }
    
//     if (line.includes('FindWishesByPriceRange')) {
//         params = line.slice(23).split(';');
//         findWishByPriceRange(params);
//     }
    
//     if (line.includes('FindWishesByChild')) {
//         params = line.slice(18);
//         findWishByName(params);
//     }    
// }
//  console.log(wishList);
// function addWish (params) {
//     const [string, float, name] = params;
 
//     if (wishList.has(name)) {
//         wishList.get(name).push({wish: string, price: Number(float).toFixed(2), name: name});
//     } else {
//         wishList.set(name, [{wish: string, price: Number(float).toFixed(2), name: name}]);
//     }
 
//     console.log('Wish added')
// }
 
// function deleteWish (name) {
 
//     if(wishList.has(name)) {
//         let count = wishList.get(name).length;
//         wishList.delete(name);
//         console.log(`${count} Wishes deleted`);
//     } else {
//         console.log(`No wishes found`)
//     }
// }
 
// function findWishByPriceRange (params) {
//     const [fromPrice, toPrice] = params; 
//     const matchingWishes = [];
 
//     for (const wishArray of wishList.values()) {
//         for (const wish of wishArray) {
//             if (wish.price >= Number(fromPrice) && wish.price <= Number(toPrice)) {
//                 matchingWishes.push(wish);
//             }
//         }
//     }
 
//     if (matchingWishes.length === 0) {
//         console.log('No Wishes found');
//         return;
//     }
 
//     matchingWishes.sort((a, b) => {
//         const itemA = a.item;
//         const itemB = b.item;
//         if (itemA < itemB) {
//             return -1;
//         } else if (itemA > itemB) {
//             return 1;
//         } else {
//             return 0;
//         }
//     });
 
//     const formattedWishes = matchingWishes.map(wish => `{${wish.wish};${wish.name};${wish.price}}`);
//     console.log(formattedWishes.join('\n'));
// }
// function findWishByName (name) { 
//     if(wishList.has(name)) {
//         wishList.get(name)
//         .sort((a, b) =>  a.wish < b.wish ? -1 : 1)
//         .forEach(el => console.log(`{${el.wish};${el.name};${el.price}}`));
//     } else {
//         console.log('No Wishes found');
//     }
// }