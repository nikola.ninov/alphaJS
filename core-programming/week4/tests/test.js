let book = {
    title: 'Harry Potter',
    author: 'J.K. Rowling',
    pages: 350,
    displayBookInfo: () => {
        return (`    Title: ${this.title}
    Author: ${this.author}
    pages: ${this.pages}`)
    }
};

let anotherBook = {
    title: 'Lord of the rings',
    author: 'John R.R. Tolkien',
    pages: 1350,
    displayBookInfo: () => {
        return (`    Title: ${this.title}
    Author: ${this.author}
    pages: ${this.pages}`)
    }
};

console.log(book['title']);
// console.log(book.displayBookInfo());
console.log(book.displayBookInfo());



let persons = [
    {
        name: 'John',
        age: 30,
        address: {
            city: 'Sofia',
            street: 'bul. Bulgaria'
        },
        pets: [
            {
                name: 'Barkley',
                breed: 'dog'
            },
            {
                name: 'Timmy',
                breed: 'cat'
            }
        ]
    },
    {
        name: 'Jayne',
        age: 33,
        address: {
            city: 'Sofia',
            street: 'bul. Bulgaria'
        },
        pets: [

            {
                name: 'Dogi',
                breed: 'dog'
            },
            {
                name: 'Cripsy',
                breed: 'cat'
            }
        ]
    }
]


const combined = persons.map(person => {
    const { pets } = person;
    console.log(pets);
    let result = pets.reduce((acc, curr) => {
        let name = curr.breed;
        console.log(name);
        if (!acc[name]) {
            acc[name] = []
        }
        acc[name].push(curr)
        return acc
    }, {})
    return result
})

console.log(JSON.stringify(combined));
