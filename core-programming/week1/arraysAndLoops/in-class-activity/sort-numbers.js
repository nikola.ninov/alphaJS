let input = [
    '123123 444444 55555 666666'
];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let num = gets().split(' ').map(Number);

let result = num.sort((a,b)=>a-b);
print(result.join(' '));