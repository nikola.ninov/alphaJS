// People with cities and population
/**
 * 
 * @param {{cities: Array<{ id: number, name: string }>, people: Array<{ name: string, city: number }>}} data 
 */
export default function (data) {
  // your code starts here
  let { people, cities } = data;

  const population = people.reduce((acc, p) => {
    let city = {};
    cities.forEach(el => {
      if (p.city === el.id) {
        city = el.name
      }
    });
    if (!acc[city]) {
      acc[city] = 0;
    }
    if (Object.keys(acc).includes(city)) {
      acc[city] = acc[city] + 1
    }
    return acc
  }, {});
  
  const combinedPeopleInfo = people.map(p => {
    const { name, city: cityId } = p;
    let city = cities.find(c => c.id === cityId);
    
    let pop = population[city.name];

    return {
      name: name,
      city: {
        name: city.name,
        population: pop
      }
    }

  })
  return combinedPeopleInfo
  // your code ends here
}
