import { Person } from "./person.js";

const john = new Person('Gosho');
john.sayName();
const pesho = new Person('Pesho');
pesho.sayName();

//this сочи към инстанцията на този клас през която е извикан този метод implicit binding