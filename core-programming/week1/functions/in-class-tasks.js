const arr = [1, 2, 3, 4, 5, 6];


const forEachFunction = (arr, func) => {
    for (const item of arr) {
        func(item);
    }
}
const checkFn = function (el) {
    return el % 2 === 0
}

const filter = (arr, checkFn) => {
    let result = [];
    for (const item of arr) {
        if (checkFn(item)) {
            result.push(item)
        }
    }
    return result;
}
const map = (arr) => {
    const arrTobeReturned = [];
    arr.forEach(el => {
        arrTobeReturned.push(el + 10);
    });
    return arrTobeReturned
}
const write = (arr) => {
    console.log(arr.join(' '));
}

let arr1 = map(arr);

forEachFunction(filter(arr1,checkFn), console.log)
