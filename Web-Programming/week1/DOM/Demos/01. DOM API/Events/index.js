const addTodo = () => {
  const input = document.querySelector('#text-input');
  const list = document.querySelector('#todos-list');
  const todo = document.createElement('li');

  todo.innerText = input.value;

  // Add todo to the list
  list.appendChild(todo);

  // Clear input
  input.value = '';
};


(() => {
  const btn = document.querySelector('#btn');
  btn.addEventListener('click', (event) => {
    console.log(event);
    addTodo();
  });
})();
