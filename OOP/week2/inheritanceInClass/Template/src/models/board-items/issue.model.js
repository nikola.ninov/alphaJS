import { issueStatus } from '../../common/issue-status.enum.js';
import { BoardItem } from './board-item.model.js';

export class Issue extends BoardItem {

    #createdOn;
    #resolvedOn;
    #description;
    #status;
    static DESCRIPTION_MIN_LENGTH=10;
    static DESCRIPTION_MAX_LENGTH=40;

     /**
     * Creates an instance of the Issue class.
     * @param {string} name - The name of the issue.
     * @param {string} description - The description of the issue.
     */    
    constructor(name, description) {
        super(name);
        this.description = description;
        this.#createdOn = new Date();
        this.#resolvedOn = null;
        this.#status = issueStatus.RAISED;

    }

    /**
     * Gets the date when the issue was created.
     * @returns {Date} The created date of the issue.
     */
    get createdOn() {
        return this.#createdOn;
    }

    /**
     * Gets the date when the issue was resolved.
     * @returns {Date|null} The resolved date of the issue, or null if not resolved.
     */
    get resolvedOn() {
        return this.#resolvedOn;
    }

    /**
     * Gets the description of the issue.
     * @returns {string} The description of the issue.
     */
    get description() {
        return this.#description;
    }

    /**
     * Sets the description of the issue.
     * @param {string} value - The new description of the issue.
     * @throws {Error} If the description is invalid.
     */
    set description(value) {
        if (typeof value !== 'string' || value.length < Issue.DESCRIPTION_MIN_LENGTH || value.length > Issue.DESCRIPTION_MAX_LENGTH) {
            throw new Error('Invalid description');
        }
        this.#description = value;
    }

    /**
     * Gets the status of the issue.
     * @returns {issueStatus} The status of the issue.
     */
    get status() {
        return this.#status;
    }

    /**
     * Resets the status of the issue to "RAISED".
     */
    reset() {
        this.#status = issueStatus.RAISED;
    }

    /**
     * Advances the status of the issue to "IN_REVIEW".
     */
    advance() {
        this.#status = issueStatus.IN_REVIEW;
    }

    /**
     * Completes the issue by setting the status to "RESOLVED" and setting the resolved date.
     */
    complete() {
        this.#status = issueStatus.RESOLVED;
        this.#resolvedOn = new Date();
    }

    /**
     * Returns a string representation of the issue.
     * @returns {string} The string representation of the issue.
     */
    toString() {
        let result = `* Issue *\n`;
        result += `${super.toString()}\n`;
        result += `Description: ${this.description}\n`;
        result += `Created on: ${this.createdOn.toLocaleString()}\n`;
        result += `Resolved on: ${this.resolvedOn ? this.resolvedOn.toLocaleString() : 'Not yet resolved'}`;
        return result;
    }
}
