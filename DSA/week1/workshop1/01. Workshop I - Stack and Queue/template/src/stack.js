import LinkedListNode from './linked-list-node.js';

export default class Stack {
    #top = null;
    #count = 0;


    get count() {
        return this.#count;
    }
    push(value) {
        const node = new LinkedListNode(value);
        node.next = this.#top;
        this.#top = node;
        this.#count++;
    }
    pop() {
        if (!this.#top) {
            throw new Error('The stack is empty');
        }
        const val = this.#top.value;
        this.#top = this.#top.next;
        this.#count--;
        return val;
    }
    peek() {
        if (!this.#top) {
            throw new Error('The Stack is empty');
        }
        return this.#top.value;
    }
    get isEmpty() {
        return !this.#top;
    }
}