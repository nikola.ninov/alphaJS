import { FileHandler } from './services/file-handler.js';

const tasks = [
  { id: 1, name: 'Task 1', status: 'done' },
  { id: 2, name: 'Task 2', status: 'pending' },
  { id: 3, name: 'Task 3', status: 'done' },
];

const reader = new FileHandler();
const fileS = reader.read('data/tasks.all.json');
fileS.then((file) => {
  const data = [file.filter(e => e.status === 'done'), file.filter(e => e.status === 'pending')];
  return data;
}).then((data) => {
  reader.write(`data/tasks.done.json`, data[0]);
  return data;
}).then((data) => {
  reader.write(`data/tasks.pending.json`, data[1]);
}).catch((error) => {  
  console.log(error);
  reader.write('logs/default.log', error.stack);
});


reader.read('data/fail-data.json').then((file) => {
  const data = [file.filter(e => e.status === 'done'), file.filter(e => e.status === 'pending')];
  return data;  
}).catch((error) => {  
  console.log(error);
  reader.write('logs/default.log', error.stack);
});
reader.write('data/tasks.txt', JSON.stringify(tasks))
  .then(() => {    
    console.log('Successfully write');
  })
  .catch((error) => {
    console.error('Error writing file:', error);
    reader.write('logs/default.log', error.stack);
  });

reader.write('nonexistent-folder/tasks.txt', JSON.stringify(tasks))
  .then(() => {    
    console.log('Successfully write');
  })
  .catch((error) => {    
    console.log(error);
    reader.write('logs/default.log', error.stack);
  });
