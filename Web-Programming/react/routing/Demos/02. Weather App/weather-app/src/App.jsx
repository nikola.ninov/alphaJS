import './App.css'
import Navbar from './components/Navbar/Navbar'
import { Route, Routes } from 'react-router-dom'
import Home from './views/Home/Home'
import About from './views/About/About'
import LogIn from './views/LogIn/LogIn'
import SignUp from './views/SignUp/SignUp'
import Landing from './views/Landing/Landing'
import { AuthContext } from './context/AuthContext'
import { useState } from 'react'
import AuthenticatedRoute from './hoc/AuthenticatedRoute'


function App() {


  const [isLoggedIn, setLoggedIn] = useState(false);

  return (
    <div className='app'>

      <AuthContext.Provider value={{ isLoggedIn, setLoggedIn: setLoggedIn }}>
        <Navbar></Navbar>
        <Routes>
          <Route path="/" element={<Landing />}></Route>
          <Route path="/home" element={<AuthenticatedRoute><Home /></AuthenticatedRoute>}></Route>
          <Route path="/about" element={<About />}></Route>
          <Route path="/log-in" element={<LogIn />}></Route>
          <Route path="/sign-up" element={<SignUp />}></Route>
          <Route path="*" element={<Error />}></Route>
        </Routes>
      </AuthContext.Provider>
    </div>
  )
}

export default App
