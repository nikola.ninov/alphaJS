const recursiveLoop = (start, end) => {
    if (start === end) {
        return;;
    }

    console.log(start);
    recursiveLoop(start + 1, end);
}

recursiveLoop(0, 5);


// n!=n*(n-1)

function factorial(number) {
    if (number <= 1) {
        return 1
    }
    return number * factorial(number - 1)
}

console.log(factorial(5));


// string reverse
function reverseString(string) {
    if (string.length === 1 || string.length === 0) {
        return string;
    }

    const first = string[0]
    const rest = string.slice(1)

    return reverseString(rest) + first;
}

console.log(reverseString('telerik'));