let input = [
    "4",
    "1 2 3 2 1",
    "2 1",
    "1 2 2 1",
    "4",
];


let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);


let n = +gets();

for (let i = 0; i < n; i++) {

    let element = gets().split(' ').map(Number);

    if (element.length === 1) {
        print('Yes');
    }else if (element.length % 2 === 0) {

        let firstArr = element.slice(0, element.length / 2);
        let secondArr = element.slice(element.length / 2, element.length).reverse();
        
        if (arrayEquals(firstArr, secondArr)) {
            print('Yes')
        } else {
            print('No');
        }
    } else if (element.length % 2 === 1) {
            let firstArr = element.slice(0, element.length / 2);
            let secondArr = element.slice(element.length / 2 + 1, element.length).reverse();
            if (arrayEquals(firstArr, secondArr)) {
                print('Yes')
            } else {
                print('No');
            }
        }
}

function arrayEquals(a, b) {
    return Array.isArray(a) && Array.isArray(b) &&
        a.length === b.length && a.every((val, index) => val === b[index]);
}
