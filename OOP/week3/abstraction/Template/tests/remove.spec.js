/* eslint-disable no-undef */
import { remove } from './../src/remove.js';

describe('remove', () => {
  it('should produce correct string', () => {
    // Arrange
    const test = 'telerik academy';
    const expected = 'tele academy';

    // Act
    const actual = remove(test, 4, 7);

    // Assert
    expect(actual).toBe(expected);
  });

  it('should produce correct string (only start index given)', () => {
    // Arrange
    const test = 'Hello, world!';
    const startIndex = 7;
    const expectedString = 'Hello, ';
    // Act
    const result = remove(test, startIndex);
    // Assert
    expect(result).toBe(expectedString);
    expect(remove('abcdef', 3)).toBe('abc');  
  });
  it('throws an error if the input string is undefined or null', () => {
    expect(() => remove(undefined, 0, 3)).toThrow('str must be defined');
    expect(() => remove(null, 0, 3)).toThrow('str must be defined');
  });
  
  it('throws an error if start is less then 0',()=>{
    expect(()=>remove('abcdef',-1,2)).toThrow('start and end must be within the boundaries of the string');
  })
  it('throws an error if end is greater than the length of the string',()=>{
    expect(()=>remove('abcdef',1,7)).toThrow('start and end must be within the boundaries of the string');
  })
  it('throws an error if end is less than  the start',()=>{ 
    expect(()=>remove('abcdef',3,3)).toThrow('end must be greater than start');
    expect(()=>remove('abcdef',3,2)).toThrow('end must be greater than start');
  })
});
