import { useState } from "react";

const MyButton = () => {

    const [state, setState] = useState(0);

    function handleClick() {
        setState(state + 1);
    }
    
    return (
        <>
            <button onClick={handleClick}> MyButton, state: {state} </button>
        </>
    )
}

export default MyButton