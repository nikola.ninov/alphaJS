import { useParams, useNavigate } from "react-router-dom";

const Details = (props) => {
    const { id } = useParams();
    const navigate = useNavigate();

    const element = props.elements.find(el => el.id === +id);
    const handleGoBack = () => {
        navigate(-1);    
    }

    return (
        <div>
            <h1>Details Page</h1>
            <h2>{element.name}</h2>
            <p>{element.description}</p>
            <button onClick={handleGoBack}>Go Back</button>
        </div>
    )
}
export default Details;