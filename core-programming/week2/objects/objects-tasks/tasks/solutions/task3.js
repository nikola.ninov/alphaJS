// Graduates
/**
 * 
 * @param {{
    course: string;
    minPassingGrade: number;
    students: {
        name: string;
        grades: number[];
    }[];
}} data 
 */
export default function (data) {
  // your code starts here
  let { course, minPassingGrade, students } = data;

  let graduates = [];
  let courseAverage = 0;

  students.forEach(el => {

    let gradesAvg = 0;

    el.grades.forEach(grade => {
      gradesAvg += grade;
    });

    gradesAvg = Number(((gradesAvg / el.grades.length)).toFixed(1));

    if (gradesAvg.toFixed(1) >= minPassingGrade) {
      graduates.push({ name: el.name, score: Number(gradesAvg) });
      courseAverage += gradesAvg;
    }
  });

  if (courseAverage !== 0 && students.length !== 0) {
    courseAverage = Number((courseAverage / graduates.length).toFixed(1))
  }

  let result = { course, minPassingGrade, courseAverage, graduates };

  return result;

  // your code ends here
}

