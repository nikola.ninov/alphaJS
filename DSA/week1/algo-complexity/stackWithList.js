class ListNode {
    constructor(value, next) {
        this.value = value;
        this.next = next || null;
    }
}

class StackList {

    #top = null;

    push(value) {
        const node = new ListNode(value);
        node.next = this.#top;
        this.#top = node;
    }
    pop() {
        if (!this.#top) {
            throw new Error('stack is empty')
        }

        const val = this.#top.value;
        this.#top = this.#top.next;
        return val
    }

    peek() {
        if (!this.#top) {
            throw new Error('stack is empty')
        }

        return this.#top.value();
    }

}