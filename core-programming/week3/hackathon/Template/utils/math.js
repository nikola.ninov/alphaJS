// easy

const min = (array) => {
  // TODO
  let result = 0;
  const minNumber = Number.MIN_SAFE_INTEGER;
  for (let i = 0; i < array.length - 1; i++) {
    if (array[i] < minNumber) {
      result = array[i];
    }
  }
  return result;
};

const sum = (array) => {
  // TODO
};

// medium

const average = (array) => {
  // TODO
};

const pow = (number, power) => {
  // TODO
};

// hard

const isPrime = (number) => {
  // TODO
};

const swapWholeAndRemainder = (number) => {
  // TODO
};

export { min, sum, average, pow, isPrime, swapWholeAndRemainder };
