//push pop peek operations 0(1)

class StackArray {
    #items = [];

    push(value) {
        this.#items.push(value);
    }
    pop() {
        if (this.isEmpty()) {
            throw new Error('Stack is empty')
        }
        this.#items.pop();
    }
    peek() {
        if (this.isEmpty()) {
            throw new Error('Stack is empty')
        }
        return this.#items[this.#items.length - 1]
    }
    isEmpty() {
        return this.#items.length === 0
    }
}
const stack = new StackArray();
stack.push(1);
stack.push(2);
stack.push(3);
stack.push(4);
console.log(stack.peek());
stack.pop();
console.log(stack.peek());