const getName = person => person.name;
const uppercase = string => string.toUpperCase();
const get3Characters = string => string.substring(0, 3);

const pipef = (fc1, fc2, fc3) => {
    return (person) => {
        const result1 = fc1(person);
        const result2 = fc2(result1);
        return fc3(result2)
    }
}
const pipeFS = (fc1, fc2, fc3) => (person) => {
    const result1 = fc1(person);
    const result2 = fc2(result1);
    return fc3(result2)
}



// pipef(
//     getName,
//     uppercase,
//     get3Characters
// )({ name: 'Pesho' });


console.log(pipef(getName, uppercase, get3Characters)({ name: 'Pesho' }));

// console.log(pipeFS(getName, uppercase, get3Characters)({ name: 'Pesho' }));
// console.log(pipeIIFE);
// console.log(pipe(getName, uppercase, get3Characters)({ name: 'Pesho' }));
// console.log(pipeGeeky(getName, uppercase, get3Characters)({ name: 'Pesho' }));