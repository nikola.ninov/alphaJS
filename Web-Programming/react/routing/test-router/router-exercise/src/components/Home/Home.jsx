import { Link } from "react-router-dom";
import './Home.css';




const Home = (props) => {

    return (
        <div className="home">
            <h1>Home</h1>
            <h3>List of elements</h3>
            <ul>
                {props.elements.map((element) =>(
                    <li key={element.id}>
                        <Link to={`/details/${element.id}`} id={element.id}>{element.name}</Link>
                    </li>    
                ))
            }
            
            </ul>


        </div>
    )
}
export default Home;
