const person = {
    address: {
        city: 'Sofia',
        street: {
            name: 'Al. Malinov Blvd.',
            number: 31
        }
    }
}


// const headNode = {
//     value: 1,
//     next: {
//         value: 2,
//         next: {
//             value: 3,
//             next: null
//         }
//     }
// }

// const headnode1 = { value: 1 };
// headnode1.next = { value: 2 };
// headnode1.next.next = { value: 3 };
// headnode1.next.next.next = {
//     value: 4, next: null
// }

// const traverse = (headNode) => {
//     let current = headNode;
//     while (current !== null) {
//         console.log(current.value);
//         current = current.next;
//     }
// }


// const thirdNode = {
//     value: 3,
//     next: null
// }

// const secondNode = {
//     value: 2,
//     next: thirdNode
// }
// const firstNode = {
//     value: 1,
//     next: secondNode
// }

// const sumValuesOfLinkedList = (node) => {
//     let current = node;
//     let sum = 0;

//     while (current !== null) {
//         sum += current.value
//         current = current.next;
//     }
//     return sum;
// }

// console.log(sumValuesOfLinkedList(firstNode));
let friends = "";
const jonas = {
    firstName: 'Jonas',
    lastName: 'Schmedted',
    birthYear: 1991,
    job: 'teacher',
    friends: ['Michael', 'Peter', 'Steven'],
    hasDriversLicense: true,

    calcAge: function () {
        return 2037 - this.birthYear
    }

};

Object.keys(jonas).forEach(el => {
    if (el === 'friends') {
        friends = jonas[`${el}`]
    }
});

console.log(friends);