import Header from "./components/Header/Header";
import Posts from "./components/Posts/Posts";
import Post from "./components/Posts/Post";
import Home from './components/Home';
import NotFound from './components/NotFound';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

const App = () => {

  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<Navigate replace to="/home" />} />
        <Route path="home" element={<Home />} />
        <Route path="posts" element={<Posts />} />
        <Route path="posts/:id" element={<Post />} />
        <Route path="*" component={NotFound} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
