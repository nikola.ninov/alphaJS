export class Person {
    firstName;
    #lastName;
    #objId;
    static id = 0;

    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.#lastName = lastName;
        Person.id++;
        this.#objId = Person.id
        console.log(`New person with object ${this.#objId} was created`);
    }

    getFirstName() {
        return this.firstName;
    }
    getLastName() {
        return this.#lastName;
    }

    setFirstName(firstName) {
        this.firstName = firstName
    }
    setLastName(lastName) {
        if (!lastName || lastName.length < 10 || lastName.length > 100) {
            throw new Error('Invalid data');
        }
        this.#lastName = lastName
    }
}