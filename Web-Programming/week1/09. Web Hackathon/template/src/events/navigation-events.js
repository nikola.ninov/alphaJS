import { CATEGORIES, CONTAINER_SELECTOR, HOME, ABOUT, FAVORITES } from '../common/constants.js';
import { toHomeView } from '../views/home-view.js';
import { toMoviesFromCategoryView } from '../views/movie-views.js';
import { toCategoriesView } from '../views/category-view.js';
import { getFavorites } from '../data/favorites.js';
import { loadCategories, loadMovies, loadCategory } from '../requests/request-service.js';
import { toAboutView } from '../views/about-view.js';
import { toFavoritesView } from '../views/favorites-view.js';
import { q, setActiveNav } from './helpers.js';

// public API
export const loadPage = (page = '') => {

  switch (page) {

    case HOME:
      setActiveNav(HOME);
      return renderHome();
    case CATEGORIES:
      setActiveNav(CATEGORIES);
      return renderCategories();
    case ABOUT:
      setActiveNav(ABOUT);
      return renderAbout();
    case FAVORITES:
      setActiveNav(FAVORITES);
      return renderFavorites();
    // missing partial implementation

    /* if the app supports error logging, use default to log mapping errors */
    default: return null;
  }

};

export const renderMovieDetails = (id = null) => {
  // missing implementation
};

export const renderCategory = (categoryId = null) => {  
  
};

// private functions

const renderHome = () => {
  q(CONTAINER_SELECTOR).innerHTML = toHomeView();
};

const renderCategories = () => {
  const categories = loadCategories();
  q(CONTAINER_SELECTOR).innerHTML = toCategoriesView(categories);
};

const renderFavorites = () => {
  const favorites = getFavorites();
  q(CONTAINER_SELECTOR).innerHTML = toFavoritesView(favorites);
};

const renderAbout = () => {
  q(CONTAINER_SELECTOR).innerHTML = toAboutView();
};
