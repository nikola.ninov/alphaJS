import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import PostDetails from './PostDetails';

const Post = () => {
  const [postData, setPostData] = useState(null);
  const { id } = useParams(); // get postId from path posts/:id i.e. posts/1

  useEffect(() => {
    fetch(`http://jsonplaceholder.typicode.com/posts/${id}`, { mode: 'cors' })
      .then(response => response.json())
      .then(data => setPostData(data));
  }, [id]);

  return postData && <PostDetails {...postData} isIndividual={true} />;
};

export default Post;
