const symbol = Symbol();
const symbol1 = Symbol('foo');
const symbol2 = Symbol('bar');
console.log(symbol, symbol1, symbol2);
console.log(typeof symbol);
console.log(symbol.description);
console.log(symbol1.description);
console.log(Symbol('symbol') === Symbol('symbol'));
const user = {
    [Symbol('id')]: 1,
    name: 'John Doe',
    email: 'john@gmail.com'
}
user.id = 2
//symbols are not enumerable
console.log(user[Symbol('id')]);
console.log(Object.values(user));
for (const key in user) {
    console.log(key);
}
//getOwnPropertSYbols
console.log(Object.getOwnPropertySymbols(user));
//Symbol.for()
const sym3 = Symbol.for('foo')
const sym4 = Symbol.for('foo')
console.log(sym3 === sym4);
console.log(Symbol.keyFor(sym3));
// console.log(Symbol.keyFor(sym1));
console.log(Object.getOwnPropertyNames(Symbol));

// ITERATORS

// const app = {
//     nextIndex: 0,
//     teams: ['Red Sox', 'Yankees', 'Astros', 'Dodgers'],
//     next() {
//         if (this.nextIndex >= this.teams.length) {
//             return { done: true }
//         }
//         const returnValue = { values: this.teams[this.nextIndex], done: false }
//         this.nextIndex++;
//         return returnValue;
//     }
// }

// console.log(app.next());
// console.log(app.next());
// console.log(app.next());
// console.log(app.next());
// console.log(app.next());

// const app = {
//     teams: ['Red Sox', 'Yankees', 'Astros', 'Dodgers'],
//     [Symbol.iterator]: function () {
//         let index = 0
//         return {
//             next: () => {
//                 return index < this.teams.length
//                     ? { value: this.teams[index++], done: false }
//                     : { done: true }
//             }
//         }
//     }
// }
// const iterator = app[Symbol.iterator]();
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());

// for (const team of app) {
//     console.log(team);
// }

//GENERATORS generate iterators



// const teams = ['Red Sox', 'Yankees', 'Astros', 'Dodgers'];


// function* simpleGen() {
//     yield 1
//     yield 2
//     yield 3
// }

// const generatorObj = simpleGen();
// const generatorObj1 = simpleGen();
// // const obj=generatorObj.next();
// console.log(generatorObj.next());
// console.log(generatorObj.next());
// console.log(generatorObj1.next());
// console.log(generatorObj1.next());


function* simpleGenId() {
    let id = 1;
    while (true) {
        yield id //next iteration will start after it will iterate +1 and will do another while loop
        id++
    }
}

const generatorObj2 = simpleGenId();

console.log(generatorObj2.next());
console.log(generatorObj2.next());
console.log(generatorObj2.next());
console.log(generatorObj2.next());
console.log(generatorObj2.next());
console.log(generatorObj2.next());
console.log(generatorObj2.next());
console.log(generatorObj2.next());

// // SETS 

// const set = new Set([1, 2, 2, 3, 4]);
// set.add(5);

// console.log(set);
// console.log(set.has(3));
// console.log(set.has(5));

// set.delete(5);

// console.log(set.has(5));

// const setArr = Array.from(set)
// console.log(setArr);

// for (const item of set) {
//     console.log(item);
// }

// const iterator = set.values();

// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());

// set.clear();
// console.log(set);

//MAPS

// const map = new Map();

// map.set('name', 'John');

// map.set(1, 'red');
// map.set(2, 'blue');

// console.log(map.get('name'));
// console.log(map.get(1));
// console.log(map.get(2));
// console.log(map.size);
// console.log(map.has('name'));
// console.log(map.has(1));
// console.log(map.has(3));

// console.log(map.has(2));
// map.delete(2)

// console.log(map.has(2));
// console.log(map);

// const peopleMap = new Map();
// peopleMap.set('brad', { phone: '555-555-5555', email: 'john@hh.gg' })
// peopleMap.set('samie', { phone: '777-777-7777', email: 'samie@hh.gg' })
// peopleMap.set('sonia', { phone: '666-666-6666', email: 'sonia@hh.gg' })
// console.log(peopleMap);

// peopleMap.forEach(p => {
//     console.log(p.email);
// })


// console.log(peopleMap.keys());
// console.log(peopleMap.values());
// console.log(peopleMap.entries());
// const iterator = peopleMap.values();
// console.log(iterator.next());