import { getSearchedGifs } from '../request/request-service.js';
import { toAllSearchedElements } from '../views/allSearchedElements-view.js';

export const searchGif = async (searchTerm) => {
    const dataFromSearchEvent = await getSearchedGifs(searchTerm);
    const all=toAllSearchedElements(dataFromSearchEvent);
    
    const main = document.querySelector('main');
    main.innerHTML = all;
};