/** The Board class holds tasks */
export class Board {

    /**The array that holds Tasks */
    _tasks;

    /**
    * Constructs a new instance of the Board class.
    */
    constructor() {
        this._tasks = [];
    }

    /**
  * Adds a new task to the board, only if it is not already present.
  * @param {Task} task - The task to be added.
  * If the task already exists on the board should type on the console Task already exist.
  */
    add(task) {
        if (!this._tasks.includes(task)) {
            this._tasks.push(task);
        } else {
            console.log('Task already exist');
            // I've prefer here to be throw error but wont work for the task
        }
    }
    /**
    * Remove a  task from the board, only if the task exist.
    * @param {Task} task - The task to be added.
    * If the task doesn't exists on the board should type on the console Task does not exist on the board.
    */
    remove(task) {
        const index = this._tasks.indexOf(task);
        if (!(index === -1)) {
            this._tasks.splice(index, 1);
        } else {
            console.log('Task does not exist on the board.');
            // I've prefer here to be throw error but wont work for the task
        }

    }
    /**
     * Returns a string representation of the board and its tasks.
     * If board is empty should return : No tasks at the moment.
     * @returns {string} Returns string representing on the board
     */
    toString() {
        let result = '---Task Board---\nTasks:\n';

        if (this._tasks.length === 0) {
            result += 'No tasks at the moment.';
        } else {
            this._tasks.forEach(t => {
                result += t.toString();
                result += '--------\n';
            });
        }
        return result;
    }
}