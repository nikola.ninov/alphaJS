// Clone Objects

export default function (data) {
  const resultObject = {};

  for (let key in data) {
      if (Array.isArray(data[`${key}`])) {
          resultObject[`${key}`] = [...data[`${key}`]];
      } else
          if (typeof data[`${key}`] === 'object' && data[`${key}`] !== null) {
              resultObject[`${key}`] = Object.assign({}, data[`${key}`]);
          } else
           {
              resultObject[`${key}`] = data[`${key}`];
          }
  }
  return resultObject;
}
