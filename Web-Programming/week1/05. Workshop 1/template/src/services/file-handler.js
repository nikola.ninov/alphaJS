/* eslint-disable consistent-return */
/* eslint-disable no-shadow */
import { error } from 'console';
import fs from 'fs';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

export class FileHandler {
  read(filename = '') {
    filename = join(__dirname, '..', filename);
    return new Promise((resolve, reject) => {
      fs.readFile(filename, 'utf-8', (error, data) => {
        if (error) {
          return reject(error);
        } else {
          let tasks;
          try {
            tasks = JSON.parse(data);
            console.log('Successfully read');
          } catch (error) {
            return reject(error);
          }
          resolve(tasks.tasks);
        }
      });
    });
  }

  write(filename = '', data = '') {
    filename = join(__dirname, '..', filename);
    return new Promise((resolve, reject) => {
      fs.writeFile(filename, JSON.stringify(data) + '\n', { encoding: 'utf8', flag: 'a+' }, (error) => {
        if (error) {
          return reject(error);
        } else {
          resolve(data);
        }
      });
    });
  }
}
