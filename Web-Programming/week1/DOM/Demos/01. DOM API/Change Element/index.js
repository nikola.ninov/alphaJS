const changeElement = () => {
  const header = document.querySelector('#header');

  header.style.backgroundColor = 'red';
  header.style.color = 'white';
};

const resetElement = () => {
  const header = document.querySelector('#header');

  header.style.backgroundColor = 'white';
  header.style.color = 'black';
};
