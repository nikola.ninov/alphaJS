let input = [
    'Append Steven',
    'Examine 1',
    'Find Ina',
    'Append Niki',
    'Insert 0 Peter',
    'Append Nadya',
    'Insert 3 Grigor',
    'Examine 5',
    'Append Asya',
    'Insert 4 Steven',
    'Append Steven',
    'Find Asya',
    'Find Steven',
    'Examine 3',
    'Find Peter',
    'Examine 4',
    'Find Steven',
    'Insert 1 Ina',
    'End'
];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
const commands = [];
let inputLine;

while ((inputLine = gets()) !== 'End') {
    const [command, ...values] = inputLine.split(' ');
    const value = values.join(' ');
    commands.push({ command, value });
}


class Queue {
    constructor() {
      this.queue = [];
      this.counts=new Map();
    }  
    append(name) {
      this.queue.push(name);
      this.counts.set(name, (this.counts.get(name) || 0) + 1);
      console.log("OK");
    }  
    insert(position, name) {
      if (position < 0 || position > this.queue.length) {
        console.log("Error");
      } else {
        this.queue.splice(position, 0, name);
        this.counts.set(name, (this.counts.get(name) || 0) + 1);
        console.log("OK");
      }
    }

    find(name) {
        const count = this.counts.get(name) || 0;
      console.log(count);
    }

    examine(count) {
      if (count <= 0 || count > this.queue.length) {
        console.log("Error");
      } else {
        const examinedPeople = this.queue.splice(0, count);
        for (const name of examinedPeople) {
            this.counts.set(name, (this.counts.get(name) || 0) - 1);
          }
        console.log(examinedPeople.join(" "));
      }
    }
  }

  const queue = new Queue();
  commands.forEach(({ command, value }) => {
    if (command === 'Append') {
        queue.append(value);
    }
    if (command === 'Examine') {    
        const count = Number(value);
        queue.examine(count);
    }
    if (command === 'Find') {
        queue.find(value)
    }
    if (command === 'Insert') {  
        const [position, name] = value.split(' ');
        queue.insert(Number(position), name)
    }
})