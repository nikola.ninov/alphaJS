let input1 = [
    '5 10',
    '1000000010',
    '1111000011',
    '1000000000',
    '1100001000',
    '1000011100'
];
let input = [
    '4 4',
    '0000',
    '0110',
    '0110',
    '0000'
]

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
let [N, M] = gets().split(' ').map(Number);
let matrix = [];
for (let i = 0; i < N; i++) {
    matrix[i] = gets().split('').map(Number);
}
function calculateConquestSizes(matrix) {
    const visited = Array(N).fill().map(() => Array(M).fill(false));
    const conquestSizes = [];
    function isValidCell(row, col) {
        return row >= 0 &&
            row < N &&
            col >= 0 &&
            col < M;
    }

    function dfs(row, col, count) {
        visited[row][col] = true;
        count++;
        const directions = [
            [1, 0], // Down
            [-1, 0], // Up
            [0, 1], // Right
            [0, -1] // Left
        ];
        for (const [x, y] of directions) {
            const newRow = row + x;
            const newCol = col + y;

            if (isValidCell(newRow, newCol) &&
                matrix[newRow][newCol] === 1 &&
                !visited[newRow][newCol]
            ) {
                count = dfs(newRow, newCol, count);
            }
        }
        return count;
    }

    for (let i = 0; i < N; i++) {
        for (let j = 0; j < M; j++) {
            if (matrix[i][j] === 1 && !visited[i][j]) {
                const conquestSize = dfs(i, j, 0);
                conquestSizes.push(conquestSize);
            }
        }
    }
    return conquestSizes.sort((a, b) => b - a);
}

const result = calculateConquestSizes(matrix);
result.forEach(size => print(size));