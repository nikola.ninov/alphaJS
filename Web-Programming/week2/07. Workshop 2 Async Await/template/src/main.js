import { AddTaskCommand } from './commands/add-task-command.js';
import { RemoveTaskCommand } from './commands/remove-task-command.js';
import { UpdateTaskCommand } from './commands/update-task-command.js';
import { ListTasksCommand } from './commands/list-tasks-command.js';
import { TaskEngine } from './engine/engine.js';
import { ConsoleIterator } from './services/console-iterator.js';
import { TaskRegistry } from './services/task-registry.js';


//services initialized 
const taskRegistry = new TaskRegistry();
const consoleIterator = new ConsoleIterator();
const taskEngine = new TaskEngine();


const removeTaskCommand = new RemoveTaskCommand(taskRegistry);
const addTaskCommand = new AddTaskCommand(taskRegistry);
const updateTaskCommand = new UpdateTaskCommand(taskRegistry);
const listTaskCommand = new ListTasksCommand(taskRegistry);

taskEngine.addCommand('add', addTaskCommand);
taskEngine.addCommand('remove', removeTaskCommand);
taskEngine.addCommand('update', updateTaskCommand);
taskEngine.addCommand('list', listTaskCommand);


(async () => {
  for await (const line of consoleIterator) {


    // line.split('\n');
    // console.log(line);

      const [command, ...args] = line.split(' ');

      await taskEngine.executeCommand(command, ...args);
  }
})();



