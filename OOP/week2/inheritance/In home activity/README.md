<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## 1. Project information
Let's practice **inheritance** and **composition**.

## 2. Inheritance
- This is a basic technique that allows us to reuse code,
- We have parent-child relationship, where the child essentially *copies* code from the parent

### 2.1 Define the 'Base' class - the Person
Let's build our parent class. Open `inheritance.js` where you will see the class defined. 
1. Add a constructor that accepts `firstName` and `lastName` and sets them to the respective properties
2. Add a method `introduce()` that returns a string in the following format .`Hello, my name is {firstName} {lastName}.`
3. Test your code:
    ```js
    const mark = new Person('Mark', 'Williams');
    const introduction = mark.introduce();
    console.log(introduction); // Hello, my name is Mark Williams.
    ```

### 2.2 Define a child class - the Athlete
We will create a class Athlete that also plays a specific sport. 
1. First, we need to extend:
    ```js
    class Athlete extends Person {
        // ...
    }
    ```
2. Now, we need to define a constructor that accepts the sport: However, a child constructor **MUST** call a parent constructor and the parent constructor **EXPECTS** `firstName` and `lastName` - so we need these when constructing an Athlete, but only to pass the to the person!
    ```js
    class Athlete extends Person {
        constructor(firstName, lastName, sport) {
            super(firstName, lastName); // pass these to the parent constructor - keyword "super()"
            this.sport = sport; // this property is specific only to Athlete
        }
    ```
3. Now, let's add a method - play() - it will return string in the format `Playing some {sport}.`;
    ```js
    const mark = new Athlete('Mark', 'Williams', 'snooker');

    const introduction = mark.introduce(); // this method is 'inherited' and exists both for Person and Athlete
    console.log(introduction); // Hello, my name is Mark Williams.

    const playing = mark.play(); // this method exists only for Athlete, but not for Person
    console.log(playing); // Playing some snooker.
    ```
4. We can actually add the athlete `sport` as part of their introduction. There are two steps:
    1. Reuse parent method.
    2. Extend parent method with some more logic.

    ```js
    // in Athlete class:
    introduce() {
        const baseMessage = super.introduce(); // call the parent introduce
        const childMessage = `I am a ${this.sport} player.`; // adds something from the child

        return baseMessage + ' ' + childMessage;
    }
    ```
5. Let's test everything:
    ```js
    const john = new Person('John', 'Travolta');
    const johnsIntro = john.introduce(); // person's introduce
    console.log(johnsIntro);
    // Hello, my name is John Travolta.

    const mark = new Athlete('Mark', 'Williams', 'snooker');
    const marksIntro = mark.introduce(); // athlete's introduce
    console.log(marksIntro);
    // Hello, my name is Mark Williams. I am a snooker player.

    const playing = mark.play(); // mark can also play (he is an Athlete), but john can't!
    console.log(playing);
    // Playing some snooker.
    ```

## 3. Composition
- Composing classes means that we can use one class instance inside another class instance. It sounds difficult but it is not.
- Composition also allows code reuse (just like inheritance)
- When we compose, we can change which class we compose with at runtime!

## 3.1 Compose a game
Let's create a Game class - it will represent a game of any sport between any two players.
1. Open the `composition.js` file - you will find the Game class defined, just needs a little finishing
2. We need players - but we already have an Athlete class, so we can use that. Export it from `inheritance.js` and import it in `composition.js`
3. Our game needs a constructor - to determine which two players will be playing.
    ```js
    class Game {
        constructor() {
            this.playerOne = new Athlete('Mark', 'Williams', 'snooker');
            this.playerTwo = new Athlete('John', 'Higgins', 'snooker');
        }
    }
    ```
4. As you can notice, properties are not limited to `strings` and `numbers`, but can be other class instances too!
5. Define a `start()` method - the players make an introduction and then start the game!
    ```js
    // inside Game class
    start() {
        console.log(this.playerOne.introduce()); // mark's intro
        console.log(this.playerTwo.introduce()); // john's intro

        console.log(`${this.playerOne.firstName} and ${this.playerTwo.firstName} just started their match!`);
    }
    ```
6. Let's test everything:
    ```js
    const game = new Game();
    game.start();
    // Hello, my name is Mark Williams. I am a snooker player.
    // Hello, my name is John Higgins. I am a snooker player.
    // Mark and John just started their match!
    ```
## 3.2 Create an Even Better Composition
- Our game class is great, but has one major flaw - only Mark and John will ever play snooker. What if we need games with other players?

1. We can do that and it's quite simple - just pass the players to the constructor!
    ```js
    constructor(playerOne, playerTwo) {
        // the players can now be whoever we want them to be
        this.playerOne = playerOne; 
        this.playerTwo = playerTwo;
    }
    ```
2. Let's test our code now:
    ```js
    const mark = new Athlete('Mark', 'Williams', 'snooker');
    const john = new Athlete('John', 'Higgins', 'snooker');
    const steve = new Athlete('Steve', 'Davis', 'snooker');

    const game1 = new Game(mark, john);
    game1.start();
    const game2 = new Game(mark, steve);
    game2.start();
    const game3 = new Game(steve, john);
    game3.start();
    ```
3. You can see that we created three games - each between any of the three players. Now just test the code and check out the output for each game instance.


