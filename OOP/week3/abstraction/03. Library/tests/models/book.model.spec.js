// Importing these is not required but it's a good idea to have them
// You can also add 'jest': true in the 'env' of .eslintrc.js to suppress ESLint warnings for these global functions
import {describe, expect, test} from '@jest/globals';

import { Book } from '../../src/models/book.model.js';

// There are different conventions when it comes to ordering your tests and naming them
// Write the `describe` and `test` descriptions as they make the most sense to you

// This is one variant for writing tests
describe('Book', () => {
  describe('constructor', () => {
    test('should throw if falsy name value was passed', () => {
      expect(() => new Book(undefined, 'Awesome Author')).toThrow();
      expect(() => new Book(null, 'Awesome Author')).toThrow();
      expect(() => new Book('', 'Awesome Author')).toThrow();
    });
  
    test('should throw if falsy authorName value was passed', () => {
      expect(() => new Book('Awesome Book', undefined)).toThrow();
      expect(() => new Book('Awesome Book', null)).toThrow();
      expect(() => new Book('Awesome Book', '')).toThrow();
    });
  });
});