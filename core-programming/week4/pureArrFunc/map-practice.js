import people from "./people.js";


const strings = ['42', '23', '15', '2'];
const numbers = strings.map(s => Number(s)); // [42, 23, 15, 2] (notice the lack of quote ')




// let result = collection.map(e => myTransformationFunc(e));
const numbers1 = [1, 2, 3, 4];
// Double each number in an array of numbers.
// Example: [1,2,3,4] -> [2,4,6,8]

const result = numbers1.map(n => n + n);
console.log(result);

let input = ['cat', 'dog'];
// Uppercase each string in an array of strings.
// Example: ['cat', 'dog'] -> ['CAT', 'DOG']

const result1 = input.map(s => s.toUpperCase());
console.log(result1);

let input1 = ['cat', 'DOG'];
// Transform each string to the opposite case.
// Example: ['cat', 'DOG'] -> ['CAT', 'dog']

const result2 = input1.map(s => s === s.toUpperCase() ? s.toLowerCase() : s.toUpperCase());
console.log(result2);

let input2 = ['eLepHANT', 'CucuMbeR'];
// Normalize each string. Normalization means taking a string containing any case letters and making it capitalized.
// Example: ['eLepHANT', 'CucuMbeR'] -> ['Elephant', 'Cucumber']
const result3 = input2.map(s => {
    let str = s.split('');
    let firstLet = str[0].toUpperCase();
    str.shift();
    return firstLet += str.map(char => {
        if (char === char.toUpperCase()) {
            return char.toLowerCase();
        }
        if (char === char.toLowerCase()) {
            return char.toLowerCase();
        }
    }).join('');
});
console.log(result3);

// Create a function getBookInfo that returns a string with information about a book and apply it on a collection of books.
// Book: { title: string, author: string, pages: number }
// The bookInfo result string is up to you, but it must contain the title, the author and the pages.

const books = [
    { title: '1984', author: 'George Orwell', pages: 328 },
    { title: 'Fahrenheit 451', author: 'Ray Bradbury', pages: 256 },
    { title: 'Brave New World', author: 'Aldous Huxley', pages: 311 },
    { title: 'The Handmaid\'s Tale', author: 'Margaret Atwood', pages: 311 },
];
// example result
/*
[
 '1984, George Orwell, 328',
 'Fahrenheit 451, Ray Bradbury, 256',
 'Brave New World, Aldous Huxley, 311',
 "The Handmaid's Tale, Margaret Atwood, 311"
]
*/
function getValues(arr) {
    // let arrCopy = JSON.parse(JSON.stringify(arr));
    let result = [];
    arr.map(e => result.push(`${Object.values(e)}`))
    return result;
}
console.log(getValues(books));

// Map each user in a collection to an HTML template. The HTML template should look like the example below. The three placeholders should give you an idea what information the user object should hold.
const myTransformationFunc = (people) => {
    let arrCopy = JSON.parse(JSON.stringify(people));
    let result = [];
    arrCopy.map(el => {
        result.push(`<div>
    <h3>${el.firstName} ${el.lastName}</h3>
    <em>Member since ${el.registeredDate}.</em>
</div>`)
    });

    return result
}
console.log(myTransformationFunc(people));
