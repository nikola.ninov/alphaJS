import { Vehicle } from './Vehicle.js';
import { VehicleType } from './VehicleType.js';

export class Car extends Vehicle {
	static SEATS_MIN_VALUE = 1;
	static SEATS_MAX_VALUE = 10;
	static SEATS_ERROR_MESSAGE = `Seats must be between ${Car.SEATS_MIN_VALUE} and ${Car.SEATS_MAX_VALUE}!`;
	static WHEELS_COUNT = 4;
	#seats;
	/**
	* Creates an instance of Car.
	*
	* @param {string} make - The car's make.
	* @param {string} model - The car's model.
	* @param {number} price - The car's price.
	* @param {number} seats - The number of seats in the car.
	*/
	constructor(make, model, price, seats) {
		super(Car.getVehicleType(), make, model, price, Car.WHEELS_COUNT );
		this.seats = seats;
	}

	get seats() {
		return this.#seats;
	}
	set seats(value) {
		if (value >= Car.SEATS_MIN_VALUE && value <= Car.SEATS_MAX_VALUE) {
			this.#seats = value;
		} else {
			throw new Error(Car.SEATS_ERROR_MESSAGE);
		}
	}
	
	static getVehicleType() {
		return Object.keys(VehicleType).find((key) => VehicleType[key] === Car.WHEELS_COUNT);
	}

	get vehicleSpecifics() {
		return `Seats: ${this.seats}`;
	}

	print() {
		let details = 'Car:\n';
		details += super.print();
		details += `\n${this.vehicleSpecifics}`;

		if (super.comments.length === 0) {
			details += '\n--NO COMMENTS--';
		} else {
			details += `\nComments: ${super.comments.join(', ')}`;
		}
		return details;
	}

}
