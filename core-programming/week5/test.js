const gameData = {
    dataProp: 'games',
    groupByProp: 'released',
    games: [
        {
            name: 'World of Warcraft',
            released: 2004,
            version: 'Vanilla',
            developer: 'Blizzard',
        },
        {
            name: 'World of Warcraft',
            released: 2007,
            version: 'The Burning Crusade',
            developer: 'Blizzard',
        },
        {
            name: 'World of Warcraft',
            released: 2008,
            version: 'The Wrath of the Lich King',
            developer: 'Blizzard',
        },
        {
            name: 'World of Warcraft',
            released: 2010,
            version: 'Cataclysm',
            developer: 'Blizzard',
        },
        {
            name: 'World of Warcraft',
            released: 2012,
            version: 'Mists of Pandaria',
            developer: 'Blizzard',
        },
        {
            name: 'Warlords Battlecry',
            released: 2000,
            version: '1',
            developer: 'Strategic Studies Group',
        },
        {
            name: 'Warlords Battlecry',
            released: 2002,
            version: '2',
            developer: 'Strategic Studies Group',
        },
        {
            name: 'Warlords Battlecry',
            released: 2004,
            version: '3',
            developer: 'Infinite Interactive',
        },
        {
            name: 'The Sims',
            released: 2000,
            version: '1',
            developer: 'Maxis',
        },
        {
            name: 'The Sims',
            released: 2004,
            version: '2',
            developer: 'Maxis',
        },
        {
            name: 'The Sims',
            released: 2009,
            version: '3',
            developer: 'Maxis',
        },
        {
            name: 'The Sims',
            released: 2014,
            version: '4',
            developer: 'Maxis',
        },
        {
            name: 'Spore',
            released: 2008,
            version: '1',
            developer: 'Maxis',
        },
    ],
};

let input = [[2], [4], [6], [8], [8]];
let input1 = [[-1, 0, 2], [-3, -3, -3], [5, 5, -5]]
function task1(data) {
    const result = Array.from({ length: data[0].length }, () => new Array(data.length).fill(0));

    const rowSums = data.map(row => row.reduce((acc, curr) => acc + curr, 0));

    const sums = data.map(row => row.reduce((acc, e) => acc + e), 0);
    data.forEach((row, i) => {
        row.forEach((val, j) => {
            result[j][i] = val / sums[i]
        });
    });
    // for (let i = 0; i < data.length; i++) {
    //     let sum = 0;
    //     data[i].forEach(element => {
    //         sum += element
    //     });
    //     for (let j = 0; j < data[0].length; j++) {
    //         result[j][i] = data[i][j] / sum
    //     }
    // }
    return result
}
// console.log(task1(input));
//---------------------------------------------------------
let input2 = x => x + 1;
let input3 = x => x / 3;
function task2(data) {
    let operations = {};
    return (param) => {
        if (operations[param]) {
            operations[param].calls = operations[param].calls + 1
        }
        if (!operations[param]) {
            const value = data(param);
            operations[param] = { value: value, calls: 1 }
        }
        return operations[param];

    }

}
// const spiedFn = task2(input3);
// let test = [3, 6, 3, 9, 3, '12'];
// console.log(task2(input2)(test));

let input4 = {
    dataProp: 'games',
    groupByProp: 'released',
    games: [
        {
            name: 'World of Warcraft',
            released: 2004,
            version: 'Vanilla',
            developer: 'Blizzard',
        },
        {
            name: 'World of Warcraft',
            released: 2007,
            version: 'The Burning Crusade',
            developer: 'Blizzard',
        },
        {
            name: 'World of Warcraft',
            released: 2008,
            version: 'The Wrath of the Lich King',
            developer: 'Blizzard',
        },
        {
            name: 'World of Warcraft',
            released: 2010,
            version: 'Cataclysm',
            developer: 'Blizzard',
        },
        {
            name: 'World of Warcraft',
            released: 2012,
            version: 'Mists of Pandaria',
            developer: 'Blizzard',
        },
        {
            name: 'Warlords Battlecry',
            released: 2000,
            version: '1',
            developer: 'Strategic Studies Group',
        },
        {
            name: 'Warlords Battlecry',
            released: 2002,
            version: '2',
            developer: 'Strategic Studies Group',
        },
        {
            name: 'Warlords Battlecry',
            released: 2004,
            version: '3',
            developer: 'Infinite Interactive',
        },
        {
            name: 'The Sims',
            released: 2000,
            version: '1',
            developer: 'Maxis',
        },
        {
            name: 'The Sims',
            released: 2004,
            version: '2',
            developer: 'Maxis',
        },
        {
            name: 'The Sims',
            released: 2009,
            version: '3',
            developer: 'Maxis',
        },
        {
            name: 'The Sims',
            released: 2014,
            version: '4',
            developer: 'Maxis',
        },
        {
            name: 'Spore',
            released: 2008,
            version: '1',
            developer: 'Maxis',
        },
    ],
}
let input5 = {
    dataProp: 'students',
    groupByProp: 'age',
    students: [
        {
            name: 'Peter',
            age: 20,
        },
        {
            name: 'George',
            age: 24,
        },
        {
            name: 'Maria',
            age: 20,
        },
    ],
}
let input6 = {
    ...gameData
}
let input7 = {
    ...gameData,
    dataProp: 'games',
    groupByProp: 'developer',
}

function task3(data) {
    let { dataProp, groupByProp, ...values } = data
    values = values[dataProp];

    let result = values.reduce((acc, curr) => {
        const name = curr[groupByProp];
        if (!acc[name]) {
            acc[name] = []
        }
        acc[name].push(curr);
        return acc;
    }, {})
    return result
}

// console.log(task3(input5));

// Array.prototype.last = function () {
//     return this.length === 0 ? -1 : this.pop();
// };

// /**
//  * const arr = [1, 2, 3];
//  * arr.last(); // 3
//  */

// const arr = [10,5,4];
// console.log(arr.last()); // 3

/**
 * @param {Function[]} functions
 * @return {Function}
 */
var compose = function (functions) {
    let result = 0;
    let evaluation = 0;
    return function (x) {
        functions.forEach(e => {              
            result = e(x);
        });
        return result
    }
};

const fn = compose([x => x + 1, x => 2 * x])
console.log(fn(4));
/**
 * const fn = compose([x => x + 1, x => 2 * x])
 * fn(4) // 9
 */

// Input: functions = [x => x + 1, x => x * x, x => 2 * x], x = 4
// Output: 65
// Explanation:
// Evaluating from right to left ...
// Starting with x = 4.
// 2 * (4) = 8
// (8) * (8) = 64
// (64) + 1 = 65