import { Task } from '../task.model.js';
import { Employee } from '../../employee.model.js';
import { programmerLevel } from '../../../common/programmer-level.enum.js';
export class ProgrammerTask extends Task {
    #programmerLevel;
    #assignee;

    constructor(name, dueDate, programmerLevel, assignee) {
        super(name, dueDate);
        this.validateProgrammerLevel(programmerLevel);
        this.validateAssignee(assignee);
        this.#programmerLevel = programmerLevel;
        this.#assignee = assignee;
    }

    get assignee() {
        return this.#assignee;
    }

    get programmerLevel() {
        return this.#programmerLevel;
    }
    
    validateProgrammerLevel(value) {
        if (!Object.values(programmerLevel).includes(value)) {
            throw new Error('Invalid programmer level for ProgrammerTask.');
        }
    }
    validateAssignee(value) {
        if (!(value instanceof Employee) || !value) {
            throw new Error('Invalid assignee for ManagerTask.');
        }
    }
}