// class Person {
//     #name;

//     constructor(name) {
//         this.setName(name);
//     }

//     setName(value) {
//         if (!value) {
//             throw new Error('Person name is invalid!')
//         }
//         this.#name = value;
//     }
//     getName() {
//         return this.#name
//     }
//     toString() {
//         return this.#name
//     }
// }

// const person = new Person('Pesho');
// person.setName('Gosho');
// console.log(person.getName());
import { Person } from "./person.js";


const personOne = new Person('Viktor', 'Valtchev');
const personTwo = new Person('Pesho', 'Basmakov');
console.log(personOne.getFirstName());
console.log(personOne.getLastName());
personOne.firstName = 'Georgi';
console.log(personOne.getFirstName());
console.log(personOne.getLastName());
