import { useNavigate } from "react-router-dom";

const PostDetails = ({ id, title, body, isIndividual, deletePost }) => {
  const navigate = useNavigate();

  const handleButtonClick = () => navigate(-1); //equivalent to hitting the back button.
  const renderDetailsButton = () => {
    if (!isIndividual) {
      return (
        <div>
          <button
            style={{ borderRadius: "10px" }}
            onClick={() => deletePost(id)}
          >
            -
          </button>
          <button
            style={{ borderRadius: "10px" }}
            onClick={() => navigate(`/posts/${id}`)}
          >
            Post_{id}
          </button>
        </div>
      );
    }
  };

  return (
    <div style={{ marginBottom: 15 }}>
      {renderDetailsButton()}
      <div>Title: {title}</div>
      <div>Body: {body}</div>
      {isIndividual && <button onClick={handleButtonClick}>Back</button>}
    </div>
  );
};

export default PostDetails;
