export const programmerLevel = Object.freeze({
    JUNIOR: 'JUNIOR',
    REGULAR: 'REGULAR',
    SENIOR: 'SENIOR',
});
