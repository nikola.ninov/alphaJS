import { Account } from "../src/account.js";

describe('Account', () => {
    describe('class', () => {
        it('should be a class', () => {
            expect(typeof Account).toBe('function');
        });
    });

    describe('name', () => {
        it('should return the account name', () => {
            const account = new Account('John Doe');
            expect(account.name).toBe('John Doe');
        });

        it('should throw an error when name is not defined', () => {
            expect(() => new Account()).toThrow('name must be defined');
        });
        it('should throw an error when name length is less than 3', () => {
            expect(() => new Account('Jo')).toThrow('name.length must be >= 3');
        });
    });

    describe('amount', () => {
        it('should have an initial amount of 0', () => {
            const account = new Account('John Doe');
            expect(account.amount).toBe(0);
        });
    });
    describe('deposit', () => {
        it('should increase the amount by the deposited value', () => {
            const account = new Account('John Doe');
            account.deposit(100);
            expect(account.amount).toBe(100);
        });
        it('should apply 5% tax for large deposits', () => {
            const account = new Account('John Doe');
            account.deposit(20000);
            expect(account.amount).toBe(20000 * 0.95);
        });

        it('should throw an error when depositing a non-positive amount', () => {
            const account = new Account('John Doe');
            expect(() => account.deposit(-50)).toThrow('deposit expects a positive number');
            expect(() => account.deposit(0)).toThrow('deposit expects a positive number');
        });
    });
    describe('withdraw', () => {
        it('should decrease the amount by the withdrawn value', () => {
            const account = new Account('John Doe');
            account.deposit(100);
            account.withdraw(50);
            expect(account.amount).toBe(50);
        });

        it('should throw an error when withdrawing a non-positive amount', () => {
            const account = new Account('John Doe');
            expect(() => account.withdraw(-50)).toThrow('withdraw expects a positive number');
            expect(() => account.withdraw(0)).toThrow('withdraw expects a positive number');
        });
        it('should throw an error when withdrawing more than the available amount', () => {
            const account = new Account('John Doe');
            expect(() => account.withdraw(100)).toThrow('insufficient funds');
        });
    });
});
