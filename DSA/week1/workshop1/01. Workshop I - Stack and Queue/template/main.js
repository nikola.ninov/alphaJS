import Queue from './src/queue.js';
import Stack from './src/stack.js';

// Create a new Queue
const queue = new Queue();

// Test enqueue and count property
queue.enqueue(1);
queue.enqueue(2);
queue.enqueue(3);
console.log(queue.count); // Output: 3

// Test peek and dequeue
console.log(queue.peek()); // Output: 1
console.log(queue.dequeue()); // Output: 1

// Test isEmpty
console.log(queue.isEmpty); // Output: false

// Create a new Stack
const stack = new Stack();

// Test push and count property
stack.push(1);
stack.push(2);
stack.push(3);
console.log(stack.count); // Output: 3

// Test peek and pop
console.log(stack.peek()); // Output: 3
console.log(stack.pop()); // Output: 3

// Test isEmpty
console.log(stack.isEmpty); // Output: false