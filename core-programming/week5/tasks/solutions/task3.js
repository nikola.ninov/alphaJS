// Grouping by property
export default function (data) {
  // your code starts here
  let { dataProp, groupByProp, ...values } = data
  values = values[dataProp];
  
  let result = values.reduce((acc, curr) => {
    const name = curr[groupByProp];
    if (!acc[name]) {
      acc[name] = []
    }
    acc[name].push(curr);
    return acc;
  }, {})
  return result
  // your code ends here
}
