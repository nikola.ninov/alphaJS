import LinkedListNode from './linked-list-node.js';

export default class Queue {
    #head;
    #tail;
    #count = 0;

    get count() {
        return this.#count;
    }
    enqueue(value) {
        const newNode = new LinkedListNode(value, null);
        if (!this.#head) {
            this.#head = newNode;
        } else {
            this.#tail.next = newNode;
        }
        this.#tail = newNode;
        this.#count++;
    }
    dequeue() {
        if (!this.#head) {
            throw new Error('Queue is empty');
        }
        const val = this.#head.value;
        this.#head = this.#head.next;
        this.#count--;
        return val;
    }
    peek() {
        if (!this.#head) {
            throw new Error('Queue is empty');
        }
        return this.#head.value;
    }
    get isEmpty() {
        return !this.#head;
    }
}