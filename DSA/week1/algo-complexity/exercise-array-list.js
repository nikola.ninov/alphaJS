// array list 




class List {
    #items = [];

    // add
    // ако знаем началото на списъка знаем колко елемента има знаем, че са последователни то би трябвало бързо да знаем къде да поставим елемента след добавяне.   О(1)
    add(item) {
        this.#items.push(item);
    }

    // get index
    // елемента се взима по алгоритъм n+(index*size)  get(3)-> 4 елемента n+(3*4) n+12
    get(index) {
        this.#checkIndex(index);
        return this.#items[index];
    }

    //insert(index,item)
    // O(n) shift n елементи
    // премества елементите 1 по 1 от една клетка към друга
    // 2те операции добавяне и премахване могат да станат много бавни ако имаме много елементи защото първо добавя после премества елементите след добавената стойност. и обратното при ремове първо премахва и премества на ляво
    

    insert(index, element) {
        this.#checkIndex(index);
        this.#items.splice(index, 0, element);
    }

    remove(index) {
        this.#checkIndex(index);
        this.#items.splice(index, 1);
    }
    size() {
        return this.#items.length;
    }

    #checkIndex(index) {
        if (typeof index !== 'number' || index > this.#items.length || index < 0) {
            throw new Error('Index out of range');
        }
    }
    get items() {
        return this.#items;
    }

}


let elements = new List();
elements.add(1)
elements.add(2)
elements.add(3)
console.log(elements.get(2));
console.log(elements.items);