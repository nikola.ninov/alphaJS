const defaultInfoMsg = 'No info provided!';

export  const info = (msg) => {
  console.info(`Info: ${msg || defaultInfoMsg}`);
};
export const error = (msg) => {
  console.info(`Error: ${msg || defaultInfoMsg}`);
};
export const success = (msg) => {
  console.info(`Success: ${msg || defaultInfoMsg}`);
};
export default {
  info,
  error,
  success,
};
