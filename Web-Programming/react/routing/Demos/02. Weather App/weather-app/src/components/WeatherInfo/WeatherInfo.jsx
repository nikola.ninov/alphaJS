import React from 'react';
import PropTypes from 'prop-types';
import './WeatherInfo.css';

const WeatherInfo = ({ weather, city }) => {

    // Extract the day, month, and hour from the last_updated string
    const lastUpdated = new Date(weather.last_updated);
    const day = lastUpdated.getDate();
    const month = lastUpdated.toLocaleString('default', { month: 'long' });
    const hour = lastUpdated.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });

    // Render the temperature and update time info
    return (
        <div className="weather-info">

            <div className="weather-info-header">{city}</div>
            <img
                className="weather-info-icon"
                src={weather.condition.icon}
                alt={weather.condition.text}
            />
            <div>
                <div className="weather-info-temp">{weather.temp_c}°C</div>
                <br />
                <div className="weather-info-updated">
                    Last updated on {month} {day}, {hour} local time
                </div>
            </div>
        </div>
    );
};

// Add prop types to ensure that the component receives the correct props
WeatherInfo.propTypes = {
    weather: PropTypes.shape({
        last_updated: PropTypes.string.isRequired,
        temp_c: PropTypes.number.isRequired,
    }).isRequired,
    city: PropTypes.string.isRequired,
};

export default WeatherInfo;