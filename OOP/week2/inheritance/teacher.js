import { Person } from "./person.js";

export class Teacher extends Person {
    subject;

    sayName() {
        const baseResult = super.sayName();
        return baseResult + ` I teach ${this.subject}`
    }
}