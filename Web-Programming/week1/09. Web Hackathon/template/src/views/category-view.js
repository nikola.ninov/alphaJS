export const toCategoriesView = (categories) => `
<div id="categories">
  <h1>Categories</h1>
  <div class="content">
  <div class="category-card">
    ${categories.map(toSingleCategoryView).join('\n')}
    </div> 
  </div>
</div>
`;

const toSingleCategoryView = (category) => `
  <div id="${category.id}" class="categories">
    <h2>${category.name}</h2>
    <span>${category.moviesCount} movies</span>
    <button class="categories" data-page="${category.id}">View category</button>    
  </div>
`;
