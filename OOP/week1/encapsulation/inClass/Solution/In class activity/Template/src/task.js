import { status } from './status.js';

/** The Task class holds all relevant data and behavior a task might have. */
export class Task {
  // One way to indicate that a field should not be touched is to put an underscore before its name.
  // This won't prevent your colleges from directly interacting with it.

  _name;
  _dueDate;
  _status;
  static #NAME_MIN_LENGTH = 6;
  static #NAME_MAX_LENGTH = 20;

  constructor(name, _dueDate) {
    this.changeName(name);
    this.changeDueDate(_dueDate);
    this._status = status.TODO;
  }

  // It should never be null, undefined or an empty string, 
  // its length should be between 6 and 20 characters
  changeName(value) {
    if (!value) {
      throw Error('Name cannot be empty!');
    }

    if (value.length < Task.#NAME_MIN_LENGTH || value.length > Task.#NAME_MAX_LENGTH) {
      // eslint-disable-next-line quotes
      throw Error(`Name should be between ${Task.#NAME_MIN_LENGTH} and ${Task.#NAME_MAX_LENGTH} symbols`);
    }

    this._name = value;
  }

  changeDueDate(value) {
    if (!value) {
      throw Error('Date cannot be empty!');
    }
    if (value.valueOf() < Date.now().valueOf()) {
      throw Error('Due date cannot be earlier than today');
    }

    this._dueDate = value;
  }

  reset() {
    this._status = status.TODO;
  }

  advance() {
    this._status = status.IN_PROGRESS;
  }

  complete() {
    this._status = status.DONE;
  }


  // Name: Update naming
  // Status: Todo
  // Due: 9/4/2022 12:00:00 AM
  toString() {
    return `\nName: ${this._name}\nStatus: ${this._status}\nDue: ${this._dueDate.toLocaleString('en-US')}\n`;
  }

}