// Importing these is not required but it's a good idea to have them
// You can also add 'jest': true in the 'env' of .eslintrc.js to suppress ESLint warnings for these global functions.
import {describe, expect, test} from '@jest/globals';

import { Library } from '../../src/models/library.model.js';
import { Book } from '../../src/models/book.model.js';

// There are different conventions when it comes to ordering your tests and naming them
// Write the `describe` and `test` descriptions as they make the most sense to you

// This is another variant for writing tests

describe('Library', () => {
  describe('constructor', constructorTests);
  describe('books', bookTests);
});

function constructorTests() {
  // You have to check if things throw errors when passed invalid data.
  test('should throw if falsy name value was passed', () => {
    expect(() => new Library(undefined)).toThrow();
    expect(() => new Library(null)).toThrow();
    expect(() => new Library('')).toThrow();
  });

  // But you also have to check if things work when you pass correct data.
  test('should not throw if truthy name value was passed', () => {
    expect(() => new Library('Sofia')).not.toThrow();
    expect(() => new Library('Awesome library')).not.toThrow();
    expect(() => new Library(1)).not.toThrow(); // Wait, is this a valid name for library? Maybe we need to add more validations
  });
}

function bookTests() {
  test('.push() should not affect the array', () => {
    // Arrange
    const sut = new Library('Awesome library');
    const book = new Book('Sample name', 'Sample author name');

    // Act
    sut.books.push(book);

    // Assert
    expect(sut.books.length).toBe(0);

    // Is this test actually meaningful? We don't care if specifically .push() doesn't work
    // Instead, we want to test if .books returns a copy, that's what we care about
  });

  test('should return a shallow copy every time it is invoked', () => {
    // Arrange
    const sut = new Library('Awesome library');

    // Act
    const firstCallResult = sut.books;
    const secondCallResult = sut.books;

    // Assert
    expect(firstCallResult === secondCallResult).toBe(false); // These two objects have different references (two copies)
    expect(firstCallResult).toMatchObject(secondCallResult); // But they have the same structure (shallow copies)

    // Now this is what we have to test .books for.
    // Therefore, the other .push() test is redundant.
  })
}