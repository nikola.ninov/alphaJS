import ListNode from './list-node.js'

class LinkedList {
  #head = null;

  addFirst(value) {
    const node = new ListNode(value);
    node.next = this.#head;
    this.#head = node;
  }

  removeFirst() {
    if (this.#head === null) {
      throw new Error('List is empty');
    }

    const val = this.#head.value;
    this.#head = this.#head.next;

    return val
  }

  insertAfter(node, value) {
    const newNode = new ListNode(value);
    newNode.next = node.next;
    node.next = newNode;
  }

  removeAfter(node) {
    if (node.next) {
      node.next = node.next.next;
    }
  }

  find(value) {
    let ref = this.#head;
    while (ref) {
      if (value === ref.value) {
        return ref;
      }
      ref = ref.next;
    }

    return null;
  }

  // Returns true if there is a loop in linked
  // list else returns false.
  detectLoop() {
    const set = [];
    let ref = this.#head;
    while (ref) {
      // If we have already have this node
      // in set it means their is a cycle
      // (Because you we encountering the
      // node second time).
      if (set.includes(ref))
        return true;

      // If we are seeing the node for
      // the first time, insert it in set
      set.push(ref);
      ref = ref.next;
    }
    return false;
  }

  *[Symbol.iterator]() {
    let current = this.#head;
    while (current) {
      yield current.value;
      current = current.next;
    }
  }
}


const list = new LinkedList();

list.addFirst(3);
list.addFirst(2);
list.addFirst(1);

for (const val of list) {
  console.log(val);
}

console.log(list.detectLoop());

const node = new ListNode(10);
const node2 = list.find(2);
const node3 = list.find(3);
node.next = node2;
node3.next = node;

console.log(list.detectLoop());

for (const val of list) {
  console.log(val);
}
