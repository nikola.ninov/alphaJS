// let input = [
//     "6",
//     "1 2 3 4 5 6",
//     "2 3 4 5 6 7",
//     "6 5 4 3 2 1",
//     "3 4 5 6 7 8",
//     "4 5 6 7 8 9",
//     "9 8 7 6 5 4",
//     "3 5 3 -5 -4 -2",
// ];
let input = [
    "5",
    "1 22 3 41 5 2",
    "2 13 4 -5 6 5",
    "-6 5 9 31 2 8",
    "3 14 5 -6 7 4",
    "-5000 -5 6 -7 8 7",
    "-3 -3 3 3 4 -3 -4 3",
];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let n = +gets();
let matrix = [];
for (let i = 0; i < n; i++) {
    matrix.push(gets().split(" ").map(Number))
}
let commands = gets().split(" ").map(Number)
let sum = 0;
let maxSum = -10000000000;

for (let i = 0; i < commands.length - 1; i += 2) {
    let R = commands[i];
    let C = commands[i + 1];
    if (R > 0) {
        for (let i = 0; i < Math.abs(C); i++) {
            sum += matrix[R - 1][i]
        }
        if (C > 0) {
            for (let i = R - 1; i >= 0; i--) {
                if (i === (R - 1)) {
                    continue
                }
                sum += matrix[i][C - 1];
            }
        } else if (C < 0) {
            for (let i = Math.abs(R) - 1; i <= matrix.length - 1; i++) {
                if (i === (Math.abs(R) - 1)) {
                    continue
                }
                sum += matrix[i][Math.abs(C) - 1];
            }
        }
    } else {
        R = Math.abs(R);
        for (let i = matrix[1].length - 1; i >= Math.abs(C) - 1; i--) {
            sum += matrix[R - 1][i];
        }
        if (C > 0) {
            for (let i = R - 1; i >= 0; i--) {
                if (i === (R - 1)) {
                    continue
                }
                sum += matrix[i][C - 1];
            }
        } else if (C < 0) {
            for (let i = R - 1; i <= matrix.length - 1; i++) {
                if (i === (R - 1)) {
                    continue
                }
                sum += matrix[i][Math.abs(C) - 1];
            }
        }
    }
    if (sum > maxSum) {
        maxSum = sum;
    }
    sum = 0;
}

print(maxSum);