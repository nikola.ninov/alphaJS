import './Footer.css';

const Footer = () => {
    return (
        <p className='footer'>
            &copy; Weather App
        </p>
    )
}

export default Footer;