import people from './people.js';

const numbers = [4, 2, 1, 3, 5, 8];
const numbers1 = [1, 15, 2, 8, 31, 5, 9];
const string1 = ['cat', 'dog', 'elephant', 'cucumber', 'duck'];

//  Filter all the numbers which are less than 5 or larger than 15.
const result = numbers1.filter(n => n < 5 || n > 15);
console.log(result);

// Filter all the numbers which are larger than 5 and less than 15.
const result1 = numbers1.filter(n => n > 5 && n < 15);
console.log(result1);

// Filter all the numbers which are prime.
const result2 = numbers1.filter((num) => {
    if (num < 2) {
        return false;
    }
    for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) {
            return false;
        }
    }
    return true;
}
);
console.log(result2)

// Filter all the strings which longer than 5 symbols.
const result3 = string1.filter(w => w.length > 5);
console.log(result3);

//  Filter all the strings that include a certain substring.
const result4 = (arr, sub) => {
    return arr.filter(w => w.includes(sub));
};
console.log(result4(string1, "uc"));

// Create an array of users { username: string, password: string } and filter all the users that have strong passwords. A 'strong' password means one lowercase, one uppercase, one digit and at least 8 symbols long.
let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
let sortedStrongPass = people.filter((p) => regex.test(p.password));
console.log(sortedStrongPass);



