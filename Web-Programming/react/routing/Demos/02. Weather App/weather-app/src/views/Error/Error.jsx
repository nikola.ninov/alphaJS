import './Error.css'

export default function Error() {
    return (
        <div className='Error'>Something's wrong, there's an error!</div>
    )
}