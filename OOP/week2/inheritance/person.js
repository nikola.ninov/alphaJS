export class Person {
    name;

    constructor(name) {
        this.name = name;
    }

    sayName() {
        return `My name is ${this.name}`;
    }
}