let input = [
    '1122',
    'A1B12C11D2'
];


let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
const secretCode = gets().trim();
const cipher = gets().trim();
const codeMap = new Map();
const regex = /([A-Z])(\d+)/g;

while ((match = regex.exec(cipher)) !== null) {
    const letter = match[1];
    const code = match[2];
    codeMap.set(code, letter);
}

const findOriginalMessages = (secretCode, codeMap) => {
    const originalMessages = [];
    const backtrack = (codeIndex, message) => {
        if (codeIndex === secretCode.length) {
            originalMessages.push(message);
            return;
        }
        for (let i = 1; i <= secretCode.length - codeIndex + 1; i++) {
            const code = secretCode.slice(codeIndex, codeIndex + i);
            if (codeMap.has(code)) {
                const letter = codeMap.get(code);
                backtrack(codeIndex + i, message + letter);
            }
        }
    };
    backtrack(0, '');
    return originalMessages;
};

const originalMessages = findOriginalMessages(secretCode, codeMap).sort();
print(originalMessages.length);
originalMessages.forEach((message) => print(message));