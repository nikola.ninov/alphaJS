export class Employee {
    static nextId = 1;
    static NAME_MIN_LENGTH = 5;
    static NAME_MAX_LENGTH = 40;
    #name;
    #id;

    constructor(name) {
        this.validateName(name);
        this.#name = name;
        this.#id = Employee.nextId++;
    }
    validateName(name) {
        if (!name ||
            typeof name !== 'string' ||
            name.trim() === '' ||
            name.length < Employee.NAME_MIN_LENGTH ||
            name.length > Employee.NAME_MAX_LENGTH) {
            throw new Error('Invalid name for Employee.');
        }
    }

    get name() {
        return this.#name;
    }
    
    get id() {
        return this.#id;
    }
}