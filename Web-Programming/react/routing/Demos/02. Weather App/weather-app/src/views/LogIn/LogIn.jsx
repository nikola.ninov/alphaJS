import './LogIn.css'
import Footer from "../../components/Footer/Footer"
import { AuthContext } from '../../context/AuthContext'
import { useContext } from 'react'


export default function LogIn() {

    const { isLoggedIn, setLoggedIn } = useContext(AuthContext);

    return (
        <div className="log-in-wrapper">

            <div>
                {isLoggedIn ? (
                    <div>
                        <div className='message'>Thanks for logging in!</div>
                        <button onClick={() => setLoggedIn(false)}>Log Out</button>
                    </div>
                ) : (
                    <div>
                        <div className='message'>You have to log in to access this website!</div>
                        <button onClick={() => setLoggedIn(true)}>Log In</button>
                    </div>
                )}
            </div>
            <Footer></Footer>
        </div>

    )
}