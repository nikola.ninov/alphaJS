


// let [n, m] = input.split(' ').map(Number);

// // initialize variables
// let row = 0;
// let col = 0;
// let direction = "down-right";
// let sum = 2 ** row;

// // loop until we hit a corner
// while (true) {
//   if (direction === "down-right") {
//     row++;
//     col++;
//     if (row === n) {
//       direction = "up-right";
//       row--;
//       col++;
//     } else if (col === m) {
//       direction = "down-left";
//       row++;
//       col--;
//     }
//   } else if (direction === "up-right") {
//     row--;
//     col++;
//     if (row === 0) {
//       direction = "down-right";
//       col--;
//       sum += 2 ** row;
//       break;
//     } else if (col === m) {
//       direction = "up-left";
//       row--;
//       col--;
//     }
//   } else if (direction === "up-left") {
//     row--;
//     col--;
//     if (col === 0) {
//       direction = "up-right";
//       row++;
//       sum += 2 ** row;
//       break;
//     }
//   } else if (direction === "down-left") {
//     row++;
//     col--;
//     if (row === n) {
//       direction = "up-left";
//       row--;
//       col--;
//     } else if (col === 0) {
//       direction = "down-right";
//       col++;
//       sum += 2 ** row;
//       break;
//     }
//   }

//   // add value of current cell to sum
//   sum += 2 ** row;
// }

// console.log(sum);
// let input = "3 4";

// let print = this.print || console.log;
// let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

// const [n, m] = input.split(' ').map(Number);

// // initialize variables
// let row = 0;
// let col = 0;
// let direction = "down-right";
// let sum = 2 ** row;

// // loop until we hit a corner
// while (true) {
//   if (direction === "down-right") {
//     row++;
//     col++;
//     if (row === n || col === m) {
//       direction = "up-left";
//       row--;
//       col--;
//     }
//   } else if (direction === "up-left") {
//     row--;
//     col--;
//     if (row < 0 || col < 0) {
//       break;
//     }
//   }

//   // add value of current cell to sum
//   if (row >= 0 && col >= 0 && row < n && col < m) {
//     sum += 2 ** row * 2 ** col;
//   }
// }

// console.log(sum);

let input = "3 4";

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
const [n, m] = input.split(' ').map(Number);


// initialize variables
let row = 0;
let col = 0;
let direction = "down-right";
let sum = 2**row;

// loop until we hit a corner
while (true) {
  if (direction === "down-right") {
    row++;
    col++;
    if (row === n || col === m) {
      direction = "up-left";
      row--;
      col--;
    }
  } else if (direction === "up-left") {
    row--;
    col--;
    if (row < 0 || col < 0) {
      break;
    }
  }

  // add value of current cell to sum
  if (row >= 0 && col >= 0 && row < n && col < m) {
    sum += 2**row * 2**col;
  }
}

console.log(sum);