let input = [
    '5'
];
let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

const n = +gets();

const arr = new Array(n).fill(null).map(() => new Array(n).fill(0));

const numConcentricSquares = Math.ceil(n / 2);
let sideLen = n;
let currNum = 0;

for (let i = 0; i < numConcentricSquares; i++) {
    
    for (let j = 0; j < sideLen; j++) {
        arr[i][i + j] = ++currNum;
    }    
    for (let j = 1; j < sideLen - 1; j++) {
        arr[i + j][n - 1 - i] = ++currNum;
    }
    for (let j = sideLen - 1; j > 0; j--) {
        arr[n - 1 - i][i + j] = ++currNum;
    }
    for (let j = sideLen - 1; j > 0; j--) {
        arr[i + j][i]= ++currNum;
    }
    sideLen -= 2;
}

for (let i = 0; i < arr.length; i++) {
    let row = "";
    for (let j = 0; j < arr[0].length; j++) {
        row += `${arr[i][j]} `;
    }
    console.log(row);
}
