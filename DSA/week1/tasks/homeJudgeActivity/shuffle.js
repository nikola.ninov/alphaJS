let input = [
  '7 4',
  '1 5 4 7'
];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

const [n, k] = gets().split(' ').map(Number);
const numbers = gets().split(' ').map(Number);

const node = (value) => {
  return {
    value: value,
    next: null,
    previous: null,
  }
};

const DoubleLinkedList = () => {
  let head = null;
  let tail = null;
  const map = new Map();


  function addNode(node) {
    if (head === null) {
      head = node;
      tail = node;
    } else {
      tail.next = node;
      node.previous = tail;
      tail = node;
    }
    map.set(node.value, node);
  }
  function find(value) {
    return map.get(value) || null;
  }
  function moveAfterElement(element, moveAfter) {
    let elementToMove = find(element);
    let elementAfter = find(moveAfter);
    if (!elementToMove || !elementAfter || elementToMove === elementAfter) {
      return;
    }
    if (elementToMove.previous) {
      elementToMove.previous.next = elementToMove.next;
    } else {
      head = elementToMove.next;
      if (head) {
        head.previous = null;
      }
    }

    if (elementToMove.next) {
      elementToMove.next.previous = elementToMove.previous;
    } else {
      tail = elementToMove.previous;
      if (tail) {
        tail.next = null;
      }
    }
    if (elementAfter.next) {
      elementToMove.next = elementAfter.next;
      elementToMove.next.previous = elementToMove;
    } else {
      tail = elementToMove;
      elementToMove.next = null;
    }

    elementToMove.previous = elementAfter;
    elementAfter.next = elementToMove;
  }
  function print() {
    const values = [];
    let headFrom = head;
    while (headFrom !== null) {
      values.push(headFrom.value)
      headFrom = headFrom.next;
    }

    return values.join(' ')
  }
  return {
    addNode,
    find,
    moveAfterElement,
    print
  }
}
const list = DoubleLinkedList();

for (let i = 1; i <= n; i++) {
  const newNode = node(i);
  list.addNode(newNode);
}

for (let i = 0; i < k; i++) {
  const numberToShuffle = numbers[i];
  const isEven = numberToShuffle % 2 === 0;
  const moveAfter = isEven ? numberToShuffle / 2 : Math.min(numberToShuffle * 2, n);
  if (numberToShuffle === moveAfter) {
    continue;
  }
  list.moveAfterElement(numberToShuffle, moveAfter);
}
print(list.print());