const API_KEY = 'iq6IDVYTJQy2JNfmxUxmnEXMgdCadCiS';

export const getSearchedGifs = async (searchTerm = '') => {
    let url = `https://api.giphy.com/v1/gifs/search?api_key=${API_KEY}&limit=10&q=`;
    let str = searchTerm.trim();
    url = url.concat(str);
    const result = await fetch(url).then(response => response.json())
        .then(content => {
            return content.data
        });

    return result
}

