## Citing JavaScript code
```javascript
console.log("It doesn't get easier than this.");
```

1. Loops & Arrays:
* Learning Objectives
    * Explain how loops work and what is the role of the 'break' and 'continue'.
    * Solve basic judge tasks that require the usage of loops.
    * Analyze the proper type of loop you should use for a given task.
    * Demonstrate the usage of the most common array methods.
    * Exercise your skills by solving some array coding challenges.
* Theoretical Questions
    * What is a loop? What types of loops do you know?
    * What is an array? What are the benefits of using arrays over using several variables?

2. Functions & Exceptions: 
* Learning Objectives
    * Explain what functions are and why they are so important.
    * Write a function that accepts parameters and produces a result.
    * Name the three types of functions and explain their differences.
    * Demonstrate writing functions that are single-responsible.
    * Explain what exceptions are and how we handle them?
    * Handle an exception that is thrown inside a function.
* Theoretical Questions
    * What is a function? What types of functions are there?
    * What is the difference between function expression, fat arrows and function declaration?
    * What is an immediately-invoked function expression? Why use it?

3. NPM / ES Module
* Learning Objectives
    * Explain what NPM is and why it is important.
    * Install packages from NPM locally or globally.
    * Determine the version of an installed package.
    * Create a simple NPM script and execute it.
    * Explain what ES Modules are and what problems they solve.
    * Create modules that contain functions and import them in other modules.
    * Import an installed package into a file and use it.
* Theoretical Questions
    * What is NPM? What are the benefits of using it?
    * What is the package.json file?
    * What are NPM scripts?
    * What are ES Modules?

4. ESNext / ESLint
* Learning Objectives
    * Explain what is the ECMAScript standard.
    * Apply the new ES6 features (let, const, arrow functions, etc.).
    * Understand the pros of the static analysis tools.
* Theoretical Questions
    * What are the benefits of including 'use strict' at the beginning of a JavaScript source file?
    * What are template strings? What are their benefits?
    * What are destructuring assignments?

5. Objects
* Learning Objectives
    * Explain the difference between primitive and reference types.
    * Demonstrate the difference between primitive and reference types.
    * Explain what objects are and how do they compare to primitive types.
    * Demonstrate how we can use objects.
    * Explain how objects help build more complex applications.
* Theoretical Questions
    * What are objects? Provide an example.
    * What do we use objects for?
    * What are the differences between primitive and reference types? Give code examples.
    * What is destructuring?
 
6. Git
* Learning Objectives
    * Explain what git is and, in simple terms, how it works.
    * Demonstrate the usage of git with a local and with a remote repository.
    * Create and manage GitLab repositories and their collaborators.
* Theoretical Questions
    * What is git? What are the benefits of using it?
    * What is GitLab? What are the benefits of using it?
    * How is GitLab different from git?
    * Which are the global configurations you need to set in order to use git? What is their role?
    * What is a commit? What information does it contain?

7. Scope & Closure
* Learning Objectives
    * Explain what is scope and what defines it.
    * Demonstrate the difference between var and let.
    * Explain what hoisting is, what is hoisted and what not.
    * Demonstrate how each of the three function type is hoisted.
    * Explain what is closure and how it's connected to scopes.
    * Demonstrate how to provide access to a variable's value outside the function it is defined in.
    * In a given piece of code, determine if closure exists and identify it.
* Theoretical Questions
    * What is scope? Provide an example.
    * What is the difference between const, let and var?
    * What is the difference between == and ===, and what is type coercion?
    * What is variable and function hoisting?
    * What is hoisted and what is not?
    * What is lexical scope? What is lexing-time?
    * What is closure? Provide an example.

8. Module Pattern & Functional Programming
* Learning Objectives
    * Explain what the module pattern is and what is its purpose.
    * Determine the value of a variable, depending on where it is declared, initialized and then accessed.
    * Explain what is functional programming and the difference between imperative and declarative programming.
    * In a given piece of code, point out all of the higher order functions and explain their implementation.
* Theoretical Questions
    * What is functional programming?
    *  What is a pure function and what are side effects?
    * What is a module and why would you use it?
    * What is the revealing module pattern?

9. Pure Array Functions
* Learning Objectives
    * Understand why built-in array methods are preferable to loops.
    * Demonstrate the usage of 'filter', 'map' and 'reduce'.
    * Learn how to aggregate and transfrom collections of data.
* Theoretical Questions
    * What are the benefits of using array methods (forEach, map, etc.) over loops?
    * Implement filter, map and reduce.
    * Refactor an imperative loop using map, filter and reduce.

10. Objects (Advanced)
* Learning Objectives
    * Differentiate between reference, shallow and deep copy.
    * Understand how we can build structures to represent complex data.
    * Learn about object methods and the value of this.
    * Understand what the Linked List structure is.
    * Learn how to build and traverse a simple Linked List.
* Theoretical Questions
    * What is the main difference between reference copy and shallow copy.
    * What is the main difference between shallow and deep copy.
    * How can you represent complex data? Give an example.
    * What are methods? Write an example.
    * What are linked lists? Can you traverse a linked list?


