import { wait } from '../utils/utils.js';
import { Command } from './command.js';

export class RemoveTaskCommand extends Command {

  async execute(name) {

    
    // this line below should be copied in every single command inheritor
    await wait(500);
    try {
      if(!(this.taskRegistry.tasks.some(el => el.name === name))) {
       throw new Error('There is no such task in the Registry!');
      } else {
      this.taskRegistry.removeTask(name);
      console.log('This task was deleted');
      }
    } catch (error) {
      console.log(error.message);
    }
    // missing method implementation
  }
}
