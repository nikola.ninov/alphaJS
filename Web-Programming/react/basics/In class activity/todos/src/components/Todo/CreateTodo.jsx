import { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import './CreateTodo.css';

const CreateTodo = ({ createTodo, onCancel }) => {
    const [todoData, setTodoData] = useState({
        name: '',
        due: moment().format('YYYY-M-D'),
        isDone: false,
    });

    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setTodoData((prevData) => ({ ...prevData, [name]: value }));
    };

    const handleCreateTodo = () => {
        const { name, due } = todoData;
        if (!name.trim()) {
            alert('Please enter a valid name for the todo.');
            return;
        }
        if (!due.trim()) {
            alert('Please enter a valid due date for the todo.');
            return;
        }

        createTodo(todoData);
        setTodoData({
            name: '',
            due: moment().format('YYYY-M-D'),
            isDone: false,
        });
    };

    const handleCancel = () => {
        alert('No todo added.');
        onCancel();
    };

    return (
        <div className="create-todo">
            <label>
                Todo Name:
                <input
                    type="text"
                    name="name"
                    value={todoData.name}
                    onChange={handleInputChange}
                />
            </label>
            <label>
                Due Date:
                <input
                    type="text"
                    name="due"
                    value={todoData.due}
                    onChange={handleInputChange}
                />
            </label>
            <button onClick={handleCreateTodo}>Create</button>
            <button onClick={handleCancel}>Cancel</button>
        </div>
    );
};

CreateTodo.propTypes = {
    createTodo: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
};

export default CreateTodo;