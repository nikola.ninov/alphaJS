import { useContext } from "react";
import { AuthContext } from "../context/AuthContext";
import { Navigate, useLocation } from "react-router-dom";


export default function AuthenticatedRoute({ children }) {

    const { isLoggedIn } = useContext(AuthContext);
    let location = useLocation();


    if (isLoggedIn === false) {
        return <Navigate to="/log-in" state={{ from: location.pathname}}></Navigate>
    }

    return children;
}