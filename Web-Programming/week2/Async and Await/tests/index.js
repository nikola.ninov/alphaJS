const wait = (seconds) => {
    return new Promise((resolve, reject) => {
        setTimeout(resolve('Result'), seconds * 1000);
    });
}
console.log('test 1');
const execute = async () => {
    const result = await wait(5);
    console.log(result);
}


execute();
console.log('Something after the execute');



// const promise = Promise.resolve(42);
// promise.then(result => console.log(result))



    (async () => {
        const promise = Promise.resolve(42);
        const result = await promise;
        console.log(result);
    })();