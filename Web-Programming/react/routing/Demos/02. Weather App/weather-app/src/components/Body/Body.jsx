import React from 'react';
import WeatherInfo from '../WeatherInfo/WeatherInfo';
import { API_KEY } from '../../common/constants';
import { useState, useEffect } from 'react';
import './Body.css';

export default function Body() {
    const [weather, setWeather] = useState(null);
    const [city, setCity] = useState('Sofia');
    const [input, setInput] = useState('');

    useEffect(() => {
        fetch(`http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=${city}`)
            .then((response) => response.json())
            .then((data) => {
                setWeather(data.current);
            });
    }, [city]);

    const handleFormSubmit = (e) => {
        e.preventDefault();
        setCity(input);
        setInput('');
    };

    return (
        <div className="body-container">
            <h1 className="body-title">Weather App</h1>
            <form onSubmit={handleFormSubmit}>
                <input
                    className="body-input"
                    type="text"
                    value={input}
                    name="city"
                    onChange={(e) => setInput(e.target.value)}
                    placeholder="Enter city name"
                />
                <button className="body-button" type="submit">Search</button>
            </form>
            {weather && <WeatherInfo {...{ weather, city }} />}
            {!weather && <div className="body-message">No data about this city</div>}
        </div>
    );
}