import moment from '../node_modules/moment/moment.js';
import { status } from './status.js';

/** The Task class holds all relevant data and behavior a task might have. */
export class Task {
  // One way to indicate that a field should not be touched is to put an underscore before its name.
  // This won't prevent your colleges from directly interacting with it.

  /**
 * Name of the task
 * @type {string}
 * @private
 */
  _name;

  /**
   * Due date of the task
   * @type {Date}
   * @private
   */
  _dueDate;

  /**
   * Status of the task
   * @type {string}
   * @private
   */
  _status;

  /**
   * Constructs a new instance of the Task class
   * @param {string} name The name of the task.
   * @param {Date} date The due date of the task
   */
  constructor(name, date) {
    this.changeName(name);
    this.changeDueDate(date);
    this._status = status.TODO;
  }

  /**
    * Changes the name of the task if it meets the required criteria.
    * Required criteria is not to be :null, undefined or an empty string
    * @param {string} value - The new name for the task.
  */
  changeName(value) {
    if (value && typeof value === 'string' && value.length > 6 && value.length < 20) {
      this._name = value;
    } else {
      console.log('Invalid name.');
    }
  }

  /**
   * Changes the due date of the task if it is a valid date in the future.
   * @param {Date} value - The new due date for the task.
   */
  changeDueDate(value) {
    const dateNow = new Date();
    const inputDate = new Date(value);

    if (inputDate instanceof Date && !isNaN(inputDate) && inputDate >= dateNow) {
      this._dueDate = inputDate;
    } else {
      console.log('Invalid due Date'); //pref throw new error
    }
  }

  /**
   * Changes the status of the task if it is a valid status.
   * @param {string} value - The new status for the task.
   */
  changeStatus(value) {
    if (Object.keys(status).includes(value)) {
      this.status = value;
    } else {
      console.log('Invalid status '); //pref throw new error
    }
  }

  /**
   * Resets the status of the task to TODO.
   */
  reset() {
    if (Object.keys(status).includes('Todo')) {
      this._status = status.TODO;
    }
  }

  /**
   * Advances the status of the task to IN_PROGRESS.
   */
  advance() {
    if (Object.keys(status).includes('IN_PROGRESS')) {
      this._status = status.IN_PROGRESS;
    }
  }

  /**
   * Completes the task by setting the status to DONE.
   */
  complete() {
    if (Object.keys(status).includes('DONE')) {
      this._status = status.DONE;
    }
  }
  
  /**
   * Returns a string representation of the task.
   * @returns {string} The string representation of the task.
   */
  toString() {
    const formatedDate = moment(this._dueDate).format('M/D/YYYY h:mm:ss');
    return `
    Name: ${this._name}
    Status: ${this._status}
    Due: ${formatedDate}
    `;
  }
}