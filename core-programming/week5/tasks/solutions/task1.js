// Reduced matrix transposition
export default function (data) {
  // your code starts here 
  const result = Array.from({ length: data[0].length }, () => new Array(data.length).fill(0));
  const sums = data.map(row => row.reduce((acc, el) => acc + el, 0));
  data.forEach((row, rowIndex) => {
    row.forEach((col, colIndex) => {
      result[colIndex][rowIndex] = col / sums[rowIndex]
    });
  });
  return result

  // your code ends here
}
