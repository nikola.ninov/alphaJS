<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

## Additional Resources - Students

- [React Router](https://reactrouter.com/docs/en/v6/getting-started/overview)
- [React Router Tutorial](https://reactrouter.com/docs/en/v6/getting-started/tutorial)
- [React Router API Reference](https://reactrouter.com/docs/en/v6/api)
- [React Router Implementation with Lazy Loading](https://hygraph.com/blog/routing-in-react#how-to-implement-lazy-loading-with-react-router)