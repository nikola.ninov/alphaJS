import { Task } from './task.model.js';

/** The Board class holds tasks */
export class Board {
  #tasks;
  static taskCount;

  /**
  * Creates an instance of the Board class.
  */
  constructor() {
    this.#tasks = [];
    Board.taskCount = 0;
  }

  /**
     * Gets an array of tasks on the board.
     * @returns {Task[]} An array of tasks.
     */
  get tasks() {
    return this.#tasks.map((task) => task);
  }

  /**
   * Gets the number of tasks on the board.
   * @returns {number} The number of tasks.
   */
  get taskCount() {
    return Board.taskCount;
  }

  /**
   * Adds a new task to the board.
   * @param {Task} task - The task to be added.
   * @throws {Error} If the argument is not an instance of the Task class or if the task is already in the board.
   */
  add(task) {
    if (!(task instanceof Task)) {
      throw new Error('Only instances of Task class can be added to the board.');
    }
    if (this.#tasks.includes(task)) {
      throw new Error('This task is already in tasks.');
    }

    this.#tasks.push(task);
    Board.taskCount++; // Increment taskCount by 1
  }

  /**
   * Removes a task from the board.
   * @param {Task} task - The task to be removed.
   * @throws {Error} If the task does not exist in the board.
   */
  remove(task) {
    const taskIndex = this.#tasks.indexOf(task);

    if (taskIndex < 0) {
      throw Error('This task does not exist. Cannot remove!');
    }

    this.#tasks.splice(taskIndex, 1);
    Board.taskCount--; // Decrement taskCount by 1
  }

  /**
  * Transforms the board data into a formatted string.
  * @returns {string} A formatted string representing the board data.
  */
  toString() {
    const titles = '---Task Board---\n\nTasks:\n\n';

    if (this.#tasks.length) {
      return titles + this.#tasks.join('\n--------\n');
    }

    return `${titles}No tasks at the moment.`;
  }
}