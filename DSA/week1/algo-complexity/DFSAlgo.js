class Node {
    constructor(value) {
        this.value = value;
        this.adjacentNodes = [];
        this.visited = false;
    }

    addAdjacentNode(node) {
        this.adjacentNodes.push(node);
    }
}

function dfs(startNode) {
    const stack = [];
    stack.push(startNode);

    while (stack.length > 0) {
        const currentNode = stack.pop();
        if (!currentNode.visited) {
            console.log(currentNode.value);
            currentNode.visited = true;
            const adjacentNodes = currentNode.adjacentNodes;
            for (let i = adjacentNodes.length - 1; i >= 0; i--) {
                stack.push(adjacentNodes[i]);
            }
        }
    }
}

// Create nodes
const nodeA = new Node('A');
const nodeB = new Node('B');
const nodeC = new Node('C');
const nodeD = new Node('D');
const nodeE = new Node('E');

// Connect nodes
nodeA.addAdjacentNode(nodeB);
nodeA.addAdjacentNode(nodeC);
nodeB.addAdjacentNode(nodeD);
nodeB.addAdjacentNode(nodeE);

// Perform DFS
dfs(nodeA);