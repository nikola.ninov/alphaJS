// Usable properties
export default function (data) {
  // your code starts here
  let dataCopy = JSON.parse(JSON.stringify(data));
    let result = dataCopy.map(el => {

        let usable = el.usable;
        let obj = el;

        let object = usable.reduce((acc, e) => {
            if (obj[e] !== undefined) {
                acc[e] = el[e];
            }
            return acc
        }, {})

        return object
    });
    return result
  // your code ends here
}
