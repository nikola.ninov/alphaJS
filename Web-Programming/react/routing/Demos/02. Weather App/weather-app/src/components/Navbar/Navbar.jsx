import './Navbar.css'
import { NavLink } from 'react-router-dom'
import Logo from './../../assets/images/logo.png'

export default function Navbar() {



    return (

        <nav className='navigation'>

            <div className='navigation-menu'>
                <NavLink to="/" className="wrapper-logo">
                    <img className='logo' src={Logo} alt="Weather App Logo" />
                </NavLink>

                <ul className='list'>
                    <li>
                        <NavLink to="/home" className="nav-link">Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/about" className="nav-link">About</NavLink>
                    </li>
                    <li>
                        <NavLink to="/log-in" className="nav-link">Log In</NavLink>
                    </li>
                    <li>
                        <NavLink to="/sign-up" className="nav-link">Sign Up</NavLink>
                    </li>
                </ul>

            </div>

        </nav>
    )
}