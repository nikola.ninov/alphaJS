export const byKey = (key, order) => {
   const orderNum = order === 'desc' ? -1 : 1;
  return (a, b) => {
    let result;
    if (a[key] > b[key]) {
      result = 1;
    } else if (a[key] === b[key]) {
      result = 0;
    } else {
      result = -1;
    }
    return result * orderNum
  };

  // Closure. You return a new function which has closure over key, order and orderNum
};

export const findBy = (key, value) => {
  let func = (elem) => {
    return elem[key] === value
  }
  
  return func;
};

export const isBetween = (key, low, high) => {
  let func = (elem) => {
    return elem[key] >= low && elem[key] <= high;
  }
  return func;
};

export const isLower = (key, value) => {
  let func = (elem) => {
    return elem[key] <= value;
  }
  return func
  // Closure. You return a new function which has closure over key and value
};

export const isHigher = (key, value) => {
  let func = (el) => {
    return el[key] >= value;
  }
  return func;
  // Closure. You return a new function which has closure over key and value
};

export const startsWith = (key, letter) => {
  let func = (el) => {
    return el[key][0] === letter;
  }
  return func;
  // Closure. You return a new function which has closure over key and letter
};
