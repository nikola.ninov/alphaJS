import { deepCompareObjects, formatObject } from './common/utils.js';

/**
 * Solves a valid math expression
 * @param {object} object The object to copy
 * @returns {object} The deep copy
 */
const solve = (expression) => {

  const evaluateExpression = (expression) => {
    return eval(expression);
  };
  if (expression === '') {
    return '';
  }
  const leftParenIndex = expression.indexOf('(');
  const rightParenIndex = expression.lastIndexOf(')');
  if (leftParenIndex === -1 && rightParenIndex === -1) {
    return evaluateExpression(expression);
  }
  const innerExpression = expression.substring(leftParenIndex + 1, rightParenIndex);
  const innerResult = solve(innerExpression);
  const modifiedExpression = expression.substring(0, leftParenIndex) + innerResult + expression.substring(rightParenIndex + 1);
  return solve(modifiedExpression);
};


// Tests:
const testCases = [
  {
    test: '45+(55)',
    expected: 100,
  },
  {
    test: '45+(24*(12+3))',
    expected: 405,
  },
  {
    test: '12*(35-(46*(5+15)))',
    expected: -10620,
  },
  {
    test: '15',
    expected: 15,
  },
];

testCases.forEach(({ test, expected }, index) => {
  // arrange & act
  const actual = solve(test);

  // assert
  const result = deepCompareObjects(expected, actual);

  const message = result
    ? 'Pass.'
    : `Fail. Expected: ${formatObject(expected)}. Actual: ${formatObject(
      actual
    )}`;

  console.log(`Test ${index + 1}: ${message}`);
});




// const tokens = tokenize(expression);
//   let index = 0; // current index in the tokens array

//   const parseNumber = () => {
//     const token = tokens[index++];
//     return parseInt(token);
//   };

//   const parseFactor = () => {
//     let result;

//     if (tokens[index] === '(') {
//       index++; // skip opening parenthesis
//       result = parseExpression();
//       index++; // skip closing parenthesis
//     } else {
//       result = parseNumber();
//     }
//     return result;
//   };

//   const parseTerm = () => {
//     let leftOperand = parseFactor();
//     while (tokens[index] === '*' || tokens[index] === '/') {
//       const operator = tokens[index++];
//       const rightOperand = parseFactor();
//       if (operator === '*') {
//         leftOperand *= rightOperand;
//       } else {
//         leftOperand /= rightOperand;
//       }
//     }
//     return leftOperand;
//   };

//   const parseExpression = () => {
//     let leftOperand = parseTerm();

//     while (tokens[index] === '+' || tokens[index] === '-') {
//       const operator = tokens[index++];
//       const rightOperand = parseTerm();

//       if (operator === '+') {
//         leftOperand += rightOperand;
//       } else {
//         leftOperand -= rightOperand;
//       }
//     }

//     return leftOperand;
//   };

//   return parseExpression();
// };

// const tokenize = (expression) => {
//   const tokens = [];
//   let currentNumber = '';

//   for (let i = 0; i < expression.length; i++) {
//     const char = expression[i];

//     if (/\d/.test(char)) {
//       currentNumber += char;
//     } else {
//       if (currentNumber !== '') {
//         tokens.push(currentNumber);
//         currentNumber = '';
//       }

//       if (char !== ' ') {
//         tokens.push(char);
//       }
//     }
//   }

//   if (currentNumber !== '') {
//     tokens.push(currentNumber);
//   }

//   return tokens;
// };