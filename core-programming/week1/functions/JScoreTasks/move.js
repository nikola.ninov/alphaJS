let input = [
    "0",
    "10,20,30,40,50",
    "2 forward 1",
    "2 backwards 1",
    "3 forward 2",
    "3 backwards 2",
    "exit",
]
let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let currentPosition = +gets();
let array = gets().split(',').map(Number);
let forwardSum = 0;
let backwardSum = 0;

while (true) {
    let commands = gets().split(" ");
    let steps = Number(commands[0]);
    let size = Number(commands[2]);
    let direction = commands[1];


    for (let i = 0; i < steps; i++) {
        if (size > array.length) {
            size = size % array.length
        }
        if (direction === 'forward') {
            currentPosition += size;
            if (currentPosition >= array.length) {
                currentPosition = currentPosition - array.length;
            }
            forwardSum += array[currentPosition];
        } else {
            currentPosition -= size;
            if (currentPosition < 0) {
                currentPosition = currentPosition + array.length;
            }

            backwardSum += array[currentPosition];
        }
    }


    if (commands[0] === 'exit') {
        break;
    }
}

print(`Forward: ${forwardSum}`);
print(`Backwards: ${backwardSum}`);