export const singleElement = (element) => {
    return `
    <figure>
      <div class="gif-link" data-id="${element.id}">
        <img src="${element.images.downsized.url}" alt="${element.title}">
      </div>
      <figcaption>${element.title}</figcaption>
    </figure>
  `;
}