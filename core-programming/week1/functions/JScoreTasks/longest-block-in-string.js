
let input = ['hoopa'];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let charInput = gets().split('');
let maxLength = 0;
let counter = 0;
let sequence = '';

for (let i = 0; i < charInput.length; i++) {
    counter++;

    if (counter > maxLength) {
        sequence = '';
        maxLength = counter;
        for (let j = 0; j < counter; j++) {
            sequence += charInput[i]
        }
    }
   
    if (charInput[i] !== charInput[i + 1]) {
        counter =0;
    }

}

print(sequence);