import { Command } from './command.js';
import { wait } from '../utils/utils.js';
import { Task } from '../models/task.js';

export class UpdateTaskCommand extends Command {


  async execute(name, status) {
    await wait(500);

    try {

      const task = new Task(name, status);
      if (!(this.taskRegistry.tasks.some(el => el.name === name))) {
        console.log('There is no such task in the Registry!');
      } else {
        this.taskRegistry.removeTask(name);
        this.taskRegistry.addTask(task);
      }
      console.log('The task status was updated!');
    } catch (error) {
      console.log(`This is an error with message ${error.message}`);
    }
  }
}

