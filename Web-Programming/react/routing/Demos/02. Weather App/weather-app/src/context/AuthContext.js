import { createContext } from "react";

export const AuthContext = createContext({
    isLoggedIn: false,
    setLoggedIn: () => {}


    /*
    user: localStorage.getItem('user'),
    role: localStorage.getItem('role'),
    userID: localStorage.getItem('userID'),
    setUser: () => {}
    */
})