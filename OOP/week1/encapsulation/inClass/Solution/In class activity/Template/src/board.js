/** The Board class holds tasks */
export class Board {
    _tasks;

    constructor(){
        this._tasks = [];
    }

    add(task){
        // First way to search by reference.
        // if(this._tasks.includes(task)){
        //     throw Error('This task is already in tasks');
        // }

        // // Second way to seach by name;
        const hasTask = this._tasks.some(element => element._name === task._name);
        if(hasTask){
            throw Error('This task is already in tasks');
        }

        this._tasks.push(task);
    }

    remove(task){
        // First way to search by reference.
        // if(!this._tasks.includes(task)){
        //    throw Error('This task does not exist. Cannot remove!'); 
        // }
        // this._tasks.splice(this._tasks.indexOf(task), 1);    


        // // Second way to seach by name;
        // const hasTask = this._tasks.some(element => element._name === task._name);
        // if(!hasTask){
        //     throw Error('This task does not exist. Cannot remove!');
        // }
        // this._tasks.splice(this._tasks.indexOf(task), 1);    


        // Third way with index
        const taskIndex = this._tasks.indexOf(task);

        if(taskIndex < 0){
            throw Error('This task does not exist. Cannot remove!');
        }
        this._tasks.splice(taskIndex, 1);    
    }

    // ---Task Board---

// Tasks:

// No tasks at the moment.

    toString(){
        const titles = '---Task Board---';

        if(this._tasks.length === 0){
            return titles + '\nTasks: \nNo tasks at the moment';
        }

        return titles + this._tasks.join('--------');
    }
}