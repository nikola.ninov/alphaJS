import fs from 'fs/promises';

(async () => {
  const result = await fs.readFile('./data.json', { encoding: 'utf-8' });
  const data = JSON.parse(result);

  data.students.forEach(name => console.log(name));
})().catch(console.error);
