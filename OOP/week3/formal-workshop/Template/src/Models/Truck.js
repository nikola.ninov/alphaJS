import { Vehicle } from './Vehicle.js';
import { VehicleType } from './VehicleType.js';

export class Truck extends Vehicle {
	static WEIGHT_CAPACITY_MIN_VALUE = 1;
	static WEIGHT_CAPACITY_MAX_VALUE = 100;
	static WEIGHT_CAPACITY_ERROR_MESSAGE = `Weight capacity must be between ${Truck.WEIGHT_CAPACITY_MIN_VALUE} and ${Truck.WEIGHT_CAPACITY_MAX_VALUE}!`;
	static WHEELS_COUNT = 8;
	#weightCapacity;

	constructor(make, model, price, weightCapacity) {
		super(Truck.getVehicleType(), make, model, price, Truck.WHEELS_COUNT);
		this.weightCapacity = weightCapacity;
	}
	get weightCapacity() {
		return this.#weightCapacity;
	}
	set weightCapacity(value) {
		if (value < Truck.WEIGHT_CAPACITY_MIN_VALUE || value > Truck.WEIGHT_CAPACITY_MAX_VALUE) {
			throw new Error(Truck.WEIGHT_CAPACITY_ERROR_MESSAGE);
		}
		this.#weightCapacity = value;
	}
	

	get vehicleSpecifics() {
		return `Weight Capacity: ${this.weightCapacity}t`;
	}

	static getVehicleType() {
		return Object.keys(VehicleType).find((key) => VehicleType[key] === Truck.WHEELS_COUNT);
	}


	print() {
		let details = 'Truck:\n';
		details += super.print();
		details += `\n${this.vehicleSpecifics}`;

		if (super.comments.length === 0) {
			details += '\n--NO COMMENTS--';
		} else {
			details += `\nComments: ${super.comments.join(', ')}`;
		}
		return details;
	}
}
