"use strict";

const someObject = {
    someFunction() {
        console.log(this);
    }
}


someObject()

// const func = someObject.someFunction;

// func();

// someObject.someFunction();

// default bind

// const defaultBindFunc = function () {
//     console.log(this);
// }

// defaultBindFunc();

//implicit binding

// const sayName = function () {
//     console.log(this.name);
// }

// sayName();


// const pesho = {
//     name: 'Pesho',
//     sayName: sayName
// }

// const parent = {
//     name: 'Tosho',
//     child: pesho
// }


// const gosho = {
//     name: 'Gosho',
//     sayName: sayName
// }
// sayName()
// pesho.sayName();
// gosho.sayName();
// parent.child.sayName();

//explicit binding

// const sayName = function (text) {
//     console.log(text + this.name);
// }

// sayName();


// const pesho = {
//     name: 'Pesho'
// }
// sayName.call(pesho,'Hello, my name is ');
// sayName.apply(pesho,['Hello, my name is ']);
// sayName.call({ name: 'Gosho' });


// const peshoSayName = sayName.bind({ name: 'Gosho' });

// peshoSayName();

