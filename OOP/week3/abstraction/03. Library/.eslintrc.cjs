module.exports = {
	'env': {
		'es2022': true,
    'node': true,
    'jest': true,
	},
	'parserOptions': {
		'sourceType': 'module',
	},
	'extends': 'eslint:recommended',
	'rules': {
		'quotes': ['error', 'single'],
		'semi': ['error', 'always'],
		'comma-dangle': ['warn', 'always-multiline'],
		'max-classes-per-file': ['error', 1],
	},
};
