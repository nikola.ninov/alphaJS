// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { getFirestore, collection, getDocs } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCMnMvmpRvPnxPirelergS9hG2J9EVXB8g",
    authDomain: "auth-token-exercise.firebaseapp.com",
    projectId: "auth-token-exercise",
    storageBucket: "auth-token-exercise.appspot.com",
    messagingSenderId: "807544702517",
    appId: "1:807544702517:web:cc3362bb3f7cb9cdb1537c"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth(app);
// const todosCol=collection(db,"todos");
// const snapshot=await getDocs(todosCol);

onAuthStateChanged(auth, (user) => {
    if (user !== null) {
        console.log("User is signed in");
    } else {
        console.log("User is signed out");
    }
});
export {
    db, auth
}