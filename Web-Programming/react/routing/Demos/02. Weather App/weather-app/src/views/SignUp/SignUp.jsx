import './SignUp.css'
import Footer from "../../components/Footer/Footer"


export default function SignUp() {

    return (
        <div className="sign-up-wrapper">
            <div className="sign-up">We sign up from here!</div>
            <Footer></Footer>
        </div>

    )
}