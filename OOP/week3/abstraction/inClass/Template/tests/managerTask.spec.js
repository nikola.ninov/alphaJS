/* eslint-disable no-undef */
import { Task } from '../src/models/board-items/task.model.js';
import { ManagerTask } from '../src/models/board-items/tasks/manager-task.model.js';
import { projectType } from '../src/common/project-type.enum.js';
import { Employee } from '../src/models/employee.model.js';

describe('ManagerTask', () => {
    const validName = 'Task 1';
    const validDueDate = new Date('2024/09/03');
    const validProjectType = projectType.EXTERNAL_PRODUCT;
    const validAssignee = new Employee('John Doe');

    describe('Class', () => {
        it('should extend Task', () => {
            expect(ManagerTask.prototype).toBeInstanceOf(Task);
        });
    });
    describe('Constructor', () => {
        it('constructor should throw if passed projectType is invalid', () => {
            const invalidProjectType = 'InvalidType';
            expect(() => new ManagerTask(validName, validDueDate, invalidProjectType, validAssignee));
        });
        it('constructor should throw if passed assignee is not a Employee object', () => {
            const invalidAsignee = 'invalidAsignee';
            expect(() => new ManagerTask(validName, validDueDate, validProjectType, invalidAsignee));
        });
        it('constructor should assign correctly the values of projectType and assignee', () => {
            const managerTask = new ManagerTask(validName, new Date('2024/09/03'), validProjectType, validAssignee);
            expect(managerTask.projectType).toBe(validProjectType);
            expect(managerTask.assignee).toBe(validAssignee);
        });
    });

    describe('projectType', () => {
        it('should not be able to change the value of projectType', () => {
            const managerTask = new ManagerTask(validName, new Date('2024/09/03'), validProjectType, validAssignee);
            expect(() => { managerTask.projectType = projectType.INTERNAL_PRODUCT;}).toThrow();
        });
    });

    describe('assignee', () => {
        it('should not be able to change the value of assignee', () => {
            const managerTask = new ManagerTask(validName, new Date('2024/09/03'), validProjectType, validAssignee);
            const newEmployee = new Employee('Star Doe');
            expect(() => { managerTask.assignee = newEmployee;}).toThrow();
        });
    });
});