let input = [
    "10",
    "2",
    "1",
    "1",
    "2",
    "3",
    "3",
    "2",
    "2",
    "2",
    "1",
]

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let n = +gets();
let maxNumber = Number.MIN_VALUE;
let counter = 0;
let arr = [];

for (let i = 0; i < n; i++) { 
    arr.push(+gets());
}

for (let i = 0; i < arr.length; i++) {
    counter++;

    if (counter > maxNumber) {
        maxNumber = counter;        
    }
   
    if (arr[i] !== arr[i + 1]) {
        counter =0;
    }

}

console.log(maxNumber);