let input = [
    '3 3 3 3 3'
];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let arr = gets().split(' ').map(Number);

let reminder0 = [];
let reminder1 = [];
let reminder2 = [];

arr.forEach(el => {
    if (el % 3 == 0) {
        reminder0.push(el);
    }
    if (el % 3 == 1) {
        reminder1.push(el);
    }
    if (el % 3 == 2) {
        reminder2.push(el);
    }
});
console.log(reminder0.join(' '));
console.log(reminder1.join(' '));
console.log(reminder2.join(' '));