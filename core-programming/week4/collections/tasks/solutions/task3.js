// Powerful pets
/**
 * 
 * @param {Array<{ name: string, pets: Array<{ name: string, power: number }> }>} data 
 */
export default function (data) {
  // your code starts here
  let dataCopy = JSON.parse(JSON.stringify(data));

  let pets = {};
  let result = [];

  dataCopy.map(player => {
    player.pets.map(pet => {
      if (!pets[pet.name]) {
        pets[pet.name] = []
      }
      pets[pet.name].push({ player, power: pet.power })
    });
  });

  result = Object.keys(pets).sort().map(name => {

    const petsOfPlayer = pets[name];
    let totalPower = 0;

    let mostPowerfulPet = petsOfPlayer.reduce((max, pet) => {
      if (max.power < pet.power) {
        return pet;
      } else {
        return max
      }
    });

    petsOfPlayer.map(p => {
      totalPower += p.power
    })

    return {
      name,
      totalPower,
      mostPowerfulPlayer: mostPowerfulPet.player
    }
  })

  return result

  // your code ends here
}
