const bankAccaunt = (function () {
    let balance = 0;

    const deposit = (number) => {
        return balance += number;
    };

    const withdraw = (number) => {
        if (balance < number) {
            throw new Error("Insufficient balance")
        }
        return balance -= number;
    };

    getBalance = () => {
        return balance;
    }

    return {
        deposit,
        withdraw,
        getBalance
    }
})()

bankAccaunt.deposit(20);
console.log(bankAccaunt.getBalance());
bankAccaunt.deposit(20);
console.log(bankAccaunt.getBalance());
bankAccaunt.withdraw(10);
console.log(bankAccaunt.getBalance());