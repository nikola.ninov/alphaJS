const Rate = ({ rating, setRating }) => {
    const stars = [1, 2, 3, 4, 5];
    return (
        <div>
        {stars.map((star, i) => (
            <span
            key={i}
            onClick={() => setRating(star)}
            style={{ color: i < rating ? "gold" : "gray", cursor: "pointer" }}
            >
            ★
            </span>
        ))}
        </div>
    );
    }

    export default Rate;