import { Stack } from './common/stack.js';
/**
 * 
 * @param {string} sequence Sequence of keystrokes
 * @returns {string} String to display 
 */
const backspaceChar = (sequence) => {
    const stack = new Stack();

    for (let i = 0; i < sequence.length; i++) {
        const char = sequence[i];
        if (char === '#') {
            if (!stack.isEmpty()) {
                stack.pop();
            }
        } else {
            stack.push(char);
        }
    }

    let result = '';
    while (!stack.isEmpty()) {
        result = stack.pop() + result;
    }
    return result;
}

// Tests:
const testCases = [
    { test: `abc#d`, expected: `abd` },
    { test: `abcd##e##`, expected: `a` },
    { test: `abc####de`, expected: `de` },
    { test: `teler#ric#k`, expected: `telerik` },
    { test: `jav##ava###script#####`, expected: `js` }
];

testCases.forEach(({ test, expected }, index) => {
    // act
    const result = backspaceChar(test);

    // assert
    const message = (result === expected)
        ? 'Pass.'
        : `Fail. Expected: ${expected}. Actual: ${result}`

    console.log(`Test ${index + 1}: ${message}`);
});
