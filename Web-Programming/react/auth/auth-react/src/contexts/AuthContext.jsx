import React, { createContext, useContext, useState, useEffect } from "react";
import { auth, createUserWithEmailAndPassword, sendPasswordResetEmail, signInWithEmailAndPassword, signOut, updateEmail as updateE, updatePassword as updatePwd, doc, setDoc, db } from "../firebase";
import { getDoc } from "firebase/firestore";

const AuthContext = createContext();

export const useAuth = () => {
    return useContext(AuthContext);
}

export const AuthProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState();
    const [loading, setLoading] = useState(true);

    function signup(email, password) {
        return createUserWithEmailAndPassword(auth, email, password)
    }

    function login(email, password) {
        return signInWithEmailAndPassword(auth, email, password)
    }
    function resetPassword(email) {
        return sendPasswordResetEmail(auth, email);
    }

    function updateEmail(email) {
        return updateE(currentUser, email);
    }

    function updatePassword(password) {
        return updatePwd(currentUser, password);
    }

    function logout() {
        return signOut(auth);
    }


    useEffect(() => {
        const checkAndCreateUserDocument = async (user) => {
            if (user) {
                // User is signed in, set the currentUser state
                setCurrentUser(user);

                // Check if the Firestore document for the user already exists
                const userRef = doc(db, 'users', user.uid);
                const docSnap = await getDoc(userRef);

                if (!docSnap.exists()) {
                    // If the Firestore document doesn't exist, create it with the user's email
                    await setDoc(userRef, {
                        email: user.email || '',
                    });
                }
            } else {
                // User is signed out, clear the currentUser state
                setCurrentUser(null);
            }
            setLoading(false);
        };
        const unsubscribe = auth.onAuthStateChanged(checkAndCreateUserDocument);

        return () => unsubscribe();
    }, [])


    const value = {
        currentUser,
        signup,
        login,
        logout,
        resetPassword,
        updatePassword,
        updateEmail
    }

    return (
        <AuthContext.Provider value={value}>
            {!loading && children}
        </AuthContext.Provider>
    )
}
