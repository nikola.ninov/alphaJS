let input = [
    '7',
    'P4 P2 P3 S1 C2 P1 C1'
];

let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let n = +gets();
let soldiers = gets().split(' ');

function armyLunchOrder(soldiers) {
    const rankOrder = new Map([
        ['S', 3],
        ['C', 2],
        ['P', 1]
    ]);
    const sortedSoldiers = new Map();
    for (let soldier of soldiers) {
        const rank = soldier[0];
        if (!sortedSoldiers.has(rank)) {
            sortedSoldiers.set(rank, []);
        }
        sortedSoldiers.get(rank).push(soldier);
    }
    for (let rank of sortedSoldiers.keys()) {
        sortedSoldiers.get(rank).sort((a, b) => {
            const orderA = rankOrder.get(rank);
            const orderB = rankOrder.get(rank);
            return orderA - orderB;
        });
    }
    const orderedSoldiers = [];
    for (let rank of rankOrder.keys()) {
        if (sortedSoldiers.has(rank)) {
            orderedSoldiers.push(...sortedSoldiers.get(rank));
        }
    }
    return orderedSoldiers.join(' ');
}
const result = armyLunchOrder(soldiers);
console.log(result);