// Neighboring cells

/**
 * 
 * @param {Array<number[]>} data
 */
export default function (data /* a matrix */) {
  // your code starts here
  const rows = data.length;
  const cols = data[0].length
  const result = Array.from({ length: rows }, () => new Array(cols).fill(0));

  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < cols; j++) {
      const top = (i === 0) ? 0 : data[i - 1][j];
      const left = (j === 0) ? 0 : data[i][j - 1];
      const bottom = (i === rows - 1) ? 0 : data[i + 1][j]
      const right = (j === cols - 1) ? 0 : data[i][j + 1];

      result[i][j] = ((top + bottom) - (left + right))
    }
  }
  return result
  // your code ends here
}
