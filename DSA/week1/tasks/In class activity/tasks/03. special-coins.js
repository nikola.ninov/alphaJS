/**
 * Count how many coins are special 
 * @param {string} coins Coins to check
 * @param {string} catalogue The catalogue of special coins
 * @returns {number} The count of special coins  
 */
const specialCoins = (coins, catalogue) => {

    // const map = new Map();
    const cata = new Set([...catalogue]);
    let totalCount = 0;
    //---------------------------------------------------------------------
    // this is O(n+m)

    // [...coins].forEach(e => {
    //     if (cata.has(e)) {
    //         if (map.has(e)) {
    //             map.set(e, map.get(e) + 1)
    //         } else {
    //             map.set(e, 1)
    //         }
    //     }
    // });
    // map.forEach(c => {
    //     totalCount += c;
    // })    
    //---------------------------------------------------------------------

    // this is O(n+m)  or something maybe not true O(n+m*1)
    [...coins].forEach(e => { //n coins we do foreach on them  and we add m-times has operations which is constant m*O(1)
        if (cata.has(e)) { //m unique elements
            totalCount++;
        }
    })
    return totalCount
}

// Tests:
const testCases = [
    { coins: 'abcD', catalogue: 'abd', expected: 2 },
    { coins: 'abcDD', catalogue: 'cDfg', expected: 3 },
    { coins: 'aaaCCcccd', catalogue: 'acCe', expected: 8 },
    { coins: 'aaBBbbbc', catalogue: 'Bc', expected: 3 }
];

testCases.forEach(({ coins, catalogue, expected }, index) => {
    // arrange & act
    const count = specialCoins(coins, catalogue);

    // assert
    const message = expected === count
        ? 'Pass.'
        : `Fail. Expected: ${expected}. Actual: ${count}`

    console.log(`Test ${index + 1}: ${message}`);
});
