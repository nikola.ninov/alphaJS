const resetStyles = () => {
  const nodeList = document.querySelectorAll('.todo-item');
  const todos = Array.from(nodeList);

  todos.forEach(element => element.style.backgroundColor = 'transparent');
};

const applyStyles = () => {
  Array.from(
    document.querySelectorAll('.todo-item')
  )
    .forEach((element, index) => {
      element.style.backgroundColor = index % 2 === 0
        ? '#fff1b8'
        : '#f2f2f2'
    });
};

(() => {
  document
    .querySelector('#colorize')
    .addEventListener('click', applyStyles);

  document
    .querySelector('#reset')
    .addEventListener('click', resetStyles);
})();
