import { BoardItem } from './board-items/board-item.model.js';
import { Task } from './board-items/task.model.js';
/** The Board class holds items */
export class Board {
  #items;

  /**
   * Creates an instance of the Board class.
   */
  constructor() {
    this.#items = [];
  }

  /**
   * Gets the items in the board.
   * @returns {BoardItem[]} An array of board items.
   */
  get items() {
    return this.#items.slice();
  }

  /**
   * Gets the number of items in the board.
   * @returns {number} The number of items in the board.
   */
  get count() {
    return this.#items.length;
  }

  /**
   * Adds an item to the board.
   * @param {BoardItem} item - The item to be added.
   * @throws {Error} If the provided item is not an instance of the BoardItem class.
   * @throws {Error} If the provided item already exists in the board.
   */
  add(item) {
    if (!(item instanceof BoardItem)) {
      throw new Error('The provided value is not an objected created from the BoardItem class!');
    }

    const itemIndex = this.#items.findIndex(existingItem => existingItem === item);

    if (itemIndex >= 0) {
      throw new Error('The provided item already exists in this board!');
    }

    this.#items.push(item);
  }
  
  /**
   * Removes an item from the board.
   * @param {BoardItem} item - The item to be removed.
   * @throws {Error} If the provided item does not exist in the board.
   */
  remove(item) {
    const itemIndex = this.#items.findIndex(existingItem => existingItem === item);

    if (itemIndex < 0) {
      throw new Error('The provided task does not exist in this board!');
    }

    this.#items.splice(itemIndex, 1);
  }

  /**
   * Transforms the board data into a formatted string.
   * @returns {string} The formatted string representation of the board.
   */
  toString() {
    const titles = '---Items Board---\n\nItems:\n\n';

    if (this.items.length) {
      return titles + this.items.join('\n--------\n');
    }

    return `${titles}No items at the moment.`;
  }
}

