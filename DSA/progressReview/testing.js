// class Stack {
//     #items
//     #top
//     #capacity

//     constructor() {
//         this.#items = [];
//     }

//     push(element) {
//         this.#items[this.#items.length] = element;  //O(amortised constant)
//     }

//     pop() {
//         if (this.isEmpty()) {
//             throw new Error('Stack is empty')
//         }
//         this.#items[this.#items.length - 1];
//         return this.#items.length = this.#items.length - 1;  //O(1)

//     }
//     peek() {
//         return this.#items[this.#items.length - 1]
//     }

//     isEmpty() {
//         return this.#items.length === 0;
//     }
// }

// let stack = new Stack();
// stack.push(1);
// stack.push(2);
// stack.push(3);
// stack.push(4);
// stack.push(5);
// console.log(stack.peek());
// console.log(stack.pop());
// console.log(stack.peek());

// implementation of stack with array
// class Node {
//     constructor(value, next) {
//         this.value = value;
//         this.next = next || null;
//     }
// }

// class Stack {
//     #top = null;

//     push(value) {
//         const node = new Node(value);
//         node.next = this.#top;
//         this.#top = node;
//     }

//     pop() {
//         if (!this.isEmpty) {
//             throw new Error('Stack is empty');
//         }
//         const val = this.#top.value;
//         this.#top = this.#top.next;
//         return val;
//     }

//     peek() {
//         if (!this.isEmpty) {
//             throw new Error('Stack is empty');
//         }

//         return this.#top.value;
//     }

//     isEmpty() {
//         return !this.#top;
//     }
// }

// let stack = new Stack();
// stack.push(1);
// stack.push(2);
// stack.push(3);
// stack.push(4);
// stack.push(5);
// console.log(stack.peek());
// console.log(stack.pop());
// console.log(stack.peek());
// let a;
// (function () {
//     a = 42
// })()
// console.log(a);


class Node {
    constructor(data) {
        this.value = data;
        this.next = null;
    }
}

class LinkedList {
    constructor() {
        this.head = null;
        this.size = 0;
    }
    prepend(value) {
        const newNode = new Node(value);
        if (this.isEmpty()) {
            this.head = newNode;
        } else {
            newNode.next = this.head;
            this.head = newNode;
        }
        this.size++;
    }
    append(value) {
        const newNode = new Node(value);
        if (this.isEmpty()) {
            this.head = newNode;
        } else {
            let prev = this.head;
            while (prev.next) {
                prev = prev.next;
            }
            prev.next = newNode;
        }
        this.size++;
    }
    isEmpty() {
        return this.size === 0
    }
    getSize() {
        return this.size
    }
    print() {
        if (this.isEmpty()) {
            console.log('List is empty');
        } else {
            let curr = this.head;
            let listValues = '';
            while (curr) {
                listValues += `${curr.value} `
                curr = curr.next;
            }
            console.log(listValues);
        }
    }
}

const list = new LinkedList()

console.log('List is empty', list.isEmpty());
console.log('List size', list.getSize());
list.print();
list.prepend(10);
list.print();
list.append(50);
list.print();
list.prepend(20);
list.prepend(30);
console.log('List is empty', list.isEmpty());
console.log('List size', list.getSize());
list.print();