let input = [
    '3'
];
let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);
const n = +gets();
let matrix = []
for (let i = 0; i < n; i++) {
    matrix[i] = [];
}
let count = 1;
for (let row = 0; row < n; row++) {
    for (let col = 0; col < n; col++) {
        matrix[col][row]=count;
        count++;        
    }    
}
for (const row of matrix) {
    print(row.join(" "));
}
