// const min = (array) => {
//     // TODO
//     let result = 0;
//     let minNumber = Number.MAX_SAFE_INTEGER;
//     for (let i = 0; i < array.length - 1; i++) {
//         if (array[i] < minNumber) {
//             minNumber = array[i];
//         }
//     }
//     result = minNumber;
//     return result;
// };


// const array = [2, 3, 4, 5, 6, 7];

// const minNumb = min(array); // 2
// console.log(minNumb);

//---------------------------------------------

// const repeat = (string, n) => {
//     // TODO
//     let result = "";
//     for (let i = 0; i < n; i++) {
//         result += `${string}`

//     }
//     return result
// };
// const string = repeat('  home', 2); //   home  home

// console.log(string);

//---------------------------------------------------------------
// const average = (array) => {
//     // TODO
//     let sum = 0;
//     for (let i = 0; i < array.length; i++) {
//         sum += +array[i]
//     }
//     sum = sum / array.length;
//     return +sum;
// };

// const array = [2, 3, 4, 5, 6, 7];
// const averageN = average(array).toFixed(2); // 3.89

// console.log(averageN);
//-------------------------------------------------------------------
// const entries = (obj) => {
//     const pushArr = (arr, ...elements) => {
//       for (let i = 0; i < elements.length; i++) {
//         arr[arr.length] = elements[i]; // for every next element we add +1 in length 
//       }
//       return arr.length;
//     };

//     const entries = [];
//     for (const key in obj) {
//       if (Object.hasOwn(obj, key)) {
//         pushArr(entries, [key, obj[key]]);
//       }
//     }
//     return entries;
//   };



// let propTypes = { ilum: 'string', pump: 'number', cat: 'boolean', dog: 'string' };
// const entriesObj = entries(propTypes); // [[0, 'string'], [1, 'number], [2, 'boolean'], [3, 'string']]

// console.log(entriesObj);
// const entries = (obj) => {
//     const entries = [];
//     for (const key in obj) {
//         entries[entries.length] = [key, obj[key]];

//     }
//     return entries;
// };

// let propTypes = { a: 5, b: 6, c: 7 };
// const entriesObj = entries(propTypes); // [[0, 'string'], [1, 'number], [2, 'boolean'], [3, 'string']]

// console.log(entriesObj);
//---------------------------------------------------------------------------------
// const zip = (...arrays) => {
//     let shortestArrLength = Number.MAX_SAFE_INTEGER; // constant for max save value or infinity

//     for (let i = 0; i < arrays.length; i++) {         // loop whose work is to find smallest length
//         if (arrays[i].length < shortestArrLength) {
//             shortestArrLength = arrays[i].length
//         }
//     }
//     const result = [];   // array whose work is to store end operation array
//     for (let i = 0; i < shortestArrLength; i++) {  //first loop is to iterate until smallest length
//         const propertyGroup = []; //array to store group of elements on first iterator arr[i]

//         for (let j = 0; j < arrays.length; j++) {  // loop to iterate trough arrays[i]  and adding each element like group then push it to result's array
//             propertyGroup[propertyGroup.length] = arrays[j][i];
//         }
//         result[result.length] = propertyGroup
//     }
//     return result
// };

// const arr4 = [
//     ['a', 'b'],
//     [1, 2],
//     [true, false]
// ]
// const arr1 = [1, 2, 3];
// const arr2 = ['a', 'b', 'c', 'd'];
// const arr3 = [true, false, true];
// const array = [1, 2, 3, 4, 5, 6];
// const joined = `6-0-0-2`;
// const zipped = zip(array, joined.split(''))
// console.log(zipped);

// const flat = (obj) => {
//     const flatObj = {};
//     for (const key in obj) {
//         if (typeof obj[key] === 'object') {
//             const flatObject = flat(obj[key]);
//             for (const deepKey in flatObject) {
//                 flatObj[key + '.' + deepKey] = flatObject[deepKey];
//             }
//         } else {
//             flatObj[key] = obj[key];
//         }
//     }
//     return flatObj;
// };

// const object = {
//     name: 'Pesho',
//     age: 20,
//     isAlive: true,
//     address: { street: 'Al Malinov', number: 34 },
// };
// const flatten = flat(object);
// console.log(flatten);


const min = (array) => {
    let answer = 0;
    for (let i = 1; i < array.length; i++) {
        if (array[i - 1] > array[i]) {
            answer = array[i];
        }
    }
    return answer;
    // let result = 0;
    // let minNumber = Number.MAX_SAFE_INTEGER;
    // for (let i = 0; i < array.length - 1; i++) {
    //     if (array[i] < minNumber) {
    //         minNumber = array[i];
    //     }
    // }
    // result = minNumber;
    // return result;
};


const array = [2, 3, 4, 5, 6, 7];

const minNumb = min(array); // 2
console.log(minNumb);