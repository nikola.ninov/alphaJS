const input1 = [
    '7 4',
    'Emo Misho Ivanka Ginka Vancho Stancho Sashka',
    'Emo Misho',
    'Misho Emo',
    'Misho Sashka',
    'Sashka Stancho',
]
const input = [
    '5 3',
    'Gosho Tosho Penka Miro Stanka',
    'Miro Gosho',
    'Gosho Stanka',
    'Stanka Miro',
];


let print = this.print || console.log;
let gets = this.gets || ((arr, index) => () => arr[index++])(input, 0);

let [n, k] = gets().split(' ').map(Number);
let elements = gets().split(' ');
let mapOfelements = new Map();

for (let i = 0; i < elements.length; i++) {
    mapOfelements.set(elements[i], i + 1);
}


const node = (value) => {
    return {
        value: value,
        next: null,
        previous: null,
    }
};

const DoubleLinkedList = () => {
    let head = null;
    let tail = null;

    const map = new Map();

    function addNode(node) {
        if (head === null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            node.previous = tail;
            tail = node;
        }
        map.set(node.value, node);
    }
    function find(value) {
        return map.get(value) || null;
    }
    function moveBeforeElement(element, moveBefore) {
        const elementToMove = find(element);
        const elementBefore = find(moveBefore);

        if (!elementToMove || !elementBefore || elementToMove === elementBefore) {
            return;
        }

        if (elementToMove.previous) {
            elementToMove.previous.next = elementToMove.next;
        } else {
            head = elementToMove.next;
            head.previous = null;
        }

        if (elementToMove.next) {
            elementToMove.next.previous = elementToMove.previous;
        } else {
            tail = elementToMove.previous;
            tail.next = null;
        }
        elementToMove.next = elementBefore;
        elementToMove.previous = elementBefore.previous;
        if (elementBefore.previous) {
            elementBefore.previous.next = elementToMove;
        } else {
            head = elementToMove;
        }
        elementBefore.previous = elementToMove;
    }

    function print() {
        const values = [];
        let headFrom = head;
        while (headFrom !== null) {
            values.push(headFrom.value)
            headFrom = headFrom.next;
        }

        return values.join(' ')
    }

    return {
        addNode,
        find,
        moveBeforeElement,
        print
    }
}
const studentNames = elements;
const seatingOrder = DoubleLinkedList();

studentNames.forEach((name) => {
  const newNode = node(name);
  seatingOrder.addNode(newNode);
});

for (let i = 0; i < k; i++) {
  const [studentA, studentB] = gets().split(' ');
  seatingOrder.moveBeforeElement(studentA, studentB);
}
print(seatingOrder.print());