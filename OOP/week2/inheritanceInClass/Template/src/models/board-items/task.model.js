import { taskStatus } from '../../common/task-status.enum.js';
import { BoardItem } from './board-item.model.js';

export class Task extends BoardItem {

  #dueDate;
  #status;

  /**
   * Creates an instance of the Task class.
   * @param {string} name - The name of the task.
   * @param {Date} dueDate - The due date of the task.
   */
  constructor(name, dueDate) {
     // Reuse base class constructor first
    super(name);
    // Set through the setters to validate fields
    this.dueDate = dueDate;
    
    this.#status = taskStatus.TODO;
  }
 
  /**
   * Gets the due date of the task.
   * @returns {Date} The due date of the task.
   */
  get dueDate() {
    return this.#dueDate;
  }

  /**
   * Sets the due date of the task.
   * @param {Date} value - The new due date of the task.
   * @throws {Error} If the due date is not provided or is in the past.
   */
  set dueDate(value) {
    if (!value) {
      throw new Error('Due date not provided!');
    }

    if (value.valueOf() < Date.now().valueOf()) {
      throw new Error('Can\'t set due date to a date in the past!');
    }

    this.#dueDate = value;
  }

  /**
   * Gets the status of the task.
   * @returns {taskStatus} The status of the task.
   */
  get status() {
    return this.#status;
  }

  /**
   * Resets the status of the task to TODO.
   */
  reset() {
    this.#status = taskStatus.TODO;
  }

  /**
   * Advances the status of the task to IN_PROGRESS.
   */
  advance() {
    this.#status = taskStatus.IN_PROGRESS;
  }

  /**
   * Completes the task by setting its status to DONE.
   */
  complete() {
    this.#status = taskStatus.DONE;
  }

  /**
   * Returns a string representation of the task.
   * @returns {string} The string representation of the task.
   */
  toString() {
    return `* Task *\n${super.toString()}\n` + 
    `Status: ${this.status}\n` +
    `Due: ${this.dueDate.toLocaleDateString()} ${this.dueDate.toLocaleTimeString()}`;
  }
}
