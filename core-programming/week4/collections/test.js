let input = [
    {
        name: 'John',
        age: 20,
        street: 'Main',
        usable: ['name', 'age'],
    },
    {
        first: 10,
        second: 12,
        third: 16,
        usable: ['first', 'second'],
    },
]
let input1 = [
    {
        a: 1,
        b: 2,
        c: 3,
        d: 4,
        usable: ['a', 'c'],
    },
]
let input2 = [
    {
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        usable: [],
    },
]
// console.log(task1(input));
function task1(data) {
    let dataCopy = JSON.parse(JSON.stringify(data));
    let result = dataCopy.map(el => {

        let usable = el.usable;
        let obj = el;

        let object = usable.reduce((acc, e) => {
            if (obj[e] !== undefined) {
                acc[e] = el[e];
            }
            return acc
        }, {})

        return object
    });
    return result
}

//--------------------TASK 2 ------------------------
let input3 = {
    cities: [
        {
            id: 1,
            name: 'Plovdiv',
        },
        {
            id: 2,
            name: 'Sofia',
        },
        {
            id: 3,
            name: 'Varna',
        },
    ],
    people: [
        {
            name: 'Peter',
            city: 2,
        },
        {
            name: 'Marry',
            city: 1,
        },
        {
            name: 'John',
            city: 3,
        },
        {
            name: 'George',
            city: 3,
        },
        {
            name: 'Anika',
            city: 1,
        },
    ],
}
let input4 = {
    cities: [
        {
            id: 444,
            name: 'Ankh-Morpork',
        },
    ],
    people: [
        {
            name: 'Sir Samuel Vimes',
            city: 444,
        },
        {
            name: 'Lord Downey',
            city: 444,
        },
        {
            name: 'Mustrum Ridcullys',
            city: 444,
        },
        {
            name: 'Hughnon Ridcully',
            city: 444,
        },
    ],
}
function task2(data) {
    let { people, cities } = data;

    const population = people.reduce((acc, p) => {
        let city = {};
        cities.forEach(el => {
            if (p.city === el.id) {
                city = el.name
            }
        });
        if (!acc[city]) {
            acc[city] = 0;
        }
        if (Object.keys(acc).includes(city)) {
            acc[city] = acc[city] + 1
        }
        return acc
    }, {});
    const combinedPeopleInfo = people.map(p => {
        const { name, city: cityId } = p;
        let city = cities.find(c => c.id === cityId);
        let pop = population[city.name]

        return {
            name: name,
            city: {
                name: city.name,
                population: pop
            }
        }

    })
    return combinedPeopleInfo

}
// console.log(task2(input4));
//----------------------------------TASK 3 --------------------------

let input5 = [
    // Player object
    {
        name: 'Dave',
        pets: [
            // Pet object
            {
                name: 'Owl',
                power: 100,
            },
            // Pet object
            {
                name: 'Raven',
                power: 200,
            },
        ],
    },
    // Player object
    {
        name: 'Steve',
        pets: [
            // Pet object
            {
                name: 'Owl',
                power: 50,
            },
            // Pet object
            {
                name: 'Raven',
                power: 300,
            },
        ],
    },
]

let input6 = [
    {
        name: 'John',
        pets: [
            {
                name: 'Capybara',
                power: 240,
            },
            {
                name: 'Tasmanian Devil',
                power: 130,
            },
        ],
    },
    {
        name: 'Jane',
        pets: [
            {
                name: 'Quokka',
                power: 800,
            },
        ],
    },
    {
        name: 'Bob',
        pets: [
            {
                name: 'Tasmanian Devil',
                power: 450,
            },
            {
                name: 'Capybara',
                power: 450,
            },
            {
                name: 'Quokka',
                power: 660,
            },
        ],
    },
]

let input7 = [
    {
        name: 'Mark',
        pets: [
            {
                name: 'Hupyrian beetle',
                power: 1,
            },
        ],
    },
    {
        name: 'Quark',
        pets: [
            {
                name: 'Hupyrian beetle',
                power: 3,
            },
        ],
    },
    {
        name: 'Lynda',
        pets: [
            {
                name: 'Hupyrian beetle',
                power: 2,
            },
        ],
    },
]

let input8 = [
    {
        name: 'Quick Ben',
        pets: [],
    },
    {
        name: 'Tattersail',
        pets: [],
    },
    {
        name: 'Shadowthrone',
        pets: [
            {
                name: 'Shan',
                power: 1000,
            },
            {
                name: 'Rood',
                power: 1000,
            },
            {
                name: 'Pallick',
                power: 1000,
            },
            {
                name: 'Baran',
                power: 1000,
            },
        ],
    },
]

let input9 = [
    {
        name: 'Quick Ben',
        pets: [],
    },
    {
        name: 'Tattersail',
        pets: [],
    },
    {
        name: 'Kalam',
        pets: [],
    },
]

function task3(data) {
    let dataCopy = JSON.parse(JSON.stringify(data));
    let pets = {};
    let result = [];
    dataCopy.map(player => {
        player.pets.map(pet => {
            if (!pets[pet.name]) {
                pets[pet.name] = []
            }
            pets[pet.name].push({ player, power: pet.power })
        });
    });

    result = Object.keys(pets).sort().map(name => {
        const petsOfPlayer = pets[name];
        let totalPower = 0;

        let mostPowerfulPet = petsOfPlayer.reduce((max, pet) => {
            if (max.power < pet.power) {
                return pet;
            } else {
                return max
            }
        });


        petsOfPlayer.map(p => {
            totalPower += p.power
        })

        return {
            name,
            totalPower,
            mostPowerfulPlayer: mostPowerfulPet.player
        }
    })
    return result
}

console.log(task2(input4));
