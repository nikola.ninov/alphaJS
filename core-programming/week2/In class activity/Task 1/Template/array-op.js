export const removeFirst = (arr) => {
  if (!Array.isArray(arr)) {
    throw new Error(`${arr} is not an Array!`);
  }
  let result = [...arr];
  result.shift();
  return result;
}

export const removeLast = (arr) => {
  if (!Array.isArray(arr)) {
    throw new Error(`${arr} is not an Array!`);
  }

  let result = [...arr];
  result.pop();
  return result;
};

export const addFirst = (arr, element) => {
  if (!Array.isArray(arr)) {
    throw new Error(`${arr} is not an Array!`);
  }
  let result = [...arr];
  result.unshift(element);
  return result;
};

export const addLast = (arr, element) => {
  if (!Array.isArray(arr)) {
    throw new Error(`${arr} is not an Array!`);
  }
  let result = [...arr];
  result.push(element);
  return result;
};

export const contains = (arr, element) => {
  if (!Array.isArray(arr)) {
    throw new Error(`${arr} is not an Array!`);
  }
  if (arr.includes(element)) {
    return true
  } else {
    return false
  }
};