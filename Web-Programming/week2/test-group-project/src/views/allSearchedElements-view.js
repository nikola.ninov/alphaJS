import { singleElement } from "./singleElement-view.js";

export const toAllSearchedElements = (data) => `
    <div class='all-from-search'>
      <div class="out">
        ${data.map(el => singleElement(el)).join('')}
      </div>        
    </div>
  `;
