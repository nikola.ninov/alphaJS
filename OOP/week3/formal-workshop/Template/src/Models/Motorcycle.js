import { Vehicle } from './Vehicle.js';
import { VehicleType } from './VehicleType.js';

export class Motorcycle extends Vehicle {
	static CATEGORY_MIN_LENGTH = 1;
	static CATEGORY_MAX_LENGTH = 10;
	static CATEGORY_ERROR_MESSAGE = `Category must be between ${Motorcycle.CATEGORY_MIN_LENGTH} and ${Motorcycle.CATEGORY_MAX_LENGTH} characters long!`;
	static WHEELS_COUNT = 2;
	#category;
	/**
	 * Creates an instance of Motorcycle.
	 *
	 * @param {string} make - The motorcycle's make.
	 * @param {string} model - The motorcycle's model.
	 * @param {number} price - The motorcycle's price.
	 * @param {string} category - The motorcycle's category.
	 */
	constructor(make, model, price, category) {
		super(Motorcycle.getVehicleType(), make, model, price, Motorcycle.WHEELS_COUNT);
		this.#category = category;
	}
	get category() {
		return this.#category;
	}
	static getVehicleType() {
		return Object.keys(VehicleType).find((key) => VehicleType[key] === Motorcycle.WHEELS_COUNT);
	}
}