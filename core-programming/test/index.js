let input5 = {
    dataProp: 'students',
    groupByProp: 'age',
    students: [
        {
            name: 'Peter',
            age: 20,
        },
        {
            name: 'George',
            age: 24,
        },
        {
            name: 'Maria',
            age: 20,
        },
    ],
}
// function task3(data) {
//     let { dataProp, groupByProp, ...values } = data
//     values = values[dataProp];

//     let result = values.reduce((acc, curr) => {
//         const name = curr[groupByProp];
//         if (!acc[name]) {
//             acc[name] = []
//         }
//         acc[name].push(curr);
//         return acc;
//     }, {})
//     return result
// }
// console.log(task3(input5));

// console.log(Object.keys(input5));

// Object.keys(input5).forEach(el => {
//     console.log(input5[el]);
// });
// const { students } = input5
// // console.log(students);
// students.map(e => {
//     if (e.name === 'Peter') {
//         e.name = 'PeterCovered'
//     }
// })

// // console.log(students);
// // console.log(input5);

// let myModule = (function () {
//     function mant() {
//         console.log(`Something`);
//     }
//     return {
//         publicMethod: function () {
//             console.log('Hello World!');
//         },
//         mant: mant
//     };
// })();

// myModule.mant;

// const nums = [1, 2, 3, 4, undefined]
// const filtered = nums.filter(x => true)
// console.log(filtered);

// function demo() {

//     let count = 0;

//     function increment() {
//         console.log(++count);

//     }

//     return increment();
// }

// const test = demo();

// test(1);
// test(2);
// test(3);

// function counter() {
//     let count = 0;

//     function increment() {
//         ++count;
//         console.log(count);
//     }

//     return increment

// }

// const myCounter = counter();

// myCounter(); // output: 1
// myCounter(); 
// // myCounter(); // output: 2
// // myCounter(); // output: 3


let a;

((a = 5) => {

})(a)

console.log(a);

